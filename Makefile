##
## Makefile for Makefile in /home/sarkad_l/rendu/piscine_cpp_d01/ex01
## 
## Made by lucas sarkadi
## Login   <sarkad_l@epitech.net>
## 
## Started on  Wed Jan  8 12:46:27 2014 lucas sarkadi
## Last update Sun Jun 15 21:13:40 2014 Thibaud Dahan
##

CXX =	g++

RM =	rm -f

CXXFLAGS +=	-Wall -Wextra -ansi -g -g3 -O4 -I includes/

LDFLAGS  +=	-Llibs/ -lgdl_gl -lGL -lGLEW -ldl -lrt -lfbxsdk 
LDFLAGS  +=	-lSDL2 -lpthread -Wl,-rpath=./libs/ -lfmodexL64-4.44.35

NAME =	Bomberman

SRCS =	src/main.cpp \
	src/AObject.cpp \
	src/Wall/Wall.cpp \
	src/Fire/Fire.cpp \
	src/Bonhomme/Bonhomme.cpp \
	src/Bonus/Bonus.cpp \
	src/Bombes/Bombes.cpp \
	src/Save.cpp \
	src/Menu/Menu.cpp \
	src/Overlay/Overlay.cpp \
	src/GameEngine/GameEngine.cpp \
	src/Mod.cpp \
	src/Floor/Floor.cpp \
	src/TextContainer.cpp \
	src/Map.cpp \
	src/Particles/Particles.cpp \
	src/Audio.cpp \
	src/Floor/Sky.cpp \
	src/Thread.cpp \
	src/Splitter.cpp \

OBJS =	$(SRCS:.cpp=.o)

all:	$(NAME)

$(NAME): $(OBJS)
	 $(CXX) $(OBJS) -o $(NAME) $(LDFLAGS)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re

