#version 120

uniform sampler2D fTexture0;

varying vec4 fColor;
varying vec3 fNormal;
varying vec2 fUv;

uniform vec4 myColor = vec4(0.4, 0.4, 0.9, 1.0);

void main(void)
{
	gl_FragColor = texture2D(fTexture0, fUv) * myColor;
}
