//
// Map.hpp for Map in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  9 20:16:29 2014 corentin bajdek
// Last update Sun Jun 15 11:48:21 2014 corentin bajdek
//

#ifndef MAP_HPP_
# define MAP_HPP_

#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Mod.hpp"
#include "TextContainer.hpp"
#include "Audio.h"

class Bonhomme;
class Bonhomme_J2;
class Bonhomme_IA;
class Wall;
class Wall_Destructible;
class Bonus;
class Bombes;
class Floor;
class Fire;
class AObject;
class Texte;
class Background;
class Loading;
class Minimap;
class Overlay;
class Overlay_long;
class Overlay_top;
class Info;
class Score;
class IScore_top;
class GameEngine;

class	Map
{
protected:
  int			        _width;
  int				_length;
  TextContainer			*_textContainer;
  Mod				&_mode;
  int				_human1;
  int				_human2;
  Minimap			*_minimap1;
  Minimap			*_minimap2;
  Audio				_sound;

public:
  std::vector<AObject *>	Objects;



  Map(Mod &mode);
  ~Map();
  GameEngine	*create_gameengine(const Mod &mod, gdl::SdlContext &context, int width_screen = 1800, int length_screen = 1000);
  IScore_top	*create_score_top(Bonhomme *father);
  Score		*create_score(Bonhomme *father);
  Bonhomme	*create_bonhomme(int _pos_x, int _pos_y, int id);
  Wall		*create_wall(int _pos_x, int _pos_y, bool);
  Bonus		*create_bonus(int _pos_x, int _pos_y);
  Fire		*create_fire(int _pos_x, int _pos_y, double delay, int multiples, int direction, Bonhomme *father);
  Bombes	*create_bombes(int _pos_x, int _pos_y, double delay, Bonhomme *father, int max_len);
  Floor		*create_floor(int _pos_x, int _pos_y);
  Texte		*create_texte(const std::string ph, const float size = 25.0f, const glm::vec4 color = glm::vec4(1, 1, 1, 1));
  Texte		*create_texte(int value, const float size = 25.0f, int nbzero = 0, const glm::vec4 color = glm::vec4(1, 1, 1, 1));
  Background	*create_background(const std::string path);
  Loading	*create_loading(int nb_items_max);
  Overlay	*create_overlay();
  void		create_minimap(int, Bonhomme *father);
  Overlay_long	*create_overlay_long();
  Overlay_top	*create_overlay_top();
  Info		*create_info(const std::string path, double target_x, double target_y);

  void		clear(void);
  void		delete_minimap(int);
  void		define_places(int);
  void		define_places(int, int);

  // Setters & getters
    int				get_width(void) const;
  int				get_length(void) const;

  void				set_width(int);
  void				set_length(int);
  TextContainer			*getTextContainer(void) const;
  Minimap			*get_minimap(int) const ;
  Mod				&get_mod(void) {return (this->_mode);}
  Audio				&get_audio(void) {return (this->_sound);}
  
};

#endif /* MAP_HPP_ */
