//
// Bombes.hh for Bombes in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 22:03:23 2014 corentin bajdek
// Last update Sun Jun 15 12:26:23 2014 corentin bajdek
//

#ifndef BOMBES_H_
# define BOMBES_H_

#include "../AObject.hpp"
#include "../Fire/Fire.hpp"
#include "../Bonhomme/Bonhomme.hpp"
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <vector>
#include <iostream>

enum
  {
    KICKED,
    THROWN,
    DYING,
  };

class Bombes : public AObject
{
private :
  gdl::Texture	_texture;
  gdl::Geometry	_geometry;
  gdl::Model	_model;
  int		_max_len;
  double	_delay;
  double	_lifetime;
  Bonhomme	*_father;
  Bonhomme	*_kicker;
  Bonhomme	*_thrower;
  int		_state;
  int		_direction;
  double	_double_pos_x;
  double	_double_pos_y;
  glm::vec2	_target;
  double	_high;
  double	_moved;
  bool		_on_fire;
  int	       _mapWidth;
  int	       _mapLength;

public :
  Bombes(int, int, double, Bonhomme *father, int, Map *data);
  ~Bombes(void);

// Setters & getters
  int		get_delay(void) const;
  int		get_max_len(void) const {return (_max_len) ;}
  void		set_delay(int);
  virtual bool	initialize();
  virtual int update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::AShader &shader, gdl::Clock const &clock);
  virtual void	explode();
  virtual void	is_touched_by_fire(Bonhomme *);
  virtual void	is_kicked(int direction, Bonhomme *);
  virtual void	is_thrown(int direction, Bonhomme *);
  void		move(gdl::Clock const &);
  void		fly(gdl::Clock const &);
  int		Harr(double value, int to_do);
  void		save(std::ofstream &) const;
  void		load(std::vector< double> &values);
};


#endif /* !BOMBES_H_ */
