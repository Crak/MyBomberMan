//
// Fire.cpp for Fire in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 22:03:34 2014 corentin bajdek
// Last update Sun Jun 15 22:58:29 2014 corentin bajdek
//

#include "Bombes.hpp"
#include "../Particles/Particles.hpp"

/*
** Constructeurs / Destructeurs
*/

Bombes::Bombes(int pos_x, int pos_y, double delay,
	       Bonhomme *father, int max_len, Map *data) :
  AObject(pos_x, pos_y, data), _max_len(max_len), _delay(delay), _father(father),
  _state(NONE), _direction(NONE), _high(0), _moved(0), _on_fire(false)
{
  _mapWidth = data->get_width();
  _mapLength = data->get_length();
  _type = BOMBE;
  _lifetime = 0.0;
  _double_pos_x = (double)_pos_x;
  _double_pos_y = (double)_pos_y;
  _kicker = NULL;
  _thrower = NULL;
  return ;
}

Bombes::~Bombes(void) { return ; }

/*
** Setters/getters
*/

int		Bombes::get_delay(void) const { return (this->_delay) ; }
void		Bombes::set_delay(int delay) { this->_delay = delay ; }

bool		Bombes::initialize(void)
{
  if (!(_model.load(std::string("./assets/bomb.fbx"))))
    return (false);
  if (!(_model.setCurrentAnim(0, true)))
    return (false);
  scale(glm::vec3(0.0025, 0.0025, 0.0025));
  _initialize = true;
  return (true);
}

void		Bombes::explode()
{
  AObject	*ptr;
  AObject	*part = new Particles (_pos_x, _pos_y, _data);

  _data->get_audio().Play_blast();
  _data->Objects.push_back(part);
  ptr = _data->create_fire(_pos_x, _pos_y, SPREAD_DELAY, _max_len, UP, _father);
  _data->Objects.push_back(ptr);
  ptr = _data->create_fire(_pos_x, _pos_y, SPREAD_DELAY, _max_len, DOWN, _father);
  _data->Objects.push_back(ptr);
  ptr = _data->create_fire(_pos_x, _pos_y, SPREAD_DELAY, _max_len, LEFT, _father);
  _data->Objects.push_back(ptr);
  ptr = _data->create_fire(_pos_x, _pos_y, SPREAD_DELAY, _max_len, RIGHT, _father);
  _data->Objects.push_back(ptr);
  
  // un dans chaque sens
}

int		Bombes::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)input;
  _lifetime += clock.getElapsed();
  if (_state == DYING)
    return (!(_lifetime > (_delay)));
  if (_state == KICKED)
    this->move(clock);
  if (_state == THROWN)
    this->fly(clock);
  if (_lifetime > (_delay) || _dying == true)
    {
      if (_lifetime > _delay || _on_fire == true)
	this->explode();
      _father->set_bombes_max(_father->get_bombes_max() + 1);
      _state = DYING;
      _lifetime = 0.0;
      _delay = (_max_len + 1) * SPREAD_DELAY;
    }
  return (true);
}

void		Bombes::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;
  if (_state == DYING)
    return ;
  translate(glm::vec3(_double_pos_x, _double_pos_y, _high));
  _model.draw(shader, getTransformation(), clock.getElapsed());
  translate(glm::vec3(-(_double_pos_x), -(_double_pos_y), -_high));
}

void		Bombes::is_touched_by_fire(Bonhomme *father)
{
  (void)father;
  _on_fire = true;
  _dying = true;
}

int		Bombes::Harr(double value, int to_do)
{
  value += 0.5;
  if (to_do == 0 && (int)(value * 8) % 8 <= 2)
    return (int)(value) - 1;
  if (to_do == 1 && (int)(value * 8) % 8 >= 5)
    return (int)(value) + 1;
  return ((int)(value));
}

void		Bombes::fly(gdl::Clock const &clock)
{
  if (_direction == UP)
    _double_pos_y += _father->get_speed() * 2 * clock.getElapsed();
  if (_direction == DOWN)
    _double_pos_y -= _father->get_speed() * 2 * clock.getElapsed();
  if (_direction == LEFT)
    _double_pos_x -= _father->get_speed() * 2 * clock.getElapsed();
  if (_direction == RIGHT)
    _double_pos_x += _father->get_speed() * 2 * clock.getElapsed();

  _moved += _father->get_speed() * 2 * clock.getElapsed();

  if (_moved <= 1.50)
    _high = (0.75 * _moved) +  ((0.75 * _moved) * (0.75 * _moved));
  else
    _high = (1.12 + (1.12 * 1.12)) - (((0.75 * _moved) - 1.50) + (((0.75 * _moved) - 1.50) * ((0.75 * _moved) - 1.50)));
  
  _pos_x = ARR(_double_pos_x);
  _pos_y = ARR(_double_pos_y);
  if (_pos_x >= _mapWidth || _pos_y >= _mapLength || _pos_y < 0 || _pos_x < 0)
    _dying = true;
  if ((_direction == UP && _double_pos_y >= _target.y) ||
      (_direction == DOWN && _double_pos_y <= _target.y) ||
      (_direction == RIGHT && _double_pos_x >= _target.x) ||
      (_direction == LEFT && _double_pos_x <= _target.x))
    {
      for (size_t i = 0; i < _data->Objects.size(); ++i)
	if (_data->Objects[i]->get_pos_x() == _pos_x && _data->Objects[i]->get_pos_y() == _pos_y)
	  {
	    if (_data->Objects[i]->get_type() == WALL)
	      {
		if (_data->Objects[i]->get_subtype() == DEFAULT)
		  _dying = true;
		else
		  {
		    this->explode();
		    _dying = true;
		  }
	      }
	    else if (_data->Objects[i]->get_type() == BOMBE && _data->Objects[i] != this)
	      {
		_data->Objects[i]->is_touched_by_fire(_father);
		_dying = true;
	      }		  
	  }
      _state = NONE;
      _high = 0;
      _direction = NONE;
      _moved = 0;
    }
}

void		Bombes::move(gdl::Clock const &clock)
{
  double	save_x;
  double	save_y;

  save_x = _double_pos_x;
  save_y = _double_pos_y;
  if (_direction == UP)
    _double_pos_y += _father->get_speed() * 2 * clock.getElapsed();
  if (_direction == DOWN)
    _double_pos_y -= _father->get_speed() * 2 * clock.getElapsed();
  if (_direction == LEFT)
    _double_pos_x -= _father->get_speed() * 2 * clock.getElapsed();
  if (_direction == RIGHT)
    _double_pos_x += _father->get_speed() * 2 * clock.getElapsed();

  _pos_x = ARR(_double_pos_x);
  _pos_y = ARR(_double_pos_y);

  for (size_t i = 0; i < _data->Objects.size(); ++i)
    {
      if ((_data->Objects[i]->get_type() == WALL || (_data->Objects[i]->get_type() == BOMBE &&
						     _data->Objects[i] != this)) &&
	  ((_data->Objects[i]->get_pos_x() == Harr(_double_pos_x, !(_double_pos_x < save_x)) &&
	    _data->Objects[i]->get_pos_y() == _pos_y) ||
	   (_data->Objects[i]->get_pos_y() == Harr(_double_pos_y, !(_double_pos_y < save_y)) &&
	    _data->Objects[i]->get_pos_x() == _pos_x)))
	{
	  _double_pos_x = save_x;
	  _double_pos_y = save_y;
	  _pos_x = ARR(_double_pos_x);
	  _pos_y = ARR(_double_pos_y);
	  _state = NONE;
	  _direction = NONE;
	}
    }
}

void		Bombes::is_kicked(int direction, Bonhomme *kicker)
{
  if (_kicker != NULL && _state == KICKED && kicker == _kicker)
    {
      kicker->set_nbkick(kicker->get_nbkick() + 1);
      return ;
    }
  _kicker = kicker;
  _state = KICKED;
  _direction = direction;
}

void		Bombes::is_thrown(int direction, Bonhomme *thrower)
{
  if (_thrower != NULL && _state == THROWN && thrower == _thrower)
    {
      thrower->set_nbthrow(thrower->get_nbthrow() + 1);
      return ;
    }
  _thrower = thrower;
  _state = THROWN;
  _direction = direction;
  if (direction == UP)
    _target = glm::vec2(_double_pos_x, _double_pos_y + 3);
  if (direction == DOWN)
    _target = glm::vec2(_double_pos_x, _double_pos_y - 3);
  if (direction == LEFT)
    _target = glm::vec2(_double_pos_x - 3, _double_pos_y);
  if (direction == RIGHT)
    _target = glm::vec2(_double_pos_x + 3, _double_pos_y);
}

void		Bombes::save(std::ofstream &ofs) const
{
  ofs << "Bombes" << "::";
  ofs << _pos_x << "::";
  ofs << _pos_y << "::";
  ofs << _father->get_id() << "::";
  ofs << _delay << "::";
  ofs << _lifetime << "::";
  ofs << _max_len << "::";
}

void		Bombes::load(std::vector< double> &values)
{
  Bonhomme	*man;

  _pos_x = (int)values[0];
  _pos_y = (int)values[1];
  for (size_t i = 0; i < _data->Objects.size() ; i++)
    {
      if (_data->Objects[i]->get_type() == BONHOMME)
	{
	  man = dynamic_cast<Bonhomme *>(_data->Objects[i]);
	  if (man->get_id() == (int)values[2])
	    _father = man;
	}
    }
  _delay = values[3];
  _lifetime = values[4];
  _max_len = values[5];
}
