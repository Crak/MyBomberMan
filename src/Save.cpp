//
// Save.cpp for save in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 00:26:31 2014 corentin bajdek
// Last update Sun Jun 15 23:11:22 2014 corentin bajdek
//

#include <math.h> // sqrt & pow

#include "Loading_map.hpp"
#include "Save.hpp"

/*
** Constructeurs / Destructeurs
*/

# define ABS(X) ((X) < 0 ? - (X) : (X))

Save::Save(const std::string &path, gdl::SdlContext &context) :
  _path(path), _ID(0), _bool(0), _context(context)
{
  return ;
}

Save::~Save(void) { return ; }

bool				Save::load_saved(const std::string *file, Map *data)
{
  std::string				target(_path);
  target.append(file->c_str());	
  std::ifstream				my_file(target.c_str());
  std::string				line;
  std::vector< std::vector< double > >	values;
  std::vector< double>			subvalues;
  std::vector< std::string>		obj;
  std::vector< std::string>		box_line;
  char					buffer[50];
  size_t				found;
  int					param;
  int					IDJ1 = -42;
  int					IDJ2 = -42;
  AObject				*ptr;
  int					width = -42;
  int					length = -42;

  if (my_file.is_open())
    {
      while (getline(my_file, line) && line.compare("MODE_END::") != 0);
      while (getline(my_file, line))
	box_line.push_back(line);
      my_file.close();

      for (size_t k = 0; k < box_line.size(); k++)
	{
	  found = 0;
	  param = 0;
	  while ((found = box_line[k].find("::")) != std::string::npos)
	    {
	      std::size_t		length = box_line[k].copy(buffer, found);
	      buffer[length] = 0;
	      std::string		inter(buffer);
	      for (int i = 0; i < 50; i++)
		buffer[i] = 0;
	      box_line[k].erase(0, found + 2);
	      if (param == 0)
		obj.push_back(inter);
	      else
		{
		  double	ret = atof(inter.c_str());
		  subvalues.push_back(ret);
		}
	      param++;
	    }
	  values.push_back(subvalues);
	  subvalues.clear();
	}
      my_file.close();
    }
  else
    {
      std::cout << "fichier invalide" << std::endl;
      return (false);
    }

  for (size_t i =0; i < obj.size(); i++)
    {
      if (obj[i].compare("Bonhomme") == 0)
	{
	  if ((int)values[i][0] == J2)
	    IDJ2 = values[i][1];
	  if ((int)values[i][0] == DEFAULT)
	    IDJ1 = values[i][1];
	}
    }
  data->define_places(IDJ1, IDJ2);
  for (size_t i =0; i < obj.size(); i++)
    {
      if (obj[i].compare("Bonhomme") == 0)
	ptr = data->create_bonhomme(0, 0, values[i][1]);
      if (obj[i].compare("Bonus") == 0)
	ptr = data->create_bonus(0, 0);
      if (obj[i].compare("Bombes") == 0)
	ptr = data->create_bombes(0, 0, 0, NULL, 8);
      if  (obj[i].compare("Fire") == 0)
	ptr = data->create_fire(0, 0, SPREAD_DELAY, 0, 0, NULL);
      if  (obj[i].compare("Wall") == 0)
	ptr = data->create_wall(0, 0, (bool)(values[i][2]));

      data->Objects.push_back(ptr);
    }
  for (size_t i =0; i < data->Objects.size(); i++)
    {
      data->Objects[i]->load(values[i]);
      if (data->Objects[i]->get_pos_x() > length || length == -42)
	length = data->Objects[i]->get_pos_x();
      if (data->Objects[i]->get_pos_y() > width || width == -42)
	width = data->Objects[i]->get_pos_y();
    }
  if (width == -42 || length == -42)
    {
      std::cout << "fichier comrompu" << std::endl;
      return (false);
    }
  std::cout << "width :" << width << std::endl;
  std::cout << "length :" << length << std::endl;
  data->set_width(width + 1);
  data->set_length(length + 1);
  return (true);
}

  bool		Save::add_to_map(const std::string &line, Mod &mode,
				 Map *data, int nbline)
  {
    std::string	Allowed(" WIBPO");
    std::size_t	found = line.find_last_not_of(Allowed);
    std::string	target;
  
    mode.setwidth(line.size());
    data->set_length(line.size());
    if (found != std::string::npos)
      {
	std::cout << line << std::endl;
	return (false);
      }
  
    target = ("I");
    found = line.find_first_of(target);
    while (found != std::string::npos)
      {
	data->Objects.push_back(data->create_wall((int)found, nbline, false)); 
	found = line.find_first_of(target , found + 1);
      }
    target = ("O");
    found = line.find_first_of(target);
    while (found != std::string::npos)
      {
	data->Objects.push_back(data->create_bonus((int)found, nbline));
	found = line.find_first_of(target , found + 1);
      }
    target = ("W");
    found = line.find_first_of(target);
    while (found != std::string::npos)
      {
	data->Objects.push_back(data->create_wall((int)found, nbline, true)); 
	found = line.find_first_of(target , found + 1);
      }
    target = ("P");
    found = line.find_first_of(target);
    while (found != std::string::npos)
      {
	data->Objects.push_back(data->create_bonhomme((int)found, nbline, _ID++)); 
	found = line.find_first_of(target , found + 1);
      }
    return (true);
  }

  int					Save::get_distance(int x, int y, int x2, int y2)
  {
    int					ret;

    ret = sqrt((pow(ABS(x - x2), 2)) + (pow(ABS(y - y2), 2)));
    return (ret);
  }

  bool		Save::my_look_around(std::vector< std::vector<int> > &map, int x, int y, Map *data)
  {
    bool		ret = true;
  
    if (x > 0 && map[x - 1][y] == -4)
      ret = false;
    if (y > 0 && map[x][y - 1] == -4)
      ret = false;
    if (x != data->get_width() && map[x + 1][y] == -4)
      ret = false;
    if (y != data->get_length() && map[x][y + 1] == -4)
      ret = false;
    return (ret);
  }

  int					Save::get_smallest(std::vector< std::vector<int> > &map, int x, int y, Map *data)
  {
    int					smallest = -42;
    int					distance;

    for (int i = 0; i < data->get_length(); i++)
      for (int j = 0; j < data->get_width(); j++)
	if (map[i][j] == -4)
	  {
	    distance = get_distance(i, j, x, y);
	    if ((distance < smallest)  || (smallest == -42))
	      smallest = distance;
	  }
    return (smallest);
  }

bool					Save::generate_map(Mod &mode, Map *data, int width, int length)
{
  std::vector< std::vector<int> >	map;
  int					nbplayer;
  double				best_distance = -42;;
  double				ret;
  int					best_pos_x = 1;
  int					best_pos_y = 1;
  int					put = 0;
  int					id = 0;
  glm::mat4				projection = glm::ortho(0.0f, 1800.0f, 1000.0f, 0.0f, -1.0f, 1.0f);

  //set_values
  glEnable(GL_DEPTH_TEST);
  if (!_shader.load("./shaders/basic.fp", GL_FRAGMENT_SHADER)
      || !_shader.load("./shaders/basic.vp", GL_VERTEX_SHADER)
      || !_shader.build())
    return false;
  _shader.bind();
  _shader.setUniform("view", glm::mat4(1));
  _shader.setUniform("projection", projection);
  glViewport(0, 0, 1800.0f, 1000.0f);


  nbplayer = mode.getIntMod("HumanPlayers") + mode.getIntMod("NbIa");
  if (nbplayer == 1)
    return (false);
  data->set_width(width);
  data->set_length(length);
  //loading
  Loading_map				load(data, nbplayer - 1);
  load.initialize();

  // resize
  map.resize(length);
  for (int i = 0; i < length; i++)
    map[i].resize(width);

  // murs tout autour
  for (int i=0; i < length; i++)
    {
      map[i][0] = -2;
      map[i][width - 1] = -2;
    }
  for (int i=0; i < width; i++)
    {
      map[0][i] = -2;
      map[length - 1][i] = -2;
    }
  // mur aux intersections
  for (int i = 0; i < length; i++)
    for (int j = 0; j < width; j++)
      if (i % 2 == 0 && j % 2 == 0)
	map[i][j] = -2;
  map[1][1] = -4;
  // algo de lee dans chaque case
  for (put = 0; put < nbplayer - 1; put++)
    {
      best_distance = -42;
      for (int i = 0; i < length; i++)
	{
	  for (int j = 0; j < width; j++)
	    if (map[i][j] == 0)
	      {
		_bool = 0;
		if (((ret = get_smallest(map, i, j, data)) > best_distance)  ||  best_distance == -42)
		  {
		    best_distance = ret;// prend la moyenne
		    best_pos_x = i;
		    best_pos_y = j;
		  }
	      }
	}
      if (best_distance == -42)
	{
	  std::cout << "impossible de placer le perso : map trop petite" << std::endl;
	  return (false);
	}
      load.update(_clock, _input, put);
      _context.updateClock(_clock);
      _context.updateInputs(_input);
      load.draw(_shader);
      _context.flush();
      map[best_pos_x][best_pos_y] = -4;
    }
  data->define_places(nbplayer);

  // attente d'input de l'utilisateur
  while (load.update(_clock, _input, put) != false)
    {
      _context.updateClock(_clock);
      _context.updateInputs(_input);
      load.draw(_shader);
      _context.flush();
    }

  //placement des murs destructibles	 
  for (int i = 0; i < data->get_length(); i++)
    for (int j = 0; j < data->get_width(); j++)
      {
	ret = rand() % 10;
	if (map[i][j] == 0 && ret <= 6)
	  if (my_look_around(map, i, j, data) == true)
	    map[i][j] = -3;
      }
  //placement on mets tout ca dans obj
  for (int i = 0; i < data->get_length(); i++)
    for (int j = 0; j < data->get_width(); j++)
      {
	if (map[i][j] == -2)
	  data->Objects.push_back(data->create_wall(i, j, false));
	else if (map[i][j] == -4)
	  data->Objects.push_back(data->create_bonhomme(i, j, id++));
	else if (map[i][j] == -3)
	  data->Objects.push_back(data->create_wall(i, j, true));
      }
  //  _data->get_mod().setIntMod("Origin", ALEATOIRE);
  return (true);
}

bool		Save::load_map(const std::string &file, Mod &mode, Map *data)
{
  std::vector<std::string>	box_lines;
  std::string			line;
  std::string			file_name(_path + file);
  std::ifstream			my_file(file_name.c_str());
  std::size_t			found;
  int				Nbspawn = 0;

  _ID = 0;
  if (my_file.is_open())
    {
      // lit le fichier
      while (getline(my_file, line))
	box_lines.push_back(line);
      my_file.close();
      // compte le nombre de points de spawn
      for (size_t i = 0; i < box_lines.size(); i++)
	{
	  found = box_lines[i].find_first_of("P");
	  while (found != std::string::npos)
	    {
	      Nbspawn++;
	      found = box_lines[i].find_first_of("P" , found + 1);
	    }
	}
      // définit la place des joueurs au hasard
      data->define_places(Nbspawn);
      // crée la map
      for (size_t i = 0; i < box_lines.size(); i++)
	if ((add_to_map(box_lines[i], mode, data, i)) == false)
	  {
	    std::cerr << "File is invalid or corrupted" << std::endl;
	    std::cout << "labitte"  << std::endl;
	    return (false);
	  }
    }
  else
    {
      std::cerr << "Error : Couldn't open the file :" << file_name << std::endl;
      return (false);
    }
  mode.setlength(box_lines.size());
  data->set_width(box_lines.size());
  std::cout << "width :" << data->get_width() << std::endl;
  std::cout << "length :" << data->get_length() << std::endl;
  return (true);
}
