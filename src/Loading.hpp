//
// Loading.hpp for Loading in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May 15 14:15:22 2014 corentin bajdek
// Last update Thu Jun 12 18:52:39 2014 corentin bajdek
//

#ifndef LOADING_H_
# define LOADING_H_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <sstream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "Mod.hpp"
#include "Map.hpp"
#include "Background.hpp"
#include "Menu/Texte.hpp"

class Loading
{
private :
  Map				*_data;
  int				_current_nbitems;
  int				_max_nbitems;
  Background			*_back;
  Texte				*_ph;
  Texte				*_percentage;
  Texte				*_modulo;
  Texte				*_press_key;
  double			_lifetime;

public :
  Loading(Map *data, int max_nbitems)
    : _data(data), _current_nbitems(0), _max_nbitems(max_nbitems), _lifetime(0.0)
  {
    if (rand() % 2 == 0)
      _back = _data->create_background("./assets/Bomberman2.tga");
    else
      _back = _data->create_background("./assets/Bomberman1.tga");
    _ph = _data->create_texte("Loading : ");
    _percentage = _data->create_texte("00");
    _modulo = _data->create_texte("%");
    _press_key  = _data->create_texte("Press space to continue");
  }

 ~Loading()
  {
    delete _back;
    delete _ph;
    delete _modulo;
    delete _press_key;
    if (_percentage != NULL)
      delete _percentage;
  }

  bool		initialize()
  {
    if ((_back->initialize()) == false)
      return (false);
    if ((_ph->initialize()) == false)
      return (false);
    if ((_modulo->initialize()) == false)
      return (false);
    if ((_percentage->initialize()) == false)
      return (false);
    if ((_press_key->initialize()) == false)
      return (false);
    return (true);
  }
  
  bool		update(gdl::Clock const &clock, gdl::Input &input, int current_nbitems)
  {
    _current_nbitems = current_nbitems;
    _lifetime += clock.getElapsed();
    if (input.getKey(SDLK_SPACE))
      return (false);
    return (true);
  }
  
  void			draw(gdl::AShader &shader)
  {
    int			result;
    std::ostringstream	ss;

    _back->draw(shader);
    _ph->draw(shader, 0, 0);
    delete _percentage;
    result = (_current_nbitems * 100) / _max_nbitems;
    ss << result;

    _percentage = _data->create_texte(ss.str()); 
    _percentage->initialize();
    _percentage->draw(shader, 110, 0);
    if (result == 100)
      _modulo->draw(shader, 150, 0);
    else
      _modulo->draw(shader, 140, 0);
    if ((_lifetime - (int)_lifetime) < (double)(0.5) && result == 100)
      _press_key->draw(shader, 0, 35);
  }
};

#endif /* */
