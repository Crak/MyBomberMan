//
// Intro.hpp for Intro in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by Lucas SARKADI
// Login   <sarkad_l@epitech.net>
// 
// Started on  Sun Jun 15 17:09:45 2014 Lucas SARKADI
// Last update Sun Jun 15 19:58:33 2014 Lucas SARKADI
//

#ifndef INTRO_HPP_
# define INTRO_HPP_

#include <Geometry.hh>
#include <Game.hh>
#include "Menu/Texte.hpp"
#include "Menu/Backmenu.hpp"
#include "Map.hpp"

class	Intro
{
private:
  gdl::Geometry		_title;
  double		_lifetime;
  glm::vec3		_position;  
  Map			*_data;
  gdl::Texture		*_text;
  Background		*_back_one;
  Background		*_back_two;
  Background		*_back_three;
  bool			_exploded;
  Texte			*_press_key;

public:
  Intro(Map *data) : _data(data)
  {
    _back_one = _data->create_background("./assets/Intro1.tga");
    _back_two = _data->create_background("./assets/Fire.tga");
    _back_three = _data->create_background("./assets/Intro2.tga");
    _back_one->initialize();
    _back_two->initialize();
    _back_three->initialize();
    _lifetime = 0;
    _exploded = false;
    _press_key  = _data->create_texte("Press space to continue");
  }
  ~Intro()
  {
    delete _press_key;
  }
  bool	initialize()
  {
    _position = glm::vec3(50.0f, -700.0f, 0.0f);
    _text = _data->getTextContainer()->getTexture("./assets/Bomberman_intro.tga");

    _title.pushVertex(glm::vec3(0, 0, 0));
    _title.pushVertex(glm::vec3(500, 0, 0));
    _title.pushVertex(glm::vec3(500, 100, 0));
    _title.pushVertex(glm::vec3(0, 100, 0));

    _title.pushUv(glm::vec2(0.0f, 0.0f));
    _title.pushUv(glm::vec2(1.0f, 0.0f));
    _title.pushUv(glm::vec2(1.0f, 1.0f));
    _title.pushUv(glm::vec2(0.0f, 1.0f));

    _title.build();

    if ((_press_key->initialize()) == false)
      return (false);
    return (true);
  }

  bool	update(gdl::Clock const &clock, gdl::Input &input)
  {
    _lifetime += clock.getElapsed();
    if (_lifetime <= 5)
      {
	_position.y += clock.getElapsed() * 300;
	if (_position.y > 900)
	  _position.y = 900;
	return (true);
      }
    return (!(input.getKey(SDLK_SPACE)));
  }

  void	draw(gdl::AShader &shader)
  {
    glm::mat4 transform(1);

    transform = glm::translate(transform, _position);
    
    if (_lifetime < 1.8)
      _back_one->draw(shader);
    else if (_lifetime <= 2.0)
      {
	if (!(_exploded))
	  {
	    _data->get_audio().Play_blast();	
	    _exploded = true;
	  }
	_back_two->draw(shader);
      }
    else
      _back_three->draw(shader);

    glAlphaFunc(GL_GREATER, 0.1f);
    glEnable(GL_ALPHA_TEST);

    _text->bind();

    _title.draw(shader, transform, GL_QUADS);

    glDisable(GL_ALPHA_TEST);

    if (_lifetime > 5)
      _press_key->draw(shader, 500, 850);
  }
};

#endif /* INTRO_HPP_ */
