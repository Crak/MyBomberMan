//
// Loading_map.hpp for Loading_map in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May 15 14:15:22 2014 corentin bajdek
// Last update Fri Jun 13 15:10:07 2014 corentin bajdek
//

#ifndef LOADING_MAP_H_
# define LOADING_MAP_H_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <sstream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "Mod.hpp"
#include "Map.hpp"
#include "Background.hpp"
#include "Menu/Texte.hpp"

class Loading_map
{
private :
  Map				*_data;
  int				_current_nbitems;
  int				_max_nbitems;
  Background			*_back;
  Texte				*_ph;
  Texte				*_percentage;
  Texte				*_modulo;
  Texte				*_press_key;
  double			_lifetime;
  int				_old;

public :
  Loading_map(Map *data, int max_nbitems)
    : _data(data), _current_nbitems(0), _max_nbitems(max_nbitems), _lifetime(0.0), _old(0)
  {
    _back = _data->create_background("./assets/Loading_map.tga");
    _ph = _data->create_texte("Generation de la map : ", 55 , glm::vec4(1, 153.0f / 255.0f, 51.0f / 255.0f, 1));
    _percentage = _data->create_texte(0, 55, 2, glm::vec4(1, 0, 0, 1));
    _modulo = _data->create_texte("%", 55, glm::vec4(1, 0, 0, 1));
    _press_key  = _data->create_texte("Press space to continue", 55, glm::vec4(1, 153.0f / 255.0f, 51.0f / 255.0f, 1));
  }

 ~Loading_map()
  {
    delete _back;
    delete _ph;
    delete _modulo;
    delete _press_key;
    if (_percentage != NULL)
      delete _percentage;
  }

  bool		initialize()
  {
    if ((_back->initialize()) == false)
      return (false);
    if ((_ph->initialize()) == false)
      return (false);
    if ((_modulo->initialize()) == false)
      return (false);
    if ((_percentage->initialize()) == false)
      return (false);
    if ((_press_key->initialize()) == false)
      return (false);
    return (true);
  }
  
  bool		update(gdl::Clock const &clock, gdl::Input &input, int current_nbitems)
  {
    _current_nbitems = current_nbitems;
    _lifetime += clock.getElapsed();
    if (input.getKey(SDLK_SPACE))
      return (false);
    return (true);
  }
  
  void			draw(gdl::AShader &shader)
  {
    int			result;

    _back->draw(shader);
    _ph->draw(shader, 150, 150);

    result = (_current_nbitems * 100) / _max_nbitems;
    if (result != _old)
      {
	_old = result;
	delete _percentage;
	_percentage = _data->create_texte(result, 55, 2, glm::vec4(1, 0, 0, 1)); 
	_percentage->initialize();
      }
    _percentage->draw(shader, 400, 500);
    _modulo->draw(shader, 480, 500);
    if ((_lifetime - (int)_lifetime) < (double)(0.5) && result == 100)
      _press_key->draw(shader, 125, 800);
  }
};

#endif /* */
