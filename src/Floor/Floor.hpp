//
// Cube.hpp for Cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:49:38 2014 corentin bajdek
// Last update Sun Jun 15 14:51:40 2014 Lucas SARKADI
//

#ifndef FLOOR_HPP_
# define FLOOR_HPP_

#include <Model.hh>
#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include "../Map.hpp"
#include "../AObject.hpp"

class Floor : public AObject

{
private:
  // La texture utilisee pour le cube
  gdl::Model		_model;
  gdl::Texture		*_texture;
  gdl::Geometry		_geometry;
  unsigned int		width;
  unsigned int		length;

public:
  Floor(unsigned int, unsigned int, Map *data);
  ~Floor();
  virtual bool initialize();
  virtual int  update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::AShader &shader, gdl::Clock const &clock);
};

#endif /* FLOOR_HPP_ */
