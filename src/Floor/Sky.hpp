//
// Sky.hpp for Sky in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Floor
// 
// Made by Lucas SARKADI
// Login   <sarkad_l@epitech.net>
// 
// Started on  Fri Jun 13 23:20:25 2014 Lucas SARKADI
// Last update Fri Jun 13 23:21:17 2014 Lucas SARKADI
//

#ifndef SKY_HPP_
# define SKY_HPP_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include "../Map.hpp"
#include "../AObject.hpp"

class Sky : public AObject
{
private:
  gdl::Texture		*_texture;
  gdl::Geometry		_geometry;
  unsigned int		width;
  unsigned int		length;

public:
  Sky(unsigned int, unsigned int, Map *data);
  ~Sky();
  virtual bool initialize();
  virtual int  update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::AShader &shader, gdl::Clock const &clock);
};

#endif /* SKY_HPP_ */
