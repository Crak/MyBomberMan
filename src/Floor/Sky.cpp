//
// Wall.cpp for cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:22:26 2014 corentin bajdek
// Last update Sat Jun 14 05:16:33 2014 Lucas SARKADI
//

#include "Sky.hpp"

Sky::Sky(unsigned int width, unsigned int length, Map *data)
  :   AObject(0, 0, data)
{
  this->width = width;
  this->length = length;
  return ; 
}

Sky::~Sky() {return ; }

bool		Sky::initialize(void)
{
  _texture = _data->getTextContainer()->getTexture("./assets/fond_3.tga");

  _geometry.pushVertex(glm::vec3(-40.5, -40.5, -5.5));
  _geometry.pushVertex(glm::vec3(39.5 + width, -40.5, -5.5));
  _geometry.pushVertex(glm::vec3(39.5 + width, 39.5 + length, -5.5));
  _geometry.pushVertex(glm::vec3(-40.5, 39.5 + length, -5.5));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.build();
  _initialize = true;
  return (true);
}

int	Sky::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
  return (true);
}

void	Sky::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;

  _texture->bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}
