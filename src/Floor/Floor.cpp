//
// Wall.cpp for cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:22:26 2014 corentin bajdek
// Last update Sun Jun 15 14:49:36 2014 Lucas SARKADI
//

#include "Floor.hpp"

Floor::Floor(unsigned int width, unsigned int length, Map *data)
  :   AObject(0, 0, data)
{
  this->width = width;
  this->length = length;
  return ; 
}

Floor::~Floor() {return ; }

bool		Floor::initialize(void)
{
  if (!(_model.load(std::string("./assets/bomb.fbx"))))
    return (false);

  _texture = _data->getTextContainer()->getTexture("./assets/10.tga");

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(width, 0.0f));
  _geometry.pushUv(glm::vec2(width, length));
  _geometry.pushUv(glm::vec2(0.0f, length));

  _geometry.pushVertex(glm::vec3(-0.5, -0.5, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + width, -0.5, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + width, -0.5 + length, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5, -0.5 + length, -0.5));

  _geometry.build();
  _initialize = true;
  return (true);
}

int	Floor::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
  return (true);
}

void	Floor::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;
  _texture->bind();
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  _geometry.draw(shader, getTransformation(), GL_QUADS);
}
