//
// Thread.hpp for Threa in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by Lucas SARKADI
// Login   <sarkad_l@epitech.net>
// 
// Started on  Fri Jun 13 23:51:02 2014 Lucas SARKADI
// Last update Sat Jun 14 01:47:09 2014 Lucas SARKADI
//

#ifndef THREAD_HPP_
# define THREAD_HPP_

#include <pthread.h>

typedef void *(*Fonction)(void *);

class	Thread
{
private:
  pthread_t	_id;
  Fonction	_function;
  void		*_data;
public:
  Thread(Fonction, void *);
  ~Thread();
  int	create();
  int	join();
};

#endif /* THREAD_HPP_ */
