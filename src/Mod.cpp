//
// Mod.cpp for mode in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 00:09:42 2014 corentin bajdek
// Last update Sun Jun 15 10:59:35 2014 corentin bajdek
//

#include "Mod.hpp"

/*
** Constructeurs / Destructeurs
*/

Mod::Mod() {}


Mod::~Mod(void) {}
/*
** Setters/getters
*/

int		Mod::getwidth(void) const { return (this->width) ; }

int		Mod::getlength(void) const { return (this->length) ; }


void		Mod::setwidth(int _width) { this->width = _width; }

void		Mod::setlength(int _length) { this->length = _length ; }

int		Mod::getIntMod(const std::string &str)
{
  return _int_mod[str];
}

std::string	Mod::getStringMod(const std::string &str)
{
  return _string_mod[str];
}

void		Mod::setIntMod(const std::string &str, int nb)
{ _int_mod[str] = nb; }

void		Mod::setStringMod(const std::string &str, const std::string &nb)
{ _string_mod[str] = nb; }

void		Mod::save(std::ofstream &ofs)
{
  ofs << "HumanPlayers" << "::" << this->getIntMod("HumanPlayers") << "::\n";
  ofs << "NbIa" << "::" << this->getIntMod("NbIa") << "::\n";
  ofs << "GameMode" << "::" << this->getIntMod("GameMode") << "::\n";
  ofs << "NbTime" << "::" << this->getIntMod("NbTime") << "::\n"; 
  ofs << "NbLife" << "::" << this->getIntMod("NbLife") << "::\n";
  ofs << "NbTime" << "::" << this->getIntMod("NbTime") << "::\n";
  ofs << "NbBonus" << "::" << this->getIntMod("NbBonus") << "::\n";
  ofs << "MODE_END::\n";
  return ;
}
