/*
** main.c for main in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
** 
** Made by Lucas SARKADI
** Login   <sarkad_l@epitech.net>
** 
** Started on  Thu May  1 19:29:00 2014 Lucas SARKADI
// Last update Sun Jun 15 23:02:51 2014 corentin bajdek
*/


#include <cstdlib>
#include <Texture.hh>
#include <BasicShader.hh>
#include <Model.hh>
#include <Geometry.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include "Save.hpp"
#include "Menu/Menu.hpp"
#include "Audio.h"
#include "Menu/Submenu_mult.hpp"

int main()
{
  // On cree son engine
  Mod		mod;
  Map		*data;

  if (NB_IA_MAX > 300 || NB_IA_MAX < 11 || TAILLE_MAP_MAX > 300 || TAILLE_MAP_MAX < 11)
    {
      std::cerr << "Invalide Macro Submenu_mult.hpp" << std::endl;
      return (-1);
    }
  data = new Map(mod);
  mod.setIntMod("HumanPlayers", 2);
  mod.setIntMod("Overlay", 0);
  mod.setIntMod("NbBonus", MOYEN);
  mod.setIntMod("Action", NONE);
  mod.setIntMod("Difficulty", MOYEN);

  srand(time(NULL));

  data->get_audio().Play_music();

  Menu		menu(mod, data, 1800, 1000);

  if (menu.initialize() == false)
    return (EXIT_FAILURE);

  while (menu.update() == true)
    menu.draw();
  return EXIT_SUCCESS;
}
