//
// TextContainer.hpp for TextContainer in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by Lucas SARKADI
// Login   <sarkad_l@epitech.net>
// 
// Started on  Fri May  9 04:11:15 2014 Lucas SARKADI
// Last update Sun Jun 15 22:51:55 2014 corentin bajdek
//

#include "TextContainer.hpp"
#include <iostream>

TextContainer::TextContainer() {}

TextContainer::TextContainer(const TextContainer &other)
{
  this->_tabText = other._tabText;
}

TextContainer::~TextContainer(){}

gdl::Texture	*TextContainer::getTexture(const std::string &path)
{
  if (_tabText.count(path) == 0)
    {
      _tabText[path] = new gdl::Texture;
      if (!(_tabText[path]->load(path, true)))
	{
	  return (NULL);
	}
    }
  return (_tabText[path]);
}

bool		TextContainer::Load_everything()
{
  if (getTexture("./assets/Speed.tga") == NULL)
    return false;
  if (getTexture("./assets/Plus_bombe.tga") == NULL)
    return false;
  if (getTexture("./assets/Plus_range.tga") == NULL)
    return false;
  if (getTexture("./assets/Slow_enemy.tga") == NULL)
    return false;
  if (getTexture("./assets/Kick.tga") == NULL)
    return false;
  if (getTexture("./assets/Throw.tga") == NULL)
    return false;
  if (getTexture("./assets/Fire.tga") == NULL)
    return false;
  if (getTexture("./assets/10.tga") == NULL)
    return false;
  if (getTexture("./assets/fond_3.tga") == NULL)
    return false;
  if (getTexture("./assets/Letters/Letters.tga") == NULL)
    return false;
  if (getTexture("./assets/Yellow.tga") == NULL)
    return false;
  if (getTexture("./assets/Box.tga") == NULL)
    return false;
  if (getTexture("./assets/J1.tga") == NULL)
    return false;
  if (getTexture("./assets/J2.tga") == NULL)
    return false;
  if (getTexture("./assets/Fire.tga") == NULL)
    return false;
  if (getTexture("./assets/Bombe.tga") == NULL)
    return false;
  if (getTexture("./assets/Beige.tga") == NULL)
    return false;
  if (getTexture("./assets/1.tga") == NULL)
    return false;
  if (getTexture("./assets/Fire.tga") == NULL)
    return false;
  if (getTexture("./assets/Yellow.tga") == NULL)
    return false;
  if (getTexture("./assets/3.tga") == NULL)
    return false;
  if (getTexture("./assets/1.tga") == NULL)
    return false;
  if (getTexture("./assets/Box.tga") == NULL)
    return false;
  return true;
}
