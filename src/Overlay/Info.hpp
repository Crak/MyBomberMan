//
// Info.hpp for l in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Overlay
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 18 21:28:22 2014 corentin bajdek
// Last update Mon Jun  9 02:36:29 2014 corentin bajdek
//

#ifndef INFO_HPP_
# define INFO_HPP_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "../Map.hpp"
#include "../Menu/Texte.hpp"

class Info
{
private :
  gdl::Texture	 		*_texture;
  gdl::Geometry			_geometry;
  gdl::BasicShader		_shader;
  glm::vec3			_position;
  glm::vec3			_rotation;
  glm::vec3			_scale;  
  Map				*_data;
  const std::string		_path;
  double			_target_x;
  double			_target_y;
  int				_quantity;
  Texte				*_mult;
  Texte				*_value;
  bool				_alive;

public :
  Info(const std::string path, Map *data, double target_x, double target_y) : _position(0, 0, 0), _rotation(0, 0, 0), _scale(1, 1, 1), _data(data), _path(path), _target_x(target_x), _target_y(target_y), _quantity(0), _alive(false) {}
  ~Info() {}

  glm::mat4	getTransformation()
  {
    glm::mat4 transform(1); // On cree une matrice identite

    transform = glm::translate(transform, _position);

    transform = glm::scale(transform, _scale);

    transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
    transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
    transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
    return (transform);
  }

  bool	initialize()
  {
    _texture = _data->getTextContainer()->getTexture(_path);

    _geometry.pushVertex(glm::vec3(0, 50, 0));
    _geometry.pushVertex(glm::vec3(50, 50, 0));
    _geometry.pushVertex(glm::vec3(50, 0, 0));
    _geometry.pushVertex(glm::vec3(0, 0, 0));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));
    _geometry.build();
    _mult = _data->create_texte("x ", 33, glm::vec4(1, 140.0f / 255.0f, 0, 1));
    _value = _data->create_texte(_quantity, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1));
    _mult->initialize();
    _value->initialize();
    _position.y = 0;
    return true;
  }

  int			update(gdl::Clock const &clock, gdl::Input &input, int quantity)
  {
    (void)input;
    if (_alive == false)
      return (false);
    if (_position.y == _target_y)
      {
	if (quantity != _quantity)
	  {
	    _quantity = quantity;
	    delete _value;
	    _value = _data->create_texte(_quantity, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1));
	    _value->initialize();
	  }
	return (true);
      }
    else
      _position.y += 1000 * clock.getElapsed();
    if (_position.y >= _target_y)
      _position.y = _target_y;
    return (false);
  }

  void			draw(gdl::AShader &shader)
  {
    int			_width = 1000; // a envoyer en param
    int			_length = 1000; // a envoyer en param
    glm::mat4		projection = glm::ortho(0.0f, (float)_width, (float)_length, 0.0f, -1.0f, 1.0f);
    
    if (_alive == false)
      return ;
    shader.bind();
    shader.setUniform("view", glm::mat4(1));
    shader.setUniform("projection", projection);
    _texture->bind();
    _position.x = _target_x;

    /* activation de la transparence et desactivation du test de profondeur (inutile en 2D) */
    glDisable(GL_DEPTH_TEST);
    _geometry.draw(shader, getTransformation(), GL_QUADS);
    /* remise en état */
    glEnable(GL_DEPTH_TEST);
    if (_position.x < 200)
      {
	_mult->draw(shader, _position.x + 50, _position.y + 20);
	_value->draw(shader, _position.x + 70, _position.y);
      }
    else
      {
	_mult->draw(shader, _position.x + 25, _position.y + 20);
	_value->draw(shader, _position.x + 45, _position.y);
      }
  }

  bool			get_alive(void) { return (_alive);}
  void			set_alive(bool alive) { _alive = alive; }
};

# endif /* !INFO_HPP_*/
