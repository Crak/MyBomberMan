//
// IScore_top.hpp for score in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Overlay
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun Jun  8 23:40:46 2014 corentin bajdek
// Last update Tue Jun 10 00:50:24 2014 corentin bajdek
//

#ifndef ISCORE_TOP_HPP_
# define ISCORE_TOP_HPP_

class IScore_top
{
public :
  virtual int		update(gdl::Clock const &clock, gdl::Input &input) = 0;
  virtual void		draw(gdl::AShader &shader) = 0;
  virtual int		get_enemies(void) const = 0;
  virtual double	get_time(void) const = 0;
};

#endif /* */
