//
// Minimap.hpp for header in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May  8 17:41:17 2014 corentin bajdek
// Last update Sun Jun 15 22:38:55 2014 corentin bajdek
//

#include "Overlay.hpp"
#include "Overlay_long.hpp"
#include "Overlay_top.hpp"

Overlay::~Overlay()
{

}

glm::mat4	Overlay::getTransformation()
{
  glm::mat4 transform(1);

  transform = glm::translate(transform, _position);

  transform = glm::scale(transform, _scale);

  transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
  transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
  transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
  return (transform);
}

void	Overlay::rotate(glm::vec3 const& axis, float angle)
{
  _rotation += axis * angle;
}

bool	Overlay::initialize()
{
  _texture = _data->getTextContainer()->getTexture(_path);

  _geometry.pushVertex(glm::vec3(0, 400, 0));
  _geometry.pushVertex(glm::vec3(470, 400, 0));
  _geometry.pushVertex(glm::vec3(470, 0, 0));
  _geometry.pushVertex(glm::vec3(0, 0, 0));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.build();
  return true;
}

int	Overlay::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)input;
  if (_scaling != 0.8)
    _scaling += (clock.getElapsed() * 1.1);
  if (_scaling > 0.8)
    _scaling = 0.8;
  _scale.x = _scaling;
  _scale.y = 0.8;
  if (_scaling < (double) 0.77)
    return (false);
  return (true);
}

void			Overlay::draw(gdl::AShader &shader)
{
  int			_width = 1000;
  int			_length = 1000;
  glm::mat4		projection = glm::ortho(0.0f, (float)_width, (float)_length, 0.0f, -1.0f, 1.0f);
    
  shader.bind();
  shader.setUniform("view", glm::mat4(1));
  shader.setUniform("projection", projection);
  _texture->bind();
  _position.x = 630;
  _position.y = 680;

  /* activation de la transparence et desactivation du test de profondeur (inutile en 2D) */
  glDisable(GL_DEPTH_TEST);
  glAlphaFunc(GL_GREATER, 0.1f);
  glEnable(GL_ALPHA_TEST);
  _geometry.draw(shader, getTransformation(), GL_QUADS);
  /* remise en état */
  glDisable(GL_ALPHA_TEST);
  glEnable(GL_DEPTH_TEST);

}
