//
// Score_top_time.hpp for score in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon Jun  9 00:06:56 2014 corentin bajdek
// Last update Sun Jun 15 22:37:48 2014 corentin bajdek
//


#ifndef SCORE_TOP_TIME_HPP_
# define SCORE_TOP_TIME_HPP_

#include <vector>
#include <string>
#include <iostream>
#include "../Bonhomme/Bonhomme.hpp"
#include "../Map.hpp"
#include "../Menu/Title.hpp"
#include "IScore_top.hpp"
#include "Static_info.hpp"
#include "Info.hpp"

class Score_top_time : public IScore_top
{
private :
  Map				*_data;
  Bonhomme			*_father;

  Texte				*_Kills_txt;
  int				_Kills_int;
  Texte				*_Kills_nb;

  Texte				*_Death_txt;
  int				_Death_int;
  Texte				*_Death_nb;

  double			_Sec_double;
  double			_Min_double;

  Texte				*_Sec_nb;
  Texte				*_Min_nb;
  Texte				*_Colon;
public :
  Score_top_time(Map *data, Bonhomme *father) : _data(data), _father(father)
  {
    _Kills_txt = data->create_texte("Kills:", 55, glm::vec4(1, 140.0f / 255.0f, 0, 1)); 
    _Kills_int = father->get_kills();
    _Kills_nb = data->create_texte(_Kills_int, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1));

    _Death_txt = data->create_texte("Death:", 55, glm::vec4(1, 140.0f / 255.0f, 0, 1)); 
    _Death_int = father->get_death();
    _Death_nb = data->create_texte(_Death_int, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1));

    _Colon = data->create_texte(":", 55, glm::vec4(1, 140.0f / 255.0f, 0, 1)); 

    _Min_double = (double)_data->get_mod().getIntMod("NbTime");
    _Sec_double = 0.00;
    _Min_nb = data->create_texte((int)_Min_double, 55, 1, glm::vec4(1, 140.0f / 255.0f, 0, 1));
    _Sec_nb = data->create_texte((int)_Sec_double, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1));

    _Colon->initialize();
    _Min_nb->initialize();
    _Sec_nb->initialize();
    _Death_txt->initialize();
    _Death_nb->initialize();
    _Kills_txt->initialize();
    _Kills_nb->initialize();
  }

  ~Score_top_time()
  {
    delete _Kills_txt;
    delete _Kills_nb;
    delete _Death_txt;
    delete _Death_nb;
    delete _Colon;
    delete _Min_nb;
    delete _Sec_nb;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    (void)clock;
    (void)input;
    int		oldsec;
    int		newsec;

    // gestion du temps;
    oldsec = (int)_Sec_double;

    if (!(_Min_double == (double)0.00  && _Sec_double == (double)0.00))
      _Sec_double -= clock.getElapsed();

    if (_Sec_double < 0)
      {
	if (_Min_double != (double)0.00)
	  {
	    _Sec_double += (double)60.00;
	    _Min_double -= (double)1.00;
	  }
	else
	  {
	    _Sec_double = (double)0.00;
	    _Min_double = (double)0.00;
	  }
	delete _Min_nb;
	_Min_nb = _data->create_texte((int)_Min_double, 55, 1, glm::vec4(1, 140.0f / 255.0f, 0, 1));	
	_Min_nb->initialize();
      }
    newsec = (int)_Sec_double;

    if (oldsec != newsec)
      {
	delete _Sec_nb;
	_Sec_nb = _data->create_texte((int)_Sec_double, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1));	
	_Sec_nb->initialize();
      }
    

    if (_father->get_kills() != _Kills_int)
      {
	_Kills_int = _father->get_kills();
	delete _Kills_nb;
	_Kills_nb = _data->create_texte(_Kills_int, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1)); 
	_Kills_nb->initialize();
      }
    if (_father->get_death() != _Death_int)
      {
	_Death_int = _father->get_death();
	delete _Death_nb;
	_Death_nb = _data->create_texte(_Death_int, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1)); 
	_Death_nb->initialize();
      }
    return (true);
  }

  virtual void	draw(gdl::AShader &shader)
  {
    _Death_txt->draw(shader, 20, 30);
    _Death_nb->draw(shader, 20 + 175, 30);

    _Kills_txt->draw(shader, 350, 30);
    _Kills_nb->draw(shader, 350 + 175, 30);

    _Min_nb->draw(shader, 750, 30);
    _Colon->draw(shader, 750 + 25, 30);
    _Sec_nb->draw(shader, 750 + 50, 30);
  }

  int		get_enemies(void) const {return (0);}
  double	get_time(void) const {return (_Min_double * (double)60 + _Sec_double);}
};

#endif
