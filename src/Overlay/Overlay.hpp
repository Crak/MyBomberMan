//
// Minimap.hpp for header in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May  8 17:41:17 2014 corentin bajdek
// Last update Sun May 18 23:26:15 2014 corentin bajdek
//

#ifndef OVERLAY_HPP_
# define OVERLAY_HPP_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "../Map.hpp"

class Overlay
{
protected :
  gdl::Texture	 		*_texture;
  gdl::Geometry			_geometry;
  gdl::BasicShader		_shader;
  glm::vec3			_position;
  glm::vec3			_rotation;
  glm::vec3			_scale;  
  Map				*_data;

  const std::string		_path;
  float				_scaling;

public :
  Overlay(const std::string path, Map *data) : _position(0, 0, 0), _rotation(0, 0, 0), _scale(0.01, 0.01, 1), _data(data), _path(path), _scaling(0.01) {}
  virtual ~Overlay();
  void			rotate(glm::vec3 const& axis, float angle);
  glm::mat4		getTransformation();
  virtual bool		initialize();
  virtual int		update(gdl::Clock const &clock, gdl::Input &input);
  virtual void		draw(gdl::AShader &shader);
};

#endif /* !OVERLAY_HPP_ */
