//
// Static_Info.hpp for l in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Overlay
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 18 21:28:22 2014 corentin bajdek
// Last update Sun Jun 15 22:37:10 2014 corentin bajdek
//

#ifndef STATIC_INFO_HPP_
# define STATIC_INFO_HPP_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "../Map.hpp"
#include "../Menu/Texte.hpp"

class Static_info
{
private :
  gdl::Texture	 		*_texture;
  gdl::Geometry			_geometry;
  gdl::BasicShader		_shader;
  glm::vec3			_position;
  glm::vec3			_rotation;
  glm::vec3			_scale;  
  Map				*_data;
  const std::string		_path;
  int				_quantity;
  Texte				*_mult;
  Texte				*_value;

public :
  Static_info(const std::string path, Map *data) : _position(0, 0, 0), _rotation(0, 0, 0), _scale(1, 1, 1), _data(data), _path(path), _quantity(0) {}
  ~Static_info() {}

  glm::mat4	getTransformation()
  {
    glm::mat4 transform(1);

    transform = glm::translate(transform, _position);

    transform = glm::scale(transform, _scale);

    transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
    transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
    transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
    return (transform);
  }

  bool	initialize()
  {
    _texture = _data->getTextContainer()->getTexture(_path);

    _geometry.pushVertex(glm::vec3(0, 50, 0));
    _geometry.pushVertex(glm::vec3(50, 50, 0));
    _geometry.pushVertex(glm::vec3(50, 0, 0));
    _geometry.pushVertex(glm::vec3(0, 0, 0));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));
    _geometry.build();
    _mult = _data->create_texte("x ", 33, glm::vec4(1, 140.0f / 255.0f, 0, 1));
    _value = _data->create_texte(_quantity, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1));
    _mult->initialize();
    _value->initialize();
    return true;
  }

  int			update(int quantity)
  {
    if (_quantity != quantity)
      {
	_quantity = quantity;
	_value = _data->create_texte(_quantity, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1));
	_value->initialize();
      }
    return (true);
  }

  void			draw(gdl::AShader &shader, double pos_x, double pos_y)
  {
    int			_width = 900;
    int			_length = 1000;
    glm::mat4		projection = glm::ortho(0.0f, (float)_width, (float)_length, 0.0f, -1.0f, 1.0f);
 

    shader.bind();
    shader.setUniform("view", glm::mat4(1));
    shader.setUniform("projection", projection);
    _texture->bind();
    _position.x = pos_x;
    _position.y = pos_y;

    /* activation de la transparence et desactivation du test de profondeur (inutile en 2D) */
    glAlphaFunc(GL_GREATER, 0.1f);
    glEnable(GL_ALPHA_TEST);
    _geometry.draw(shader, getTransformation(), GL_QUADS);

    /* remise en état */
    glDisable(GL_ALPHA_TEST);
    _mult->draw(shader, pos_x + 60, pos_y + 20);
    _value->draw(shader, pos_x + 60 + 20, pos_y);
  }
};

# endif /* !STATIC_INFO_HPP_*/
