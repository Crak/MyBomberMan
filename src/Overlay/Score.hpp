//
// Info.hpp for l in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Overlay
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 18 21:28:22 2014 corentin bajdek
// Last update Sun Jun 15 22:38:25 2014 corentin bajdek
//

#ifndef SCORE_HPP_
# define SCORE_HPP_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "Score_top.hpp"

class Score
{
private :
  gdl::Texture	 		*_texture;
  gdl::Geometry			_geometry;
  gdl::BasicShader		_shader;
  glm::vec3			_position;
  glm::vec3			_rotation;
  glm::vec3			_scale;  
  Map				*_data;
  Info				*_bombes;
  Info				*_fire;
  Info				*_throw;
  Info				*_kick;
  IScore_top			*_score_top;
  Bonhomme			*_father;

public :
  Score(Map *data, Bonhomme *father) : _position(0, 0, 0), _rotation(0, 0, 0), _scale(1, 1, 1), _data(data), _father(father) {}
  ~Score() {}

  bool	initialize()
  {
    _bombes = _data->create_info("./assets/Plus_bombe.tga", 50, 850);
    _fire = _data->create_info("./assets/Plus_range.tga", 50, 925);
    _throw = _data->create_info("./assets/Throw.tga", 250, 850);
    _kick = _data->create_info("./assets/Kick.tga", 250, 925);
    _score_top = _data->create_score_top(_father);

    _bombes->initialize();
    _fire->initialize();
    _throw->initialize();
    _kick->initialize();
    return (true);
  }
  
  int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    (void)input;
    (void)clock;

    if (_bombes->get_alive() == false)
      _bombes->set_alive(true);
    if (_bombes->update(clock, input, _father->get_bombes_max()) == true && _fire->get_alive() == false)
      _fire->set_alive(true);
    _fire->update(clock, input, _father->get_explosion_len());


    if (_throw->get_alive() == false)
      _throw->set_alive(true);
    if (_throw->update(clock, input, _father->get_nbthrow()) == true && _kick->get_alive() == false)
      _kick->set_alive(true);

    _kick->update(clock, input, _father->get_nbkick());
    _score_top->update(clock, input);
    return (true);
  }
  
  void	draw(gdl::AShader &shader)
  {
    _throw->draw(shader);
    _kick->draw(shader);
    _bombes->draw(shader);
    _fire->draw(shader);
    if (_kick->get_alive() == true)
      _score_top->draw(shader);
  }
  int		get_enemies(void) const {return (_score_top->get_enemies());}
  double	get_time(void) const {return (_score_top->get_time());}
};

#endif /* !SCORE_HPP_ */
