//
// Minimap.hpp for header in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May  8 17:41:17 2014 corentin bajdek
// Last update Tue Jun 10 00:45:14 2014 corentin bajdek
//

#ifndef MINIMAP_HPP_
# define MINIMAP_HPP_

#include "../AObject.hpp"
#include "../Map.hpp"
#include "Overlay_long.hpp"
#include "Overlay_top.hpp"
#include "../Bonhomme/Bonhomme.hpp"
#include "Score.hpp"
#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>

class Minimap
{
private :
  glm::vec3			_position;
  glm::vec3			_rotation;
  glm::vec3			_scale;  
  gdl::Texture			*_texture;
  gdl::Geometry			_geometry;
  gdl::BasicShader		_shader;
  std::vector<AObject *>	_obj;
  Overlay			*_overlay;
  Overlay_long			*_overlay_long;
  Overlay_top			*_overlay_top;
  Score				*_score;
  bool				_start;
  int				_pos_x;
  int				_pos_y;
  Map				*_data;
  Bonhomme			*_father;

public : 
  Minimap(int pos_x, int pos_y, Map *data, Bonhomme *father)
    : _position(0, 0, 0), _rotation(0, 0, 0), _scale(1, 1, 1), _start(false), _pos_x(pos_x), _pos_y(pos_y), _data(data), _father(father) {}

  virtual ~Minimap(void) 
   {
     delete _overlay;
     delete _overlay_long;
     delete _overlay_top;
     delete _score;
   }

  glm::mat4	getTransformation()
  {
    glm::mat4	transform(1); // On cree une matrice identite

    transform = glm::translate(transform, _position);
    transform = glm::scale(transform, _scale);
    transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
    transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
    transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
    return (transform);
  }

  virtual bool	initialize()
  {
    _geometry.pushVertex(glm::vec3(0, (250.0f / 10.0f), 0));
    _geometry.pushVertex(glm::vec3((250.0f / 10.0f), (250.0f / 10.0f), 0));
    _geometry.pushVertex(glm::vec3((250.0f / 10.0f), 0, 0));
    _geometry.pushVertex(glm::vec3(0, 0, 0));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f)); //coin haut gauche
    _geometry.pushUv(glm::vec2(1.0f, 0.0f)); //coin haut droite
    _geometry.pushUv(glm::vec2(1.0f, 1.0f)); //coin bas droite
    _geometry.pushUv(glm::vec2(0.0f, 1.0f)); //coin bas gauche
    _geometry.build();


    _overlay = _data->create_overlay();
    _overlay->initialize();
    _overlay_long = _data->create_overlay_long();
    _overlay_long->initialize();
    _overlay_top = _data->create_overlay_top();
    _overlay_top->initialize();
    _score = _data->create_score(_father);
    _score->initialize();
    return true;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    if ((_overlay->update(clock, input)) == true)
      _start = true;
    _score->update(clock, input);
    _overlay_long->update(clock, input);
    _overlay_top->update(clock, input);
    _obj = _data->Objects;
    (void)clock;
    (void)input;
    return (true);
  }

  bool		is_in_range(int objectx, int objecty)
  {
    if ((_pos_x + 5 >= objectx && _pos_x - 6 <= objectx) &&
	(_pos_y + 5 >= objecty && _pos_y - 5 <= objecty))
      return (true);
    return (false);
  }

  void		reverse_screen()
  {
    if (_position.y > 875)
      _position.y -= (2 *(_position.y - 875));
    else if (_position.y < 875)
      _position.y += (2 *(875 - _position.y));
  }

  void		load_texture(int type, int subtype)
  {
    if (type == WALL)
      {
	if (subtype == DEFAULT)
	  _texture = _data->getTextContainer()->getTexture("./assets/Yellow.tga");
	else
	  _texture = _data->getTextContainer()->getTexture("./assets/Box.tga");
      }
    if (type == BONHOMME)
      {
	if (subtype == _father->get_subtype())
	  _texture = _data->getTextContainer()->getTexture("./assets/J1.tga");
	else
	  _texture = _data->getTextContainer()->getTexture("./assets/J2.tga");
      }
    if (type == FIRE)
      _texture = _data->getTextContainer()->getTexture("./assets/Fire.tga");
    if (type == BOMBE)
      _texture = _data->getTextContainer()->getTexture("./assets/Bombe.tga");
  }

  virtual void	draw(gdl::AShader &shader, gdl::Clock const &clock)
  {
    (void)clock;
    int		_width = 900; // a envoyer en param
    int		_length = 1000; // a envoyer en param
    glm::mat4	projection = glm::ortho(0.0f, (float)_width, (float)_length, 0.0f, -1.0f, 1.0f);

    if (_start == false)
      {
	_overlay->draw(shader);
	_overlay_top->draw(shader);
	_overlay_long->draw(shader);
	_score->draw(shader);
	return ;
      }
    shader.bind();
    shader.setUniform("view", glm::mat4(1));
    shader.setUniform("projection", projection);
    _texture = _data->getTextContainer()->getTexture("./assets/Beige.tga");
    _texture->bind();
    _scale.x = 15.0f;
    _scale.y = 15.0f;
    _position.x = 900 - 250 - 50;
    _position.y = 1000 - 250;
    _geometry.draw(shader, getTransformation(), GL_QUADS);
    _scale.x = 1.0f;
    _scale.y = 1.0f;
    glDisable(GL_DEPTH_TEST);
    for (size_t i = 0; i < _obj.size(); i++)
      if (is_in_range(_obj[i]->get_pos_x(), _obj[i]->get_pos_y()) == true)
	if (_obj[i]->get_type() == WALL || _obj[i]->get_type() == BONHOMME ||
	    _obj[i]->get_type() == FIRE || _obj[i]->get_type() == BOMBE)
	  {
	    _position.x = (_obj[i]->get_pos_x() - _pos_x + 5) * 25 + 900 - 250;
	    _position.y = (_obj[i]->get_pos_y() - _pos_y + 5) * 25 + 1000 - 250;
	    //pour inverser la map
	    this->reverse_screen();
	    // pour activer la transparence
	    glAlphaFunc(GL_GREATER, 0.1f);
	    glEnable(GL_ALPHA_TEST);
	    //pour load la texture
	    load_texture(_obj[i]->get_type(), _obj[i]->get_subtype());
	    _texture->bind();
	    _geometry.draw(shader, getTransformation(), GL_QUADS);
	  }
    glDisable(GL_ALPHA_TEST);
    glEnable(GL_DEPTH_TEST);
    _overlay->draw(shader);
    _overlay_top->draw(shader);
    _overlay_long->draw(shader);
    _score->draw(shader);
  }

  /*
  ** setters & getters
  */
  int		get_pos_x(void) const { return (this->_pos_x) ; }
  int		get_pos_y(void) const { return (this->_pos_y) ; }
  int		get_enemies(void) const {return (_score->get_enemies());}
  double	get_time(void) const {return (_score->get_time());}

  void		set_pos_x(int pos_x) { this->_pos_x = pos_x ; }
  void		set_pos_y(int pos_y) { this->_pos_y = pos_y ; }
};

#endif /* !MINIMAP_HPP_  */
