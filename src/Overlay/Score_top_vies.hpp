//
// Score_top_vies.hpp for score in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon Jun  9 00:06:56 2014 corentin bajdek
// Last update Tue Jun 10 00:50:20 2014 corentin bajdek
//


#ifndef SCORE_TOP_VIES_HPP_
# define SCORE_TOP_VIES_HPP_

#include <vector>
#include <string>
#include <iostream>
#include "../Bonhomme/Bonhomme.hpp"
#include "../Map.hpp"
#include "../Menu/Title.hpp"
#include "IScore_top.hpp"
#include "Static_info.hpp"
#include "Info.hpp"

class Score_top_vies : public IScore_top
{
private :
  Map				*_data;
  Bonhomme			*_father;
  //kills
  Texte				*_Kills_txt;
  int				_Kills_int;
  Texte				*_Kills_nb;

  //vie
  Static_info			*_Vies;
  int				_Vies_int;

  Static_info			*_Enemis; // nombre d'enemis en vie
  int				_Enemis_int;
public :
  Score_top_vies(Map *data, Bonhomme *father) : _data(data), _father(father)
  {
    _Kills_txt = data->create_texte("Kills:", 55, glm::vec4(1, 140.0f / 255.0f, 0, 1)); 
    _Kills_int = father->get_kills();
    _Kills_nb = data->create_texte(_Kills_int, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1));

    _Enemis =  new Static_info("./assets/J2.tga", data);
    _Enemis_int = 0;

    _Vies =  new Static_info("./assets/J1.tga", data);
    _Vies_int = 0;

    _Vies->initialize();
    _Enemis->initialize();
    _Kills_txt->initialize();
    _Kills_nb->initialize();
  }

  ~Score_top_vies()
  {
    delete _Kills_txt;
    delete _Kills_nb;

    delete _Enemis;
    delete _Vies;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		current_enemis = 0;

    (void)clock;
    (void)input;

    if (_father->get_kills() != _Kills_int)
      {
	_Kills_int = _father->get_kills();
	delete _Kills_nb;
	_Kills_nb = _data->create_texte(_Kills_int, 55, 2, glm::vec4(1, 140.0f / 255.0f, 0, 1)); 
	_Kills_nb->initialize();
      }
    if (_father->get_life() != _Vies_int)
      {
	_Vies_int = _father->get_life();
	_Vies->update(_Vies_int);
      }

    for (size_t i = 0 ; i < _data->Objects.size() ; i++)
      {
	if (_data->Objects[i]->get_type() == BONHOMME && _data->Objects[i] != _father)
	  current_enemis++;
      }
    if (current_enemis != _Enemis_int)
      {
	_Enemis_int = current_enemis;
	_Enemis->update(_Enemis_int);
      }
    return (true);
  }

  virtual void	draw(gdl::AShader &shader)
  {
    _Vies->draw(shader, 30, 30);

    _Kills_txt->draw(shader, 300, 30);
    _Kills_nb->draw(shader, 300 + 175, 30);

    _Enemis->draw(shader, 700, 30);
  }
  int		get_enemies(void) const {return (_Enemis_int);}
  double	get_time(void) const {return (0.00);}
};

#endif /* */
