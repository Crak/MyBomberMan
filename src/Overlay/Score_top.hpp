//
// Info.hpp for l in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Overlay
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 18 21:28:22 2014 corentin bajdek
// Last update Thu Jun 12 18:52:38 2014 corentin bajdek
//

#ifndef SCORE_TOP_HPP_
# define SCORE_TOP_HPP_

#include <vector>
#include <string>
#include <iostream>
#include "../Bonhomme/Bonhomme.hpp"
#include "../Map.hpp"
#include "../Menu/Title.hpp"
#include "IScore_top.hpp"
#include "Info.hpp"
#include "Static_info.hpp"

class Score_top : public IScore_top
{
private :
  Map				*_data;
  Bonhomme			*_father;
  Texte				*_Score_txt; // le mot "SCORE :"
  int				_Score_int; // le score réel en int
  Texte				*_Score_nb; // le score réel en texte

  Static_info			*_Vies;
  int				_Vies_int;

  Static_info			*_Enemis; // nombre d'enemis en vie
  int				_Enemis_int;

public :
  Score_top(Map *data, Bonhomme *father) : _data(data), _father(father)
  {
    _Score_txt = data->create_texte("SCORE:", 55,  glm::vec4(1, 140.0f / 255.0f, 0, 1)); 
    _Score_int = father->get_score();
    _Score_nb = data->create_texte(_Score_int, 55, 5, glm::vec4(1, 140.0f / 255.0f, 0, 1)); 

    _Enemis =  new Static_info("./assets/J2.tga", data);
    _Enemis_int = 0;

    _Vies =  new Static_info("./assets/J1.tga", data);
    _Vies_int = 0;

    _Vies->initialize();
    _Enemis->initialize();
    _Score_txt->initialize();
    _Score_nb->initialize();
  }
  ~Score_top() 
  {
    delete _Score_txt;
    delete _Score_nb;

    delete _Enemis;
    delete _Vies;
  }
  
  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		current_enemis = 0;

    (void)clock;
    (void)input;
    if (_father->get_score() != _Score_int)
      {
	_Score_int = _father->get_score();
	delete _Score_nb;
	_Score_nb = _data->create_texte(_Score_int, 55, 5, glm::vec4(1, 140.0f / 255.0f, 0, 1)); 
	_Score_nb->initialize();
      }
    if (_father->get_life() != _Vies_int)
      {
	_Vies_int = _father->get_life();
	_Vies->update(_Vies_int);
      }

    for (size_t i = 0 ; i < _data->Objects.size() ; i++)
      {
	if (_data->Objects[i]->get_type() == BONHOMME && _data->Objects[i] != _father)
	  current_enemis++;
      }
    if (current_enemis != _Enemis_int)
      {
	_Enemis_int = current_enemis;
	_Enemis->update(_Enemis_int);
      }
    return (true);
  }
  
  virtual void	draw(gdl::AShader &shader)
  {
    _Vies->draw(shader, 30, 30);

    _Score_txt->draw(shader, 260, 30);
    _Score_nb->draw(shader, 260 + 170, 30);

    _Enemis->draw(shader, 700, 30);
  }
  int		get_enemies(void) const {return (_Enemis_int);}
  double	get_time(void) const {return (0.00);}
};

#endif /* !SCORE_TOP_HPP_ */
