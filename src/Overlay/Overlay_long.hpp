//
// Minimap.hpp for header in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May  8 17:41:17 2014 corentin bajdek
// Last update Thu May 22 18:10:00 2014 corentin bajdek
//
#ifndef OVERLAY_LONG_HPP_
# define OVERLAY_LONG_HPP_

#include "Overlay.hpp"

class Overlay_long : public Overlay
{
public :
  Overlay_long(const std::string path, Map *data) :
    Overlay(path, data) {}
  virtual ~Overlay_long() {}

  virtual bool		initialize()
  {
    _texture = _data->getTextContainer()->getTexture(_path);

    _geometry.pushVertex(glm::vec3(0, 350, 0));
    _geometry.pushVertex(glm::vec3(805, 350, 0));
    _geometry.pushVertex(glm::vec3(805, 0, 0));
    _geometry.pushVertex(glm::vec3(0, 0, 0));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));
    _geometry.build();
    return true;
  }

  virtual int		update(gdl::Clock const &clock, gdl::Input &input)
  {
    (void)input;
    if (_scaling != 0.8)
      _scaling += (clock.getElapsed() * 1.1);
    if (_scaling > 0.8)
      _scaling = 0.8;
    _scale.x = _scaling;
    _scale.y = 0.8;
    return (true);
  }


  virtual void		draw(gdl::AShader &shader)
  {
    _texture->bind();
    _position.x = 0;
    _position.y = 750;

    /* activation de la transparence et desactivation du test de profondeur (inutile en 2D) */
    glDisable(GL_DEPTH_TEST);
    glAlphaFunc(GL_GREATER, 0.1f);
    glEnable(GL_ALPHA_TEST);
    _geometry.draw(shader, getTransformation(), GL_QUADS);
    /* remise en état */
    glDisable(GL_ALPHA_TEST);
    glEnable(GL_DEPTH_TEST);
  }
};

#endif /* !OVERLAY_LONG_HPP_ */
