//
// Bonhomme.cpp for cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:22:26 2014 corentin bajdek
// Last update Sun Jun 15 22:59:01 2014 corentin bajdek
//

#include "Bonhomme.hpp"
#include "../Overlay/Minimap.hpp"

Bonhomme::Bonhomme(int pos_x, int pos_y, int team, Map *data, int id, const std::string name, glm::vec4 vec) :
  AObject(pos_x, pos_y, data), _name(name), _team(team), _vec(vec),
  _special(NONE), _nbkick(0), _nbthrow(0), _score(0), _kills(0), _invincibility(0.00),
  _animupdated(false), _death(0), _id(id)
{
  int		mode;

  mode = _data->get_mod().getIntMod("GameMode");
  if (mode == MODE_CLASSIC || mode == MODE_HISTOIRE)
    _life = 1;
  else if (mode == MODE_VIE)
    _life = _data->get_mod().getIntMod("NbLife");
  else
    _life = -1;

  _speed = 2.5;
  _type = BONHOMME;
  _bombes_max = 1;
  _explosion_len = 1;
  _double_pos_x = (double)_pos_x;
  _double_pos_y = (double)_pos_y;
  _name_2D = _data->create_texte(_name, 20);
  _size = 0.2;
  _passiv_state = IDLE_OFF;
  _passiv_anim = -1;
  _activ_anim = -1;
  _spawn.x = pos_x;
  _spawn.y = pos_y;
  return ; 
}

Bonhomme::~Bonhomme() 
{
  if (_subtype == DEFAULT)
    _data->delete_minimap(DEFAULT);
  return ; 
}

void	Bonhomme::chooseAnim(void)
{
  if (_activ_anim < 0)
    {
      if (_passiv_state == IDLE_OFF)
	{
	  _model.setCurrentSubAnim(std::string("Idle"), false);
	  _passiv_state = IDLE_ON;
	}
      if (_passiv_state == WALKING && _passiv_anim < 0)
	{
	  _passiv_anim = (54 - 34) * _model.getFrameDuration();
	  _model.setCurrentSubAnim(std::string("Walk"), false);    
	}
    }
}

bool	Bonhomme::initialize(void)
{
  if (_subtype == J2)
    _data->get_minimap(J2)->initialize();
  else if (_subtype == DEFAULT)
    {
      _data->create_minimap(DEFAULT, this);
      _data->get_minimap(DEFAULT)->initialize();
    }
  if (!(_model.load(std::string("./assets/marvin.fbx"))))
    return (false);
  if (!(_model.createSubAnim(0, std::string("Walk"), 34, 54)))
    return (false);
  if (!(_model.createSubAnim(0, std::string("Idle"), 1, 1)))
    return (false);
  if (!(_model.createSubAnim(0, std::string("Kick"), 77, 82)))
    return (false);
  _model.setCurrentSubAnim(std::string("Idle"), false);
  scale(glm::vec3(0.002, 0.002, 0.002));
  rotate(glm::vec3(1, 0, 0), 90);
  _initialize = true;
  return (true);
}

int		Bonhomme::update(gdl::Clock const &clock, gdl::Input &input)
{
  //multi_move
  multi_move = 1;
  if ((input.getKey(SDLK_UP) || input.getKey(SDLK_DOWN)) &&
      !(input.getKey(SDLK_UP) && input.getKey(SDLK_DOWN)) &&
      (input.getKey(SDLK_LEFT) || input.getKey(SDLK_RIGHT)) &&
      !(input.getKey(SDLK_LEFT) && input.getKey(SDLK_RIGHT)))
    multi_move = 1.4142;

  //animations
  if (_passiv_anim >= 0)
    _passiv_anim -= clock.getElapsed();
  if (_activ_anim >= 0)
    _activ_anim -= clock.getElapsed();
  if (_passiv_state == WALKING)
    _passiv_state = IDLE_OFF;
  _animupdated = false;
  
  // gestion d'input
  if (input.getKey(SDLK_UP))
    this->move(UP, clock);
  if (input.getKey(SDLK_DOWN))
    this->move(DOWN, clock);
  if (input.getKey(SDLK_LEFT))
    this->move(LEFT, clock);
  if (input.getKey(SDLK_RIGHT))
    this->move(RIGHT, clock);
  if (input.getKey(SDLK_RETURN) && _bombes_max > 0)
    this->drop_bomb();
  if (input.getKey(SDLK_EXCLAIM) && _special != NONE)
    use_special();

  //update de l'interface minimap & co
  _data->get_minimap(DEFAULT)->set_pos_x(_pos_x);
  _data->get_minimap(DEFAULT)->set_pos_y(_pos_y);

  //invincibilité
  if (_invincibility < 0.00)
    _invincibility = 0.00;
  else if (_invincibility > 0.00)
    _invincibility -= clock.getElapsed();
  //mort
  if (_dying == true)
    return (false);
  return (true);
}

void		Bonhomme::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;
  double	delta;

  if (_animupdated == false)
    {
      _animupdated = true;
      chooseAnim();
      delta = clock.getElapsed();
    }
  else
    delta = 0;

  translate(glm::vec3(_double_pos_x, _double_pos_y, 0));
  _model.draw(shader, getTransformation(), delta);
  translate(glm::vec3(-(_double_pos_x), -(_double_pos_y), 0));
}

void		Bonhomme::drop_bomb()
{
  AObject	*ptr;

  for (size_t i = 0; i < _data->Objects.size(); ++i)
    {
      if (_data->Objects[i]->get_pos_x() == _pos_x && _data->Objects[i]->get_pos_y() == _pos_y)
	{
	  if (_data->Objects[i]->get_type() != BONHOMME)
	    return ;
	}
    }
  ptr = _data->create_bombes(_pos_x, _pos_y, 3, this, _explosion_len);
  _data->Objects.push_back(ptr);
  _bombes_max -= 1;
}

int		Bonhomme::Harr(double value, int to_do)
{
  value += 0.5;
  if (to_do == 0 && (int)(value * 8) % 8 <= 1)
    return (int)(value) - 1;
  if (to_do == 1 && (int)(value * 8) % 8 >= 6)
    return (int)(value) + 1;
  return ((int)(value));
}

void		Bonhomme::move(int direction,
			       gdl::Clock const &clock)
{
  double	save_x;
  double	save_y;
  Bonus		*bonus;
  double	to_move;

  _passiv_state = WALKING;
  save_x = _double_pos_x;
  save_y = _double_pos_y;
  _direction = direction;
  to_move = _speed * clock.getElapsed() / multi_move;
  to_move = (to_move < 0) ? 0 : ((to_move > 1) ? 1 : to_move);
  if (direction == UP)
    {
      _rotation.y = 180;
      _double_pos_y += to_move;
    }
  if (direction == DOWN)
    {
      _rotation.y = 0;
      _double_pos_y -= to_move;
    }
  if (direction == LEFT)
    {
      _rotation.y = 270;
      _double_pos_x -= to_move;
    }
  if (direction == RIGHT)
    {
      _rotation.y = 90;
      _double_pos_x += to_move;
    }
  _pos_x = ARR(_double_pos_x);
  _pos_y = ARR(_double_pos_y);

  for (size_t i = 0; i < _data->Objects.size(); ++i)
    {
      if (((_pos_x != ARR(save_x) || _pos_y != ARR(save_y))) &&
	  _data->Objects[i]->get_pos_x() == _pos_x && _data->Objects[i]->get_pos_y() == _pos_y)
	{
	  if (_data->Objects[i]->get_type() == BONUS)
	    {
	      if ((bonus = dynamic_cast<Bonus *>(_data->Objects[i])) == NULL)
		std::cerr << "Impossible error happened" << std::endl;
	      bonus->is_touched_by_human(this);
	    }
	  if (_data->Objects[i]->get_type() == BOMBE)
	    {
	      _double_pos_x = save_x;
	      _double_pos_y = save_y;
	      _pos_x = ARR(_double_pos_x);
	      _pos_y = ARR(_double_pos_y);
	    }
	}
      if ((_data->Objects[i]->get_type() == WALL) &&
	  ((_data->Objects[i]->get_pos_x() == Harr(_double_pos_x, !(_double_pos_x < save_x)) &&
	    _data->Objects[i]->get_pos_y() == _pos_y) ||
	   (_data->Objects[i]->get_pos_y() == Harr(_double_pos_y, !(_double_pos_y < save_y)) &&
	    _data->Objects[i]->get_pos_x() == _pos_x)))
	{
	  _double_pos_x = save_x;
	  _double_pos_y = save_y;
	  _pos_x = ARR(_double_pos_x);
	  _pos_y = ARR(_double_pos_y);
	}
    }
}
  
void		Bonhomme::is_touched_by_fire(Bonhomme *father)
{
  if (_invincibility != 0.00)
    return ;
  if (father != this)
    {
      father->set_kills(father->get_kills() + 1);
      father->set_score(father->get_score() + 500);
    }
  else
    this->set_score(this->get_score() - 500);
  if (_life > 0)
    _life--;
  _death++;
  if (_life == 0)
    _dying = true;
  else
    {
      _pos_x = _spawn.x;
      _pos_y = _spawn.y;
      _double_pos_x = (double)_pos_x;
      _double_pos_y = (double)_pos_y;
    }
  _invincibility = 1.00;
  return ;
}

void		Bonhomme::use_special()
{
  Bombes	*bombe;

  for (size_t i = 0; i < _data->Objects.size(); ++i)
    {
      if ((_direction == RIGHT && _data->Objects[i]->get_pos_x() == _pos_x + 1 && _data->Objects[i]->get_pos_y() == _pos_y) ||
	  (_direction == LEFT && _data->Objects[i]->get_pos_x() == _pos_x - 1 && _data->Objects[i]->get_pos_y() == _pos_y) ||
	  (_direction == UP && _data->Objects[i]->get_pos_x() == _pos_x && _data->Objects[i]->get_pos_y() == _pos_y + 1) ||
	  (_direction == DOWN && _data->Objects[i]->get_pos_x() == _pos_x && _data->Objects[i]->get_pos_y() == _pos_y - 1))
	{
	  if (_data->Objects[i]->get_type() == BOMBE)
	    {
	      if ((bombe = dynamic_cast<Bombes *>(_data->Objects[i])) == NULL)
		std::cerr << "Impossible error happened" << std::endl;
	      if (_special == KICK && _nbkick > 0)
		{
		  _passiv_state = IDLE_OFF;
		  _model.setCurrentSubAnim(std::string("Kick"), false);
		  _activ_anim = (80 - 75) * _model.getFrameDuration();
		  bombe->is_kicked(_direction, this);
		  _nbkick--;
		}
	      if (_special == THROW && _nbthrow > 0)
		{
		  bombe->is_thrown(_direction, this);
		  _nbthrow--;
		}
	    }
	  
	}
    }
}



/*
** Setters & Getters
*/

int		Bonhomme::get_explosion_len() const { return (this->_explosion_len); }
float		Bonhomme::get_speed(void) const { return (this->_speed) ; }
void		Bonhomme::set_explosion_len(int __explosion_len) { this->_explosion_len = __explosion_len ; }
void		Bonhomme::set_speed(float __speed) { this->_speed = __speed ; }
void		Bonhomme::set_kills(int nb) { _kills = nb; }
void		Bonhomme::set_score(int nb) 
{
  _score = nb;
  if (_score < 0)
    _score = 0;
}

int		Bonhomme::get_score(void) const { return (_score); }
void		Bonhomme::set_nbkick(int nb) {_nbkick = nb; }
int		Bonhomme::get_nbkick(void) const { return _nbkick;}
void		Bonhomme::set_nbthrow(int nb) {_nbthrow = nb; }
int		Bonhomme::get_nbthrow(void) const { return _nbthrow ;}
int		Bonhomme::get_kills(void) const { return _kills ;}
int		Bonhomme::get_life(void) const { return _life ;}
int		Bonhomme::get_death(void) const { return _death ;}
int		Bonhomme::get_id(void) const {return _id;}


int		Bonhomme::get_bombes_max(void) const
{
  return (_bombes_max);
}

void		Bonhomme::set_bombes_max(int bombes)
{
  _bombes_max = bombes;
}

double		Bonhomme::get_d_pos_x(void) const
{
  return (_double_pos_x);
}

double		Bonhomme::get_d_pos_y(void) const
{
  return (_double_pos_y);
}

int		Bonhomme::get_special(void) const
{
  return (_special);
}

void		Bonhomme::set_special(int special) { _special = special; }

void		Bonhomme::save(std::ofstream &ofs) const
{
  ofs << "Bonhomme" << "::";
  ofs << _subtype << "::";
  ofs << _id << "::";
  ofs << _pos_x << "::";
  ofs << _pos_y << "::";
  ofs << _speed << "::";  
  ofs << _bombes_max << "::";    
  ofs << _explosion_len << "::";    
  ofs << _special << "::";    
  ofs << _nbkick << "::";    
  ofs << _nbthrow << "::";
  ofs << _score << "::";
  ofs << _kills << "::";
  ofs << _life << "::";
  return ;
}

void		Bonhomme::load(std::vector< double> &values)
{
  _pos_x = (int)values[2];
  _pos_y = (int)values[3];
  _double_pos_x = (double)_pos_x;
  _double_pos_y = (double)_pos_y;
  _speed = values[4];
  _bombes_max = (int)values[5];
  _explosion_len = (int)values[6];
  _special = (int)values[7];
  _nbkick = (int)values[8];
  _nbthrow = (int)values[9];
  _score = (int)values[10];
  _kills = (int)values[11];
  _life = (int)values[12];
  _spawn.x = _pos_x;
  _spawn.y = _pos_y;
}
