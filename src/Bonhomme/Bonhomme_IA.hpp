//
// Cube.hpp for Cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:49:38 2014 corentin bajdek
// Last update Sun Jun 15 22:59:31 2014 corentin bajdek
//

#ifndef BONHOMME_IA_HPP_
# define BONHOMME_IA_HPP_

#include <vector>
#include <Model.hh>
#include <Geometry.hh>
#include <Texture.hh>
#include <stdio.h>
#include "Bonhomme.hpp"

class Bonhomme_IA : public Bonhomme
{
private:
  int		_obj_x;
  int		_obj_y;
  int		_bool;
public:
  Bonhomme_IA(int pos_x, int pos_y, int team, Map *data, int id) :
    Bonhomme(pos_x, pos_y, team, data, id), _obj_x(-42), _obj_y(-42), _bool(0)
  {
    _subtype = IA;
    multi_move = 1;
    return ; 
  }

  virtual void mapping(std::vector< std::vector< std::vector<int> > > &map)
  {
    Bombes     *bombe; 
    int	       type;
    int	       len;

    map.resize(_data->get_length());
    for (int i = 0; i < _data->get_length(); i++)
      {    
	map[i].resize(_data->get_width());
	for (int j = 0; j < _data->get_width(); j++)
	  map[i][j].resize(3);
      }

    for (size_t i = 0; i < _data->Objects.size(); ++i)
      {
  	type = _data->Objects[i]->get_type();
	if (type == BOMBE)
	  {
	    if (_data->Objects[i]->get_pos_x() >= 0 && 
		_data->Objects[i]->get_pos_x() < _data->get_length() && 
		_data->Objects[i]->get_pos_y() >= 0 && 
		_data->Objects[i]->get_pos_y() < _data->get_width())
	      {
		map[_data->Objects[i]->get_pos_x()][_data->Objects[i]->get_pos_y()][0] = -2;
		if ((bombe = dynamic_cast<Bombes *>(_data->Objects[i])) == NULL)
		  std::cerr << "Impossible error happened" << std::endl;
		len = bombe->get_max_len();
	    
		for (int j = _data->Objects[i]->get_pos_x() - len;
		     j <= _data->Objects[i]->get_pos_x() + len; j++)
		  if (j >= 0 && j < _data->get_length())
		    map[j][_data->Objects[i]->get_pos_y()][1] = -1;
	    
		for (int j = _data->Objects[i]->get_pos_y() - len; 
		     j <= _data->Objects[i]->get_pos_y() + len; j++)
		  if (j >= 0 && j < _data->get_width())
		    map[_data->Objects[i]->get_pos_x()][j][1] = -1;
	      }
	  }
	else if (type == FIRE)
	  map[_data->Objects[i]->get_pos_x()][_data->Objects[i]->get_pos_y()][1] = -1;
	else if (type == WALL || type == WALL_DESTRUCTIBLE)
	  map[_data->Objects[i]->get_pos_x()][_data->Objects[i]->get_pos_y()][0] = -2; 
	else if (type == BONUS)
	  map[_data->Objects[i]->get_pos_x()][_data->Objects[i]->get_pos_y()][2] = 1; 
      }
  }

  //
  //
  void  aff_map(std::vector< std::vector< std::vector<int> > > &map)
  {
    for (int i = 0; i < _data->get_length(); i++)
      {
	for (int j = 0; j < _data->get_width(); j++)
	  printf("%3d", map[j][i][0]);
	printf("\n");
      }
    printf("\n");
    for (int i = 0; i < _data->get_length(); i++)
      {
	for (int j = 0; j < _data->get_width(); j++)
	  if (j == _obj_x && i == _obj_y)
	    printf("%3s", "Ob");
	  else if (j == _pos_x && i == _pos_y)
	    printf("%3c", 'X');
	  else
	    printf("%3d", map[j][i][1]);
	printf("\n");
      }
    printf("\n");
    for (int i = 0; i < _data->get_length(); i++)
      {
	for (int j = 0; j < _data->get_width(); j++)
	  printf("%3d", map[j][i][2]);
	printf("\n");
      }
    printf("\n\n");
  }

  virtual int	my_look_around(std::vector< std::vector< std::vector<int> > > &map, int x, int y)
  {
    int   ret;

    ret = -42;
    if (x > 0 && map[x - 1][y][0] > 0)
      if (ret > map[x - 1][y][0] || ret == -42)
	ret = map[x - 1][y][0];

    if (y > 0 && map[x][y - 1][0] > 0)
      if (ret > map[x][y - 1][0] || ret == -42)
	ret = map[x][y - 1][0];

    if (x != 15 && map[x + 1][y][0] > 0)
      if (ret > map[x + 1][y][0] || ret == -42)
	ret = map[x + 1][y][0];

    if (y != 15 && map[x][y + 1][0] > 0)
      if (ret > map[x][y + 1][0] || ret == -42)
	ret = map[x][y + 1][0];

    return (ret + 1);
  }
  
  virtual void	my_lee(std::vector< std::vector< std::vector<int> > > &map, int x, int y)
  {
    int           ret;

    _bool++;
    if ((map[x][y][0] >= 0 && map[x][y][1] != -1) || (_bool == 1))
      {
	if (_bool == 1)
	  map[x][y][0] = 1;
	else if (map[x][y][0] >= 0)
	  {
	    ret =  my_look_around(map, x, y);
	    if (ret < map[x][y][0] || map[x][y][0] == 0)
	      map[x][y][0] = ret;
	    else
	      return ;
	  }

	if (x != _data->get_width() && map[x + 1][y][0] >= 0)
	  my_lee(map, x + 1, y);
	if (y != _data->get_length() && map[x][y + 1][0] >= 0)
	  my_lee(map, x, y + 1);
	if (x > 0 && map[x - 1][y][0] >= 0)
	  my_lee(map, x - 1, y);
	if (y > 0 && map[x][y - 1][0] >= 0)
	  my_lee(map, x, y - 1);
      }
    return ;
  }

  virtual void	my_lee2(std::vector< std::vector< std::vector<int> > > &map, int x, int y)
  {
    int           ret;

    _bool++;
    if ((map[x][y][0] >= 0) || (_bool == 1))
      {
	if (_bool == 1)
	  map[x][y][0] = 1;
	else if (map[x][y][0] >= 0)
	  {
	    ret =  my_look_around(map, x, y);
	    if (ret < map[x][y][0] || map[x][y][0] == 0)
	      map[x][y][0] = ret;
	    else
	      return ;
	  }

	if (x != _data->get_width() && map[x + 1][y][0] >= 0)
	  my_lee2(map, x + 1, y);
	if (y != _data->get_length() && map[x][y + 1][0] >= 0)
	  my_lee2(map, x, y + 1);
	if (x > 0 && map[x - 1][y][0] >= 0)
	  my_lee2(map, x - 1, y);
	if (y > 0 && map[x][y - 1][0] >= 0)
	  my_lee2(map, x, y - 1);
      }
    return ;
  }

  virtual int	lee_move(std::vector< std::vector< std::vector<int> > > &map, int x, int y)
  {
    int		ret;

    ret = -1;
    if (x <= 0 || y <= 0)
      return (ret);
    if (map[_obj_x][_obj_y][0] <= 1)
      return (ret);
    if (map[x][y][0] == 2)
      {
	if (map[x + 1][y][0] == 1)
	  return (LEFT);
	if (map[x - 1][y][0] == 1)
	  return (RIGHT);
	if (map[x][y + 1][0] == 1)
	  return (DOWN);
	if (map[x][y - 1][0] == 1)
	  return (UP);
      }

    if (x != _data->get_width() && map[x + 1][y][0] == map[x][y][0] - 1)
      if ((ret = lee_move(map, x + 1, y)) != -1)
	return (ret);

    if (y != _data->get_length() && map[x][y + 1][0] == map[x][y][0] - 1)
      if ((ret = lee_move(map, x, y + 1)) != -1)
	return (ret);

    if (x > 0 && map[x - 1][y][0] == map[x][y][0] - 1)
      if ((ret = lee_move(map, x - 1, y)) != -1)
	return (ret);

    if (y > 0 && map[x][y - 1][0] == map[x][y][0] - 1)
      if ((ret = lee_move(map, x, y - 1)) != -1)
	return (ret);
    
    return (ret);
  }

  virtual int	move_ia(std::vector< std::vector< std::vector<int> > > &map, int x, int y)
  {
    int		ret;
    double	dbl_x;
    double	dbl_y;

    ret = lee_move(map, x, y);
    dbl_x = _double_pos_x - (double)_pos_x;
    dbl_y = _double_pos_y - (double)_pos_y;
    if (ret == UP || ret == DOWN)
    {
	if (dbl_x < 0)
	  {
	    if (dbl_x < -0.10)
	      return (RIGHT);
	  }
	else if (dbl_x > 0.10)
	  return (LEFT);
    }
    else if (ret == LEFT || ret == RIGHT)
      {
	if (dbl_y < 0)
	  {
	    if (dbl_y < -0.10)
	      return (UP);
	  }
	else if (dbl_y > 0.10)
	  return (DOWN);
      }
    return (ret);
  }

  virtual int	is_the_way_safe(std::vector< std::vector< std::vector<int> > > &map, int x, int y)
  {
    int		ret;

    ret = -1;
    if (map[x][y][1] < 0)
      return (ret);
    if (map[x][y][0] == 2)
      return (1);
    if (x != _data->get_width() && map[x + 1][y][0] == map[x][y][0] - 1)
      if ((ret = is_the_way_safe(map, x + 1, y)) != -1)
	return (ret);

    if (y != _data->get_length() && map[x][y + 1][0] == map[x][y][0] - 1)
      if ((ret = is_the_way_safe(map, x, y + 1)) != -1)
	return (ret);

    if (x > 0 && map[x - 1][y][0] == map[x][y][0] - 1)
      if ((ret = is_the_way_safe(map, x - 1, y)) != -1)
	return (ret);

    if (y > 0 && map[x][y - 1][0] == map[x][y][0] - 1)
      if ((ret = is_the_way_safe(map, x, y - 1)) != -1)
	return (ret);
    return (ret);
  }

  virtual int	max_lee(std::vector< std::vector< std::vector<int> > > &map)
  {
    int		max;

    max = 1;
    for (int i = 0; i < _data->get_length(); i++)
      {
	for (int j = 0; j < _data->get_width(); j++)
	  if (map[i][j][0] > max)
	    max = map[i][j][0];
      }
    return (max);
  }

  virtual int	try_set_obj(std::vector< std::vector< std::vector<int> > > &map, int dist)
  {
    if (dist == 1 || dist == 0)
      return (-1);
    
    for (int i = 0; i < _data->get_length(); i++)
      {
	for (int j = 0; j < _data->get_width(); j++)
	  if (map[i][j][0] == dist)
	    {
	      if (map[i][j][1] != -1)
		{
		  _obj_x = i;
		  _obj_y = j;
		  return (1);
		}
	    }
      }
    return (-1);
  }

  virtual void	set_objectif(std::vector< std::vector< std::vector<int> > > &map)
  {
    int		max;
    int		nbr;

    max = max_lee(map);
    nbr = rand() % max;
    if (nbr == 0 || nbr == 1)
      nbr = max;
    if ((_obj_x != -42 && _obj_y != -42) && (map[_pos_x][_pos_y][1] == -1))
      {
	my_lee2(map, _pos_x, _pos_y);
	_bool = 0;
	for (int i = 2; i <= max; i++)
	  {
	    if (try_set_obj(map, i) == 1)
	      return ;
	  }
      }
    else if ((((_obj_x == -42 || _obj_y == -42) ||
	       (_obj_x == _pos_x && _obj_y == _pos_y) ||
               (map[_obj_x][_obj_y][1] == -1)) ||
              (is_the_way_safe(map, _obj_x, _obj_y) != 1)) && max > 1)
      {
	if (_data->get_mod().getIntMod("Difficulty") == 2)
	  {
	    for (int i = 0; i < _data->get_length(); i++)
	      {
		for (int j = 0; j < _data->get_width(); j++)
		  {
		    if ((map[i][j][2] == 1) && (is_the_way_safe(map, i, j) == 1))
		      {
			_obj_x = i;
			_obj_y = j;
			return ;
		      }
		  }
	      }
	  }

	for (int i = 0; i < _data->get_length(); i++)
	  {
	    for (int j = 0; j < _data->get_width(); j++)
	      {
		if (map[i][j][0] == nbr)
		  {
		    _obj_x = i;
		    _obj_y = j;
		    return ;
		  }
	      }
	  }
      }
    
    return ;
  }
  
  virtual int	try_drop_bomb(std::vector< std::vector< std::vector<int> > > &map)
  {
    if (map[_pos_x][_pos_y][1] == -1)
      return (-1);
    map[_pos_x][_pos_y][1] = -1;

    if (_pos_x != _data->get_width())
      map[_pos_x + 1][_pos_y][1] = -1;
    if (_pos_x > 0)
      map[_pos_x - 1][_pos_y][1] = -1;
    if (_pos_y != _data->get_length())
      map[_pos_x][_pos_y + 1][1] = -1;
    if (_pos_y > 0)
      map[_pos_x][_pos_y - 1][1] = -1;
 
    
    for (int i = 2; i <= 5; i++)
      {
	if (try_set_obj(map, i) == 1)
	  {
	    drop_bomb();
	    return (1);
	  }
      }
    return (-1);
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    std::vector< std::vector< std::vector<int> > > map;
    int						   ret;

    if (_dying == true)
      return (false);
    //    animations
    if (_passiv_anim >= 0)
      _passiv_anim -= clock.getElapsed();
    if (_activ_anim >= 0)
      _activ_anim -= clock.getElapsed();
    if (_passiv_state == WALKING)
      _passiv_state = IDLE_OFF;
    _animupdated = false;


    mapping(map); 
    my_lee(map, _pos_x, _pos_y);
    _bool = 0;

    set_objectif(map);

    if ((ret = move_ia(map, _obj_x, _obj_y)) >= 0)
      move(ret, clock);

    if ((_obj_x == _pos_x && _obj_y == _pos_y) && _bombes_max > 0)
      try_drop_bomb(map);

    
    //invincibilité
    if (_invincibility < 0.00)
      _invincibility = 0.00;
    else if (_invincibility > 0.00)
      _invincibility -= clock.getElapsed();

    //animations
    (void)input;
    return (true);
  }
};

#endif /* BONHOMME_IA_HPP_ */
