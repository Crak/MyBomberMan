//
// Cube.hpp for Cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:49:38 2014 corentin bajdek
// Last update Sun Jun 15 00:30:47 2014 corentin bajdek
//


#ifndef BONHOMME_J2_HPP_
# define BONHOMME_J2_HPP_

#include <vector>
#include <Model.hh>
#include <Geometry.hh>
#include <Texture.hh>
#include "Bonhomme.hpp"
#include "../Overlay/Minimap.hpp"

class Bonhomme_J2 : public Bonhomme
{
public:
  Bonhomme_J2(int pos_x, int pos_y, int team, Map *data, int id) :
    Bonhomme(pos_x, pos_y, team, data, id)
  {
    _subtype = J2;
    data->create_minimap(J2, this);
    return ; 
  }
  virtual ~Bonhomme_J2() 
  {
    _data->delete_minimap(J2);
    return ; 
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    //multi_move
    multi_move = 1;
    if ((input.getKey(SDLK_z) || input.getKey(SDLK_s)) &&
	!(input.getKey(SDLK_z) && input.getKey(SDLK_s)) &&
	(input.getKey(SDLK_q) || input.getKey(SDLK_d)) &&
	!(input.getKey(SDLK_q) && input.getKey(SDLK_d)))
      multi_move = 1.4142;

    //animations
    if (_passiv_anim >= 0)
      _passiv_anim -= clock.getElapsed();
    if (_activ_anim >= 0)
      _activ_anim -= clock.getElapsed();
    if (_passiv_state == WALKING)
      _passiv_state = IDLE_OFF;
    _animupdated = false;

    // gestion d'input
    if (input.getKey(SDLK_z))
      this->move(UP, clock);
    if (input.getKey(SDLK_s))
      this->move(DOWN, clock);
    if (input.getKey(SDLK_q))
      this->move(LEFT, clock);
    if (input.getKey(SDLK_d))
      this->move(RIGHT, clock);
    if (input.getKey(SDLK_e) && _bombes_max > 0)
      this->drop_bomb();
    if (input.getKey(SDLK_r) && _special != NONE)
      use_special();

    //update de l'interface minimap & co
    _data->get_minimap(J2)->set_pos_x(_pos_x);
    _data->get_minimap(J2)->set_pos_y(_pos_y);

    //invincibilité
    if (_invincibility < 0.00)
      _invincibility = 0.00;
    else if (_invincibility > 0.00)
      _invincibility -= clock.getElapsed();
    //mort
    if (_dying == true)
      return (false);
    return (true);
  }
};

#endif /* BONHOMME_J2_HPP */
