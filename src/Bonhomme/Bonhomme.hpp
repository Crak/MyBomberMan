//
// Cube.hpp for Cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:49:38 2014 corentin bajdek
// Last update Sun Jun 15 22:43:27 2014 corentin bajdek
//

#ifndef BONHOMME_HPP_
# define BONHOMME_HPP_

#include <string>
#include <map>
#include <queue>
#include <vector>
#include <Model.hh>
#include <Geometry.hh>
#include <Texture.hh>
#include "../AObject.hpp"
#include "../Bombes/Bombes.hpp"
#include "../Bonus/Bonus.hpp"
#include "../Fire/Fire.hpp"
#include "../Map.hpp"
#include "../Menu/Texte.hpp"

# define ARR(X)	((((int)((X) * 2) % 2 == 1)) ? (((int)(X + 0.5))) : (((int)(X))))

enum
  {
    IDLE_OFF,
    IDLE_ON,
    WALKING
  };
    
class Minimap;

class Bonhomme : public AObject
{
protected:
  void		drop_bomb();
  void		move(int direction, gdl::Clock const &clock); 
  int		Harr(double, int);
  void		chooseAnim(void);

  int					_passiv_state;
  double				_passiv_anim;
  double				_activ_anim;
  Texte					*_name_2D;
  float					_speed;
  gdl::Model				_model;
  int					_bombes_max;
  int					_explosion_len;
  double				_double_pos_x;
  double				_double_pos_y;
  int					_direction;
  double				_size;
  double				multi_move;
  const std::string			_name;
  int					_team;
  glm::vec4				_vec;
  int					_special;
  int					_nbkick;
  int					_nbthrow;
  int					_score;
  int					_kills;
  int					_life;
  glm::vec2				_spawn;
  double				_invincibility;
  bool					_animupdated;
  int					_death;
  int					_id;

public:
  Bonhomme(int, int, int, Map *, int, const std::string name = "Marvin", glm::vec4 vec = glm::vec4(1, 1, 1, 1));
  ~Bonhomme();
  virtual bool	initialize();
  virtual int	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::AShader &shader, gdl::Clock const &clock);
  virtual void	is_touched_by_fire(Bonhomme *);
  
  // setters & getters
  int		get_bombes_max(void) const;
  double	get_d_pos_x(void) const;
  double	get_d_pos_y(void) const;
  int		get_explosion_len(void) const;
  float		get_speed(void) const;
  int		get_special(void) const;
  int		get_nbkick(void) const;
  int		get_nbthrow(void) const;
  int		get_score(void) const;
  int		get_kills(void) const;
  int		get_life(void) const;
  int		get_death(void) const;
  int		get_id(void) const;

  void		set_bombes_max(int);
  void		set_explosion_len(int);
  void		set_speed(float);
  void		set_special(int);
  void		set_score(int);
  void		set_nbkick(int);
  void		set_nbthrow(int);
  void		set_kills(int);

  void		use_special();
  void		save(std::ofstream &ofs) const;
  void		load(std::vector< double> &values);
};

#endif /* BONHOMME_HPP_ */
