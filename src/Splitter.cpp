//
// Splitter.hpp for Splitter in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by Lucas SARKADI
// Login   <sarkad_l@epitech.net>
// 
// Started on  Sat Jun 14 00:22:26 2014 Lucas SARKADI
// Last update Sun Jun 15 22:57:20 2014 corentin bajdek
//

#include "AObject.hpp"
#include "Splitter.hpp"

Map			*Splitter::getData() const { return _data; }
gdl::Input		&Splitter::getInput() { return _input; }
const gdl::Clock	&Splitter::getClock() const { return _clock; }

void		*Splitter::update_one(void *arg)
{
  Splitter	*splitter = (Splitter *)arg;

  for (size_t i = 0 ; i < splitter->getData()->Objects.size() ; i += 4)
    if ((splitter->getData()->Objects[i]->update(splitter->getClock(), splitter->getInput())) == false)
      {
	delete (splitter->getData()->Objects[i]);
	splitter->getData()->Objects.erase(splitter->getData()->Objects.begin() + i);
      }
  return (NULL);
}

void		*Splitter::update_two(void *arg)
{
  Splitter	*splitter = (Splitter *)arg;

  for (size_t i = 1 ; i < splitter->getData()->Objects.size() ; i += 4)
    if ((splitter->getData()->Objects[i]->update(splitter->getClock(), splitter->getInput())) == false)
      {
	delete (splitter->getData()->Objects[i]);
	splitter->getData()->Objects.erase(splitter->getData()->Objects.begin() + i);
      }
  return (NULL);
}

void		*Splitter::update_three(void *arg)
{
  Splitter	*splitter = (Splitter *)arg;

  for (size_t i = 2 ; i < splitter->getData()->Objects.size() ; i += 4)
    if ((splitter->getData()->Objects[i]->update(splitter->getClock(), splitter->getInput())) == false)
      {
	delete (splitter->getData()->Objects[i]);
	splitter->getData()->Objects.erase(splitter->getData()->Objects.begin() + i);
      }
  return (NULL);
}

void		*Splitter::update_four(void *arg)
{
  Splitter	*splitter = (Splitter *)arg;

  for (size_t i = 3 ; i < splitter->getData()->Objects.size() ; i += 4)
    if ((splitter->getData()->Objects[i]->update(splitter->getClock(), splitter->getInput())) == false)
      {
	delete (splitter->getData()->Objects[i]);
	splitter->getData()->Objects.erase(splitter->getData()->Objects.begin() + i);
      }
  return (NULL);
}

Splitter::Splitter(gdl::Clock const &clock, gdl::Input &input, Map *data) : _data(data) , _input(input) , _clock(clock)
{
  Fonction	d_one;
  Fonction	d_two;
  Fonction	d_three;
  Fonction	d_four;

  d_one = (Fonction)(&update_one);
  d_two = (Fonction)(&update_two);
  d_three = (Fonction)(&update_three);
  d_four = (Fonction)(&update_four);
  _tabUpdate.push_back(Thread(d_one, this));
  _tabUpdate.push_back(Thread(d_two, this));
  _tabUpdate.push_back(Thread(d_three, this));
  _tabUpdate.push_back(Thread(d_four, this));
}

Splitter::Splitter(const Splitter &cpy) : _data(cpy._data) , _input(cpy._input) , _clock(cpy._clock)
{
  _tabUpdate = cpy._tabUpdate;
}

Splitter::~Splitter()
{
}
 
int	Splitter::update()
{
  for (size_t i = 0 ; i < _tabUpdate.size() ; i++)
    _tabUpdate[i].create();
  for (size_t i = 0 ; i < _tabUpdate.size() ; i++)
    _tabUpdate[i].join();
  
  return (0);
}
