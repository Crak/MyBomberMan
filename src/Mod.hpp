//
// Mod.hh for mode in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 00:09:33 2014 corentin bajdek
// Last update Sat Jun 14 20:30:51 2014 corentin bajdek
//

#ifndef MOD_H_
# define MOD_H_

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

enum
  {
    RESTART,
    NEXTLVL,
  };

enum
  {
    MODE_CLASSIC,
    MODE_TEMPS,
    MODE_VIE,
    MODE_HISTOIRE,
  };

enum
  {
    PEU,
    MOYEN,
    NOMBREUX,
  };

/* VALUES FOR _INT_MOD
**
** HumanPlayers => nombre de joueurs humains dans la partie
** NbIa => nombre d'IA
** Overlay => choix de l'overlay
** Music => 0 = off, 1 = on
** Sons =>  0 = off, 1 = on
** GameMode => liste des gamemodes (cf enum)
** NbLife => nombre de vies (en mode vie)
** NbTime => Temps de la partie (en mode vie, et en minutes)
** NbBonus => Frequence des bonus
** Action => que faire a la fin d'un menu (trix forcé par le type bool de GameEngine::update)
*/

/*
** Fonctions nottée avec un .. non const
** car le GetIntMod ne peut pas etre en const vu
** qu'il touche au std::map
*/

class Mod
{
protected :
  int					width;
  int					length;
  std::map<std::string, int>		_int_mod;
  std::map<std::string, std::string>	_string_mod;


public :
  Mod();
  ~Mod(void);
  
  // Setters & getters
  int		getwidth(void) const;
  int		getlength(void) const;
  // std::vector<AObject *>	getObjects(void) const;

  
  void		setwidth(int);
  void		setlength(int);
  // void		setObjects(const std::vector<AObject *> &);  

  int		getIntMod(const std::string &); //..
  std::string	getStringMod(const std::string &); //..

  void		setIntMod(const std::string &, int); //..
  void		setStringMod(const std::string &, const std::string &); //..
  void		save(std::ofstream &ofs); //..
};

#endif /* !MOD_H_ */
