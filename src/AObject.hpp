//
// AObject.hpp for header in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:13:47 2014 corentin bajdek
// Last update Sun Jun 15 22:46:02 2014 corentin bajdek
//


#ifndef AOBJECT_HPP_
# define AOBJECT_HPP_

#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <BasicShader.hh>
#include <vector>
#include "Map.hpp"
#include "Mod.hpp"

enum
  {
    DEFAULT,
    BOMBE,
    BONHOMME,
    BONUS,
    WALL,
    FIRE,
    WALL_DESTRUCTIBLE,
    IA,
    J2,
    PARTICLES,
  };


class AObject
{
protected:
  int	    _type;
  int	    _dying;
  glm::vec3 _position;
  glm::vec3 _rotation;
  glm::vec3 _scale;
  int	    _pos_x;
  int	    _pos_y;
  double    _lifetime;
  int	    _subtype;
  Map	    *_data;
  bool	    _initialize;

public:
  AObject(int, int, Map *);
  virtual	~AObject();
  virtual bool initialize();

  virtual int	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::AShader &shader, gdl::Clock const &clock) = 0;
  void		translate(glm::vec3 const &v);
  void		rotate(glm::vec3 const& axis, float angle);
  void		scale(glm::vec3 const& scale);
  int		get_type(void);
  virtual void	is_touched_by_fire(Bonhomme *father);
  glm::mat4	getTransformation();
  virtual void	save(std::ofstream &ofs) const;
  virtual void	load(std::vector< double> &values);
  
  //setters & getters
  int		get_pos_x(void) const;
  int		get_pos_y(void) const;
  void		set_pos_x(int pos_x);
  void		set_pos_y(int pos_y);
  int		get_subtype();
  int		get_dying() const { return _dying;}
  bool		is_initialize() const { return _initialize;}
};

#endif /* AOBJECT_HPP_ */
