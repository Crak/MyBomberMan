//
// Fire.hh for Fire in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 22:03:23 2014 corentin bajdek
// Last update Sun Jun 15 12:18:25 2014 corentin bajdek
//

#ifndef FIRE_H_
# define FIRE_H_

#include "../Bonhomme/Bonhomme.hpp"
#include "../AObject.hpp"

#include <vector>
#include <fstream>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>

enum
  {
    LEFT,
    RIGHT,
    UP,
    DOWN,
  };

# define SPREAD_DELAY (0.06)

class Fire : public AObject
{
private :
  void			spread(int posx, int posy, int dir);
private :
  gdl::Texture		*_texture;
  gdl::Geometry		_geometry;
  double		_delay;
  int			_multiples;
  int			_direction;
  Bonhomme		*_father;

public :
  Fire(int, int, double, int, int, Map *, Bonhomme *);
  ~Fire(void);

// Setters & getters
  int		get_delay(void) const;
  void		set_delay(int);
  virtual bool	initialize();
  virtual int	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::AShader &shader, gdl::Clock const &clock);
  virtual void	is_touched_by_fire(Bonhomme *);
  void		delete_me(void);
  void		save(std::ofstream &ofs) const;
  void		load(std::vector< double> &values);
};

#endif /* !FIRE_H_ */
