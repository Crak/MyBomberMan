//
// Fire.cpp for Fire in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 22:03:34 2014 corentin bajdek
// Last update Sun Jun 15 12:19:30 2014 corentin bajdek
//

#include "Fire.hpp"

/*
** Constructeurs / Destructeurs
*/

Fire::Fire(int pos_x, int pos_y, double delay, int multiples, int direction, Map *data, Bonhomme *father)
  : AObject(pos_x, pos_y, data), _delay(delay), _multiples(multiples), _direction(direction), _father(father)
{
  _type = FIRE;
  _dying = false;
  _lifetime = 0;
  return ;
}

Fire::~Fire(void) { return ; }

/*
** Setters/getters
*/

int		Fire::get_delay(void) const { return (this->_delay) ; }
void		Fire::set_delay(int delay) { this->_delay = delay ; }

bool		Fire::initialize(void)
{
  //Gerer l'erreur
  _texture = _data->getTextContainer()->getTexture("./assets/Fire.tga");

  if ((_direction == RIGHT || _direction == LEFT))
    rotate(glm::vec3(0.0, 0.0, 1.0), 90);
  // if ((_direction == LEFT))
  //   rotate(glm::vec3(0.0, 0.0, 1.0), 270);
  translate(glm::vec3(_pos_x, _pos_y, 0));

  _geometry.pushVertex(glm::vec3(0.33, -0.5, 0.16));
  _geometry.pushVertex(glm::vec3(0.33, 0.5, 0.16));
  _geometry.pushVertex(glm::vec3(-0.33, 0.5, 0.16));
  _geometry.pushVertex(glm::vec3(-0.33, -0.5, 0.16));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  
  _geometry.pushVertex(glm::vec3(0.33, -0.5, -0.50));
  _geometry.pushVertex(glm::vec3(0.33, 0.5, -0.50));
  _geometry.pushVertex(glm::vec3(-0.33, 0.5, -0.50));
  _geometry.pushVertex(glm::vec3(-0.33, -0.5, -0.50));
  
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(0.33, -0.5, -0.50));
  _geometry.pushVertex(glm::vec3(0.33, 0.5, -0.50));
  _geometry.pushVertex(glm::vec3(0.33, 0.5, 0.50));
  _geometry.pushVertex(glm::vec3(0.33, -0.5, 0.50));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(-0.33, -0.5, 0.16));
  _geometry.pushVertex(glm::vec3(-0.33, 0.5, 0.16));
  _geometry.pushVertex(glm::vec3(-0.33, 0.5, -0.50));
  _geometry.pushVertex(glm::vec3(-0.33, -0.5, -0.50));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(0.33, 0.5, 0.16));
  _geometry.pushVertex(glm::vec3(0.33, 0.5, -0.50));
  _geometry.pushVertex(glm::vec3(-0.33, 0.5, -0.50));
  _geometry.pushVertex(glm::vec3(-0.33, 0.5, 0.16));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  
  _geometry.pushVertex(glm::vec3(0.33, -0.5, -0.50));
  _geometry.pushVertex(glm::vec3(0.33, -0.5, 0.16));
  _geometry.pushVertex(glm::vec3(-0.33, -0.5, 0.16));
  _geometry.pushVertex(glm::vec3(-0.33, -0.5, -0.50));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.build();
  _initialize = true;
  return (true);
}

void		Fire::spread(int posx, int posy, int dir)
{
  //spreading des flammes
  AObject	*ptr;

  for (size_t i = 0; i < _data->Objects.size(); ++i)
    if (_data->Objects[i]->get_pos_x() == posx && _data->Objects[i]->get_pos_y() == posy)
      {
	if (_data->Objects[i]->get_type() == WALL)
	  {
	    if (_data->Objects[i]->get_subtype() == DEFAULT)
	      {
		_multiples = 0;
		return ;
	      }
	    else if (_data->Objects[i]->get_subtype() == WALL_DESTRUCTIBLE)
	      _multiples = 1;
	  }
      }
  ptr = _data->create_fire(posx, posy, SPREAD_DELAY, _multiples - 1, dir, _father);
  _data->Objects.push_back(ptr);
  _multiples = 0;
}

int		Fire::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)input;
  _lifetime += clock.getElapsed();
  for (size_t i = 0; i < _data->Objects.size(); ++i)
    if (_data->Objects[i]->get_pos_x() == _pos_x && _data->Objects[i]->get_pos_y() == _pos_y)
      _data->Objects[i]->is_touched_by_fire(_father);
  if (_multiples > 0 && _lifetime > _delay)
    {
      if (_direction == LEFT)
  	this->spread(_pos_x - 1, _pos_y, LEFT);
      else if (_direction == RIGHT)
  	this->spread(_pos_x + 1, _pos_y, RIGHT);
      else if (_direction == UP)
  	this->spread(_pos_x, _pos_y - 1, UP);
      else if (_direction == DOWN)
  	this->spread(_pos_x, _pos_y + 1, DOWN);
    }
  if (_lifetime > (_delay * 6) || _dying == true)
    return (false);
  return (true);
}

void		Fire::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  _texture->bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}

void	Fire::is_touched_by_fire(Bonhomme *father)
{
  (void)father;
  return ;
}

void		Fire::delete_me()
{
  _dying = true;
}

void		Fire::save(std::ofstream &ofs) const
{
  ofs << "Fire" << "::";
  ofs << _pos_x << "::";
  ofs << _pos_y << "::";
  ofs << _delay << "::";
  ofs << _multiples << "::";
  ofs << _direction << "::";
  ofs << _lifetime << "::";
  ofs << _father->get_id() << "::";
}

void		Fire::load(std::vector< double> &values)
{
  Bonhomme	*man;

  _pos_x = (int)values[0];
  _pos_y = (int)values[1];
  _multiples = (int)values[3];
  _lifetime = values[4];
  for (size_t i = 0; i < _data->Objects.size() ; i++)
    {
      if (_data->Objects[i]->get_type() == BONHOMME)
	{
	  man = dynamic_cast<Bonhomme *>(_data->Objects[i]);
	  if (man->get_id() == (int)values[5])
	    _father = man;
	}
      return ;
    }
}
