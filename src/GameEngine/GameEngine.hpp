//
// GameEngine.hpp for game in /home/bajdek_c/bomber/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:28:38 2014 corentin bajdek
// Last update Sun Jun 15 20:01:51 2014 Lucas SARKADI
//

#ifndef GAMEENGINE_HPP_
# define GAMEENGINE_HPP_

#include <vector>
#include <Game.hh>
#include <SdlContext.hh>
#include "../Bonhomme/Bonhomme_J2.hpp"
#include "../Bonhomme/Bonhomme_IA.hpp"
#include "../Bonhomme/Bonhomme.hpp"
#include "../Loading.hpp"
#include "../Background.hpp"
#include "../AObject.hpp"
#include "../Mod.hpp"
#include "../Map.hpp"
#include "../Splitter.hpp"

class ASubmenu;

class GameEngine : public gdl::Game
{
protected :
  gdl::SdlContext	&_context;
  gdl::Clock		_clock;
  gdl::Input		_input;
  gdl::BasicShader	_shader;
  gdl::BasicShader	_shader_red;
  gdl::BasicShader	_shader_blue;
  gdl::BasicShader	_shader_green;
  Map		       *_data;
  unsigned int		width;
  unsigned int		length;
  Bonhomme		*_hero2;
  Bonhomme		*_hero;
  int			s_width;
  int			s_length;
  ASubmenu		*_Pause;
  ASubmenu		*_End;
  int			_on_hold;
protected :
  void			draw_from(Bonhomme *, float);
  void			clear_ressources();
  virtual bool		GameOver();

public:
  GameEngine(const Mod &, Map *, gdl::SdlContext &, int width_screen = 1800, int length_screen = 1000);
  ~GameEngine();
  bool		initialize();
  bool		update();
  void		draw();
};

#endif /* GAMEENGINE_HPP_ */
