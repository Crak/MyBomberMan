//
// GameEngine_temps.hpp for gameengine in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/GameEngine
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Tue Jun 10 00:32:07 2014 corentin bajdek
// Last update Tue Jun 10 05:03:42 2014 corentin bajdek
//

#ifndef GAMEENGINE_TEMPS_HPP_
# define GAMEENGINE_TEMPS_HPP_

#include "../Menu/Submenu_end_temps.hpp"
#include "GameEngine.hpp"

class GameEngine_temps : public GameEngine
{
private :

public :
  GameEngine_temps(const Mod &mod, Map *data, gdl::SdlContext &context, int width_screen = 1800, int length_screen = 1000)
    : GameEngine(mod, data, context, width_screen, length_screen)
  {
    return ;
  }
  ~GameEngine_temps() {}

  virtual bool	GameOver()
  {
    bool	over;
    glm::mat4	projection = glm::ortho(0.0f, (float)s_width, (float)s_length, 0.0f, -1.0f, 1.0f);
    int		ret = -1;
    Bonhomme	*bonhomme;
    Bonhomme	*bestbonhomme = NULL;
    int		kd;
    int		bestkd = -42;
    int		bestkills = 0;
    int		bestdeath = 0;

    //is game over ?
    over = false;
    if ((_data->get_minimap(DEFAULT) != NULL && _data->get_minimap(DEFAULT)->get_time() == (double)0.00) ||
    	(_data->get_minimap(J2) != NULL && _data->get_minimap(J2)->get_time() == (double)0.00))
      {
	over = true;
	//find the best K/D ratio
	for (size_t i = 0; i < _data->Objects.size(); ++i)
	  if (_data->Objects[i]->get_type() == BONHOMME)
	    {
	      bonhomme = dynamic_cast<Bonhomme *>(_data->Objects[i]);
	      kd = bonhomme->get_kills() - bonhomme->get_death();
	      if (kd > bestkd || bestkd == -42)
		{
		  bestkd = kd;
		  bestkills = bonhomme->get_kills();
		  bestdeath = bonhomme->get_death();
		  bestbonhomme = bonhomme;
		}
	    }
	if (bestbonhomme != NULL && bestbonhomme->get_subtype() == DEFAULT)
	  _End = new Submenu_end_temps(_data, NULL, "Player 1 wins !!", bestkills, bestdeath);
	else if (bestbonhomme->get_subtype() == J2)
	  _End = new Submenu_end_temps(_data, NULL, "Player 2 wins !!", bestkills, bestdeath);
	else
	  _End = new Submenu_end_temps(_data, NULL, "Computer wins !!", bestkills, bestdeath);
      }

    //menu de fin
    if (over == true)
      {
	_End->set_activate(true);
	_shader.setUniform("view", glm::mat4(1));
	_shader.setUniform("projection", projection);
	glViewport(0, 0, s_width, s_length);
	while ((ret = _End->update(_clock, _input)) != QUITTER)
	  {
	    if (_input.getInput(SDL_QUIT))
	      {
		_Pause->set_activate(false);
		return (over);
	      }
	    _context.updateInputs(_input);
	    _End->draw(_shader);
	    _context.flush();
	  }
	_End->set_activate(false);
	_context.updateClock(_clock);
	delete _End;
      }
    return (over);
  }
};

#endif /* */
