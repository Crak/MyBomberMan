//
// GameEngine.cpp for Gameengine in /home/bajdek_c/bomber/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:29:06 2014 corentin bajdek
// Last update Sun Jun 15 20:01:43 2014 Lucas SARKADI
//

#include "../Save.hpp"
#include "GameEngine.hpp"
#include "../Wall/Wall.hpp"
#include "../Floor/Sky.hpp"
#include "../Floor/Floor.hpp"
#include "../Menu/Submenu_pause.hpp"
#include "../Menu/Submenu_end.hpp"
#include "../Menu/Submenu_end_temps.hpp"

GameEngine::GameEngine(const Mod &mod, Map  *data, gdl::SdlContext &context,
		       int width_screen, int length_screen) :
  _context(context) , _data(data), _on_hold(NONE)
{
  this->width = mod.getwidth();
  this->length = mod.getlength();
  this->s_width = width_screen;
  this->s_length = length_screen;
  this->_hero = NULL;
  this->_hero2 = NULL;
  for (size_t i = 0 ; i < _data->Objects.size() ; i++)
    {
      if (_data->Objects[i]->get_type() == BONHOMME && !(dynamic_cast<Bonhomme_J2 *>(data->Objects[i])) &&
	  !(dynamic_cast<Bonhomme_IA *>(data->Objects[i])))
	this->_hero = dynamic_cast<Bonhomme *>(data->Objects[i]);
      if (_data->Objects[i]->get_type() == BONHOMME && dynamic_cast<Bonhomme_J2 *>(data->Objects[i]))
	this->_hero2 = dynamic_cast<Bonhomme_J2 *>(data->Objects[i]);
    }
  _Pause = new Submenu_pause(_data, NULL);
  _Pause->set_activate(false);
  return ;
}

void	GameEngine::clear_ressources()
{
  for (size_t i = 0; i < _data->Objects.size(); ++i)
    delete _data->Objects[i];
  _data->Objects.clear();
  _data->clear();
  delete _Pause;
}

GameEngine::~GameEngine()
{
  this->clear_ressources();
}

bool	GameEngine::initialize()
{
  if (s_width < 50 || s_length < 50)
    return false;

  glEnable(GL_DEPTH_TEST);
  if (!_shader.load("./shaders/basic.fp", GL_FRAGMENT_SHADER)
      || !_shader.load("./shaders/basic.vp", GL_VERTEX_SHADER)
      || !_shader.build())
    return false;
  if (!_shader_red.load("./shaders/basic_red.fp", GL_FRAGMENT_SHADER)
      || !_shader_red.load("./shaders/basic.vp", GL_VERTEX_SHADER)
      || !_shader_red.build())
    return false;
  if (!_shader_blue.load("./shaders/basic_blue.fp", GL_FRAGMENT_SHADER)
      || !_shader_blue.load("./shaders/basic.vp", GL_VERTEX_SHADER)
      || !_shader_blue.build())
    return false;
  if (!_shader_green.load("./shaders/basic_green.fp", GL_FRAGMENT_SHADER)
      || !_shader_green.load("./shaders/basic.vp", GL_VERTEX_SHADER)
      || !_shader_green.build())
    return false;

  // On place ensuite la camera (sa projection ainsi que sa transformation)
  glm::mat4 projection;
  glm::mat4 transformation;
  // La projection de la camera correspond a la maniere dont les objets vont etre dessine a
  projection = glm::perspective(60.0f, 1200.0f / 1000.0f, 0.1f, 100.0f);

  if (_hero == NULL)
    {
      _shader.bind();
      _shader.setUniform("view", transformation);
      _shader.setUniform("projection", projection);
    }

  Loading	*load = _data->create_loading((int)(_data->Objects.size()));
  load->initialize();
  for (size_t i = 0; i < _data->Objects.size(); ++i)
    {
      if (_data->Objects[i]->initialize() == false)
	return (false);
      load->update(_clock, _input, i);
      load->draw(_shader);
      _context.flush();
    }
  //update du contexte
  _context.updateClock(_clock);
  _context.updateInputs(_input);
  while ((load->update(_clock, _input, _data->Objects.size())) != false)
    {
      _context.updateClock(_clock);
      _context.updateInputs(_input);
      load->draw(_shader);
      _context.flush();
    }

  AObject	*floor;
  floor = _data->create_floor(_data->get_length(), _data->get_width());
  if (floor->initialize() == false)
    return (false);
  _data->Objects.push_back(floor);

  AObject	*sky;
  sky = new Sky (_data->get_length(), _data->get_width(), _data);
  if (sky->initialize() == false)
    return (false);
  _data->Objects.push_back(sky);

  return true;
}

bool		GameEngine::update()
{
  int		ret;
  glm::mat4     projection = glm::ortho(0.0f, (float)s_width, (float)s_length, 0.0f, -1.0f, 1.0f);

  if (_input.getInput(SDL_QUIT))
    return (false);
  _context.updateClock(_clock);
  _context.updateInputs(_input);
  //  _splitter.update();
  for (size_t i = 0; i < _data->Objects.size(); ++i)
    {
      if ((_data->Objects[i]->update(_clock, _input)) == false)
	{
	  if (_data->Objects[i] == _hero)
	    _hero = NULL;
	  if (_data->Objects[i] == _hero2)
	    _hero2 = NULL;
	  delete (_data->Objects[i]);
	  _data->Objects.erase(_data->Objects.begin() + i);
	}
    }
  for (size_t i = 0; i < _data->Objects.size(); ++i)
    if (_data->Objects[i]->is_initialize() == false)
      _data->Objects[i]->initialize();
  if (_data->get_minimap(DEFAULT) != NULL)
      _data->get_minimap(DEFAULT)->update(_clock, _input);
  if (_data->get_minimap(J2) != NULL)
    _data->get_minimap(J2)->update(_clock, _input);
  
  if (_input.getKey(SDLK_ESCAPE) && _on_hold == NONE)
    _on_hold = UP;
  if (!(_input.getKey(SDLK_ESCAPE)) && _on_hold == UP)
    {
      _on_hold = NONE;
      _Pause->set_activate(true);
      _shader.setUniform("view", glm::mat4(1));
      _shader.setUniform("projection", projection);
      glViewport(0, 0, s_width, s_length);
      while ((ret = _Pause->update(_clock, _input)) != RETOUR)
	{
	  if (ret == QUITTER || _input.getInput(SDL_QUIT))
	    {
	      _Pause->set_activate(false);
	      return (false);
	    }
	  _context.updateInputs(_input);
	  _Pause->draw(_shader);
	  _context.flush();
	}
      _Pause->set_activate(false);
      _context.updateClock(_clock);
    }
  if (this->GameOver() == true)
    return (false);
  return (true);
}

void		GameEngine::draw_from(Bonhomme *view, float aspect)
{
  glm::mat4 transformation;
  glm::mat4 projection;

  projection = glm::perspective(60.0f, aspect, 0.1f, 100.0f);
  if (view == NULL)
    transformation = glm::lookAt(glm::vec3(width / 2, length / 2 - 1, (width + length) / 2),
				 glm::vec3(width / 2, length / 2, 0),
				 glm::vec3(0, 0, 1));
  else
    transformation = glm::lookAt(glm::vec3(view->get_d_pos_x(), view->get_d_pos_y() - 7, 10),
				 glm::vec3(view->get_d_pos_x(), view->get_d_pos_y(), 0),
				 glm::vec3(0, 0, 1));
  
  _shader.bind();
  _shader.setUniform("projection", projection);
  _shader.setUniform("view", transformation);
  _shader_red.bind();
  _shader_red.setUniform("projection", projection);
  _shader_red.setUniform("view", transformation);
  _shader_blue.bind();
  _shader_blue.setUniform("projection", projection);
  _shader_blue.setUniform("view", transformation);
  _shader_green.bind();
  _shader_green.setUniform("projection", projection);
  _shader_green.setUniform("view", transformation);
  for (size_t i = 0; i < _data->Objects.size(); ++i)
    {
      if (_data->Objects[i]->get_type() == BONHOMME)
	{
	  if (_data->Objects[i]->get_subtype() == DEFAULT)
	    {
	      _shader_blue.bind();
	      _data->Objects[i]->draw(_shader_blue, _clock);
	    }
	  else if (_data->Objects[i]->get_subtype() == J2)
	    {
	      _shader_green.bind();
	      _data->Objects[i]->draw(_shader_green, _clock);
	    }
	  else
	    {
	      _shader_red.bind();
	      _data->Objects[i]->draw(_shader_red, _clock);
	    }
	  _shader.bind();
	}
      else
	_data->Objects[i]->draw(_shader, _clock);
    }
}

void		GameEngine::draw()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  if (!_hero && !_hero2)
    {
      glViewport(0, 0, s_width, s_length);
      draw_from(NULL, (double)s_width / (double)s_length);
    }
    
  if (_hero2)
    {
      glViewport(0, 0, ((_hero) ? s_width / 2 - 3 : s_width), s_length);
      draw_from(_hero2, ((_hero) ? (float)(s_width / 2) : (float)(s_width)) / (double)s_length);
      glm::mat4     projection = glm::ortho(0.0f, (float)s_width, (float)s_length, 0.0f, -1.0f, 1.0f);
      _shader.bind();
      _shader.setUniform("view", glm::mat4(1));
      _shader.setUniform("projection", projection);
      if (_data->get_minimap(J2) != NULL)
	_data->get_minimap(J2)->draw(_shader, _clock);
    }

  if (_hero)
    {
      glViewport((s_width / 2 + 3) * (_hero2 != NULL), 0, ((_hero2) ? s_width / 2 - 3 : s_width), s_length);
      draw_from(_hero, (_hero2 ? (float)(s_width / 2) : (float)(s_width)) / (double)s_length);
      glm::mat4     projection = glm::ortho(0.0f, (float)s_width, (float)s_length, 0.0f, -1.0f, 1.0f);
      _shader.bind();
      _shader.setUniform("view", glm::mat4(1));
      _shader.setUniform("projection", projection);
      if (_data->get_minimap(DEFAULT) != NULL)
	_data->get_minimap(DEFAULT)->draw(_shader, _clock);
    }
  _context.flush();
}

bool		GameEngine::GameOver()
{
  bool		over;
  glm::mat4     projection = glm::ortho(0.0f, (float)s_width, (float)s_length, 0.0f, -1.0f, 1.0f);
  int		ret = -1;

  over = false;
  if (_hero == NULL && _hero2 == NULL)
    {
      _End = new Submenu_end(_data, NULL, "Computer wins !!");
      over = true;
    }
  if (_data->get_minimap(DEFAULT) != NULL && _data->get_minimap(DEFAULT)->get_enemies() == 0)
    {
      _End = new Submenu_end(_data, NULL, "Player 1 wins !!");
      over = true;
    }
  else if (_data->get_minimap(J2) != NULL && _data->get_minimap(J2)->get_enemies() == 0)
    {
      _End = new Submenu_end(_data, NULL, "Player 2 wins !!");
      over = true;
    }

  if (over == true)
    {
      _End->set_activate(true);
      _shader.setUniform("view", glm::mat4(1));
      _shader.setUniform("projection", projection);
      glViewport(0, 0, s_width, s_length);
      while ((ret = _End->update(_clock, _input)) != QUITTER)
	{
	  if (_input.getInput(SDL_QUIT))
	    {
	      _Pause->set_activate(false);
	      return (over);
	    }
	  _context.updateInputs(_input);
	  _End->draw(_shader);
	  _context.flush();
	}
      _End->set_activate(false);
      _context.updateClock(_clock);
      delete _End;
    }
  return (over);
}
