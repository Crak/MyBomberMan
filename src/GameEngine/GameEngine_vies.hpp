//
// GameEngine_vies.hpp for gameengine in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/GameEngine
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Tue Jun 10 00:32:07 2014 corentin bajdek
// Last update Tue Jun 10 14:42:58 2014 corentin bajdek
//

#ifndef GAMEENGINE_VIES_HPP_
# define GAMEENGINE_VIES_HPP_

#include "../Menu/Submenu_end.hpp"
#include "GameEngine.hpp"

class GameEngine_vies : public GameEngine
{
private :

public :
  GameEngine_vies(const Mod &mod, Map *data, gdl::SdlContext &context, int width_screen = 1800, int length_screen = 1000)
    : GameEngine(mod, data, context, width_screen, length_screen)
  {
    return ;
  }
  ~GameEngine_vies() {}

  bool		GameOver()
  {
    bool	over;
    glm::mat4   projection = glm::ortho(0.0f, (float)s_width, (float)s_length, 0.0f, -1.0f, 1.0f);
    int		ret = -1;

    over = false;
    if (_hero == NULL && _hero2 == NULL)
      {
	_End = new Submenu_end(_data, NULL, "Computer wins !!");
	over = true;
      }
    if (_data->get_minimap(DEFAULT) != NULL && _data->get_minimap(DEFAULT)->get_enemies() == 0)
      {
	_End = new Submenu_end(_data, NULL, "Player 1 wins !!");
	over = true;
      }
    else if (_data->get_minimap(J2) != NULL && _data->get_minimap(J2)->get_enemies() == 0)
      {
	_End = new Submenu_end(_data, NULL, "Player 2 wins !!");
	over = true;
      }

    if (over == true)
      {
	_End->set_activate(true);
	_shader.setUniform("view", glm::mat4(1));
	_shader.setUniform("projection", projection);
	glViewport(0, 0, s_width, s_length);
	while ((ret = _End->update(_clock, _input)) != QUITTER)
	  {
	    if (_input.getInput(SDL_QUIT))
	      {
		_Pause->set_activate(false);
		return (over);
	      }
	    _context.updateInputs(_input);
	    _End->draw(_shader);
	    _context.flush();
	  }
	_End->set_activate(false);
	_context.updateClock(_clock);
	delete _End;
      }
    return (over);
  }
};

#endif /* */
