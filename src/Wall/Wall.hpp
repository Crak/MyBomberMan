//
// Cube.hpp for Cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:49:38 2014 corentin bajdek
// Last update Sun Jun 15 22:35:19 2014 corentin bajdek
//

#ifndef WALL_HPP_
# define WALL_HPP_

#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include "../AObject.hpp"
#include "../Map.hpp"
#include "../Mod.hpp"
#include "../Bonus/Bonus.hpp"

class Wall : public AObject
{
protected:
  // La texture utilisee pour le cube
  gdl::Texture	*_texture;
  gdl::Geometry _geometry;
  glm::vec4	_vec;

public:
  Wall(int, int, Map *, glm::vec4 vec = glm::vec4(1, 1, 1, 1));
  ~Wall();
  virtual bool initialize();
  virtual int  update(gdl::Clock const &clock, gdl::Input &input);
  virtual void draw(gdl::AShader &shader, gdl::Clock const &clock);
  void		save(std::ofstream &ofs) const;
  void		load(std::vector< double> &values);
};

#endif /* WALL_HPP_ */
