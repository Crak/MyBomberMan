//
// Cube.hpp for Cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:49:38 2014 corentin bajdek
// Last update Sat Jun 14 05:11:56 2014 Lucas SARKADI
//

#ifndef WALLD_HPP_
# define WALLD_HPP_

#include "Wall.hpp"
#include "../Particles/Particles_box.hpp"

class Wall_Destructible : public Wall

{
public:
  Wall_Destructible(int pos_x, int pos_y, Map *data, glm::vec4 vec = glm::vec4(1, 1, 1, 1)) :
    Wall(pos_x, pos_y, data, vec)
  {
    _subtype = WALL_DESTRUCTIBLE;
  }
  ~Wall_Destructible() {}

  virtual bool initialize()
  {
    _texture = _data->getTextContainer()->getTexture("./assets/1.tga");
    // _texture = _data->getTextContainer()->getTexture("./assets/Box.tga");
    // on set la color d'une premiere face
    _geometry.setColor(_vec);

    // tout les pushVertex qui suivent seront de cette couleur
    // On y push les vertices d une premiere face
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, 0.5));
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, 0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, 0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, 0.5));

    // Les UVs d'une premiere face
    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));

    // ETC ETC
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, -0.5));
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, -0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, -0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, -0.5));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));

    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, -0.5));
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, -0.5));
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, 0.5));
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, 0.5));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));

    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, 0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, 0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, -0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, -0.5));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));

    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, 0.5));
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, -0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, -0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, 0.5));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, -0.5));
    _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, 0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, 0.5));
    _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, -0.5));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));
    // Tres important, on n'oublie pas de build la geometrie pour envoyer ses informations a la carte
    _geometry.build();
    _initialize = true;
    return (true);
  }

  virtual int  update(gdl::Clock const &clock, gdl::Input &input)
  {
    (void)clock;
    (void)input;
    AObject	*ptr;
    int		mode;		

    mode = _data->get_mod().getIntMod("NbBonus");
    if (_dying == true)
      {
	if ((mode == PEU && rand() % 10 <= 2) ||
	    (mode == MOYEN && rand() % 10 <= 4) ||
	    (mode == NOMBREUX && rand() % 10 <= 6))
	  {
	    ptr = _data->create_bonus(_pos_x, _pos_y);
	    ptr->initialize();
	    _data->Objects.push_back(ptr);
	  }
	return (false);
      }
    return (true);
  }

  virtual void	is_touched_by_fire(Bonhomme *father)
  {
    AObject       *part = new Particles_box (_pos_x, _pos_y, _data);

    part->initialize();
    _data->Objects.push_back(part);
    if (father != NULL)
      father->set_score(father->get_score() + 10);
    _dying = true;
  }
};

#endif /* WALLD_HPP_ */
