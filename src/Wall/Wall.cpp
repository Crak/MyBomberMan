//
// Wall.cpp for cube in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:22:26 2014 corentin bajdek
// Last update Sun Jun 15 22:36:25 2014 corentin bajdek
//

#include "Wall.hpp"

Wall::Wall(int pos_x, int pos_y, Map *data, glm::vec4 vec) :
  AObject(pos_x, pos_y, data), _vec(vec)
{
  _type = WALL;
  return ;
}

Wall::~Wall() {return ; }

bool	Wall::initialize(void)
{
  _texture = _data->getTextContainer()->getTexture("./assets/3.tga");
  _geometry.setColor(_vec);

  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, 0.5));
  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, 0.5));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, -0.5));
  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, -0.5));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, -0.5));
  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, -0.5));
  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, 0.5));
  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, 0.5));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, -0.5));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, 0.5));
  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, 0.5 + _pos_y, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, -0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, 0.5 + _pos_y, 0.5));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  
  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, -0.5));
  _geometry.pushVertex(glm::vec3(0.5 + _pos_x, -0.5 + _pos_y, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, 0.5));
  _geometry.pushVertex(glm::vec3(-0.5 + _pos_x, -0.5 + _pos_y, -0.5));

  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _initialize = true;
  _geometry.build();
  return (true);
}

int	Wall::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
  return (true);
}

void	Wall::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;
  _texture->bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}

void	Wall::save(std::ofstream &ofs) const
{
  ofs << "Wall" << "::";
  ofs << _pos_x << "::";
  ofs << _pos_y << "::";
  ofs << _subtype << "::";
}

void	Wall::load(std::vector< double> &values)
{
  _pos_x = (int)values[0];
  _pos_y = (int)values[1];
  return ;
}
