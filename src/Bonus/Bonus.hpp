//
// Bonus.hh for Bonus in
// /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 03:47:08 2014 corentin bajdek
// Last update Sun Jun 15 13:19:06 2014 corentin bajdek
//

#ifndef BONUS_H_
# define BONUS_H_

#include <Geometry.hh>
#include <Texture.hh>
#include <vector>
#include <string>
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include "../AObject.hpp"
#include "../Bonhomme/Bonhomme.hpp"

class Bonhomme;

# define NONE -42

enum
  {
    SPEED = 1,
    PLUS_BOMBE = 2,
    PLUS_RANGE = 3,
    SLOW_ENEMY = 4,
    THROW = 5,
    KICK = 6,
  };

class Bonus : public AObject
{
protected :
  int		_effect;
  gdl::Texture	*_texture;
  gdl::Geometry	_geometry;
  glm::vec4	_vec;
  double	_height_vec;

public :
  Bonus(int, int, Map *data, int effect = -1, glm::vec4 vec = glm::vec4(1, 1, 1, 1));
  ~Bonus(void);

// Setters & getters
  int		get_effect(void) const;
  void		set_effect(int);

  virtual bool	initialize();
  virtual int update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::AShader &shader, gdl::Clock const &clock);
  virtual void	is_touched_by_fire(Bonhomme *);
  void		is_touched_by_human(Bonhomme *bonhomme);
  void		save(std::ofstream &ofs) const ;
  void		load(std::vector< double> &values);
};

#endif /* !BONUS_H_ */
