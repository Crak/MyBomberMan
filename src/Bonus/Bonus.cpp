//
// Bonus.cpp for  in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 03:47:15 2014 corentin bajdek
// Last update Sun Jun 15 23:24:26 2014 corentin bajdek
//

#include "Bonus.hpp"

/*
** Constructeurs / Destructeurs
*/

Bonus::Bonus(int __pos_x, int __pos_y, Map *data, int effect, glm::vec4 vec) :
  AObject(__pos_x, __pos_y, data), _vec(vec)
{
  int	randy;

  if (effect == -1)
    {
      randy = rand() % 6 + 1;
      if (randy >= 4)
	randy = rand() % 6 + 1;
    }
  else
    randy = effect;

  _effect = randy;
  translate(glm::vec3(__pos_x, __pos_y, 0));
  _type = BONUS;
  return ;
}

Bonus::~Bonus(void) { return ; }

/*
** Setters/getters
*/

int		Bonus::get_effect(void) const { return (this->_effect) ; }
void		Bonus::set_effect(int __effect) { this->_effect = __effect ; }


bool		Bonus::initialize(void)
{
  // gerer les erreurs de textures ici

  if (_effect == SPEED)
    _texture = _data->getTextContainer()->getTexture("./assets/Speed.tga");
  if (_effect == PLUS_BOMBE)
    _texture = _data->getTextContainer()->getTexture("./assets/Plus_bombe.tga");
  if (_effect == PLUS_RANGE)
    _texture = _data->getTextContainer()->getTexture("./assets/Plus_range.tga");
  if (_effect == SLOW_ENEMY)
    _texture = _data->getTextContainer()->getTexture("./assets/Slow_enemy.tga");
  if (_effect == KICK)
    _texture = _data->getTextContainer()->getTexture("./assets/Kick.tga");
  if (_effect == THROW)
    _texture = _data->getTextContainer()->getTexture("./assets/Throw.tga");

  // on set la color d'une premiere face
  _geometry.pushVertex(glm::vec3(0.4, 0.01, 0.3));
  _geometry.pushVertex(glm::vec3(0.4, 0.01, -0.3));
  _geometry.pushVertex(glm::vec3(-0.4, 0.01, -0.3));
  _geometry.pushVertex(glm::vec3(-0.4, 0.01, 0.3));

  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  
  _geometry.pushVertex(glm::vec3(0.4, -0.0, -0.3));
  _geometry.pushVertex(glm::vec3(0.4, -0.0, 0.3));
  _geometry.pushVertex(glm::vec3(-0.4, -0.0, 0.3));
  _geometry.pushVertex(glm::vec3(-0.4, -0.0, -0.3));

  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  // Tres important, on n'oublie pas de build la geometrie pour envoyer ses informations a la carte
  _geometry.build();
  _initialize = true;
  return (true);
}

int	Bonus::update(gdl::Clock const &clock, gdl::Input &input)
{
  int	state;

  (void)input;
  _lifetime += clock.getElapsed();
  state = (int)(_lifetime * 100) % 200;
  if (state > 100)
    state = 100 - (state - 100);
  rotate(glm::vec3(0, 0, 1), clock.getElapsed() * 50);
  _position.z = 0;
  translate(glm::vec3(0, 0, (double)state / 200));
  if (_dying == true)
    return (false);
  return (true);
}

void	Bonus::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;
  _texture->bind();
  _geometry.draw(shader, getTransformation(), GL_QUADS);
}

void	Bonus::is_touched_by_fire(Bonhomme *father)
{
  (void)father;
}

void		Bonus::is_touched_by_human(Bonhomme *bonhomme)
{
  Bonhomme	*target;
  
  bonhomme->set_score(bonhomme->get_score() + 50);
  if (_effect == SPEED)
    bonhomme->set_speed(bonhomme->get_speed() + 0.25);
  if (_effect == PLUS_BOMBE)
    bonhomme->set_bombes_max(bonhomme->get_bombes_max() + 1);
  if (_effect == PLUS_RANGE)
    bonhomme->set_explosion_len(bonhomme->get_explosion_len() + 1);
  if (_effect == SLOW_ENEMY)
    {
      for (size_t i = 0; i < _data->Objects.size(); ++i)
	if (_data->Objects[i]->get_type() == BONHOMME)
	  {
	    if ((target = dynamic_cast<Bonhomme *>(_data->Objects[i])) == NULL)
	      std::cerr << "Impossible error happened" << std::endl;
	    if (target != bonhomme)
	      target->set_speed(target->get_speed() - 0.10);
	  }
    }
  if (_effect == KICK)
    {
      bonhomme->set_special(KICK);
      bonhomme->set_nbkick(5);
      bonhomme->set_nbthrow(0);
    }
  if (_effect == THROW)
    {
      bonhomme->set_special(THROW);
      bonhomme->set_nbkick(0);
      bonhomme->set_nbthrow(5);
    }
  _dying = true;
}

void		Bonus::save(std::ofstream &ofs) const
{
  ofs << "Bonus" << "::";
  ofs << _pos_x << "::";
  ofs << _pos_y << "::";
  ofs << _effect << "::";
}

void		Bonus::load(std::vector< double> &values)
{
  _pos_x = (int)values[0];
  _pos_y = (int)values[1];
  _effect = (int)values[2];
}
