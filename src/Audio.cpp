
#include "Audio.h"
#include <fmod.h>
#include <string>
#include <fstream>

Audio::Audio()
  : _sound(true)
{
  
    FMOD_System_Create(&sys);
    FMOD_System_Init(sys, 3, FMOD_INIT_NORMAL, NULL);
    FMOD_System_CreateStream(sys, "BOMBERMAN.wav", FMOD_HARDWARE | FMOD_LOOP_NORMAL | FMOD_2D, 0, &_music);
    FMOD_System_CreateStream(sys, "bomb.wav", FMOD_HARDWARE | FMOD_LOOP_OFF | FMOD_2D, 0, &_bomb);
}

Audio::~Audio()
{
  
    FMOD_System_Release(sys);
    FMOD_Sound_Release(_music);
    FMOD_Sound_Release(_bomb);
}

void Audio::Play_music()
{
      FMOD_System_PlaySound(sys, FMOD_CHANNEL_FREE, _music, 0, &_cmusic);
}

void Audio::Play_blast()
{
  if (_sound)
    {
      FMOD_System_PlaySound(sys, FMOD_CHANNEL_FREE, _bomb, 0, &_cbomb);
      FMOD_Channel_SetVolume(_cbomb, 0.3);
    }
}

void Audio::set_music(float f)
{
  FMOD_Channel_SetVolume(_cmusic, f);
}

void Audio::set_blast(float f)
{
  FMOD_Channel_SetVolume(_cbomb, f);
}

void Audio::set_sound(int mute)
{
     _sound = mute;
}
