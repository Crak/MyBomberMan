//
// TextContainer.hpp for TextContainer in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by Lucas SARKADI
// Login   <sarkad_l@epitech.net>
// 
// Started on  Fri May  9 04:11:15 2014 Lucas SARKADI
// Last update Sun Jun 15 20:24:16 2014 Lucas SARKADI
//

#ifndef TEXTCONTAINER_HPP_
# define TEXTCONTAINER_HPP_

#include <Texture.hh>
#include <map>
#include <string>

class	TextContainer
{
 private:
  std::map<std::string, gdl::Texture *>	_tabText;
 public:
  TextContainer(const TextContainer &);
  TextContainer();
  ~TextContainer();
  gdl::Texture	*getTexture(const std::string &);
  bool		Load_everything();
};

#endif /* TEXTCONTAINER_HPP_ */
