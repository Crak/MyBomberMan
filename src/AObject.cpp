//
// AObject.cpp for AObject in /home/bajdek_c/bomber
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  2 00:15:15 2014 corentin bajdek
// Last update Sun Jun 15 22:45:44 2014 corentin bajdek
//

#include "AObject.hpp"

AObject::AObject(int pos_x, int pos_y, Map *data)
  : _type(0), _position(0, 0, 0), _rotation(0, 0, 0), _scale(1, 1, 1),
    _pos_x(pos_x), _pos_y(pos_y), _lifetime(0), _subtype(DEFAULT), _data(data),
    _initialize(false)
{
  _dying = false;
  return ;
}

AObject::~AObject()
{ return ; }

int		AObject::get_pos_x(void) const { return (this->_pos_x) ; }
int		AObject::get_pos_y(void) const { return (this->_pos_y) ; }
void		AObject::set_pos_x(int pos_x) { this->_pos_x = pos_x ; }
void		AObject::set_pos_y(int pos_y) { this->_pos_y = pos_y ; }

bool	AObject::initialize()
{
  _initialize = true;
  return (true);
}

int	AObject::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)clock;
  (void)input;
  return (true);
}

void	AObject::translate(glm::vec3 const &v)
{
  _position += v;
}

void	AObject::rotate(glm::vec3 const& axis, float angle)
{
  _rotation += axis * angle;
}

void	AObject::scale(glm::vec3 const& scale)
{
  _scale *= scale;
}

glm::mat4	AObject::getTransformation()
{
  glm::mat4 transform(1);

  transform = glm::translate(transform, _position);
  transform = glm::scale(transform, _scale);
  transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
  transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
  transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));

  return (transform);
}

void	AObject::save(std::ofstream &ofs) const
{
  (void)ofs;
  return ;
}

void	AObject::load(std::vector< double> &values)
{
  (void)values;
  return ;
}

int	AObject::get_type()
{
  return (_type);
}

void	AObject::is_touched_by_fire(Bonhomme *father)
{
  (void)father;
  return ;
}

int	AObject::get_subtype() { return (_subtype); }
