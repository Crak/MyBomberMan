//
// Save.hpp for save in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 00:26:29 2014 corentin bajdek
// Last update Sun Jun 15 11:52:36 2014 corentin bajdek
//

#ifndef SAVE_H_
# define SAVE_H_

#include <string>
#include <iostream>
#include <fstream>
#include <cstddef>
#include <vector>
#include "AObject.hpp"
#include "Bonhomme/Bonhomme.hpp"
#include "Bombes/Bombes.hpp"
#include "Bonus/Bonus.hpp"
#include "Fire/Fire.hpp"
#include "Wall/Wall.hpp"
#include "Wall/Wall_Destructible.hpp"
#include "Mod.hpp"
#include "Map.hpp"
#include "Loading_map.hpp"

class Save
{
private:
  gdl::Clock		_clock;
  gdl::Input		_input;
  gdl::BasicShader	_shader;
  bool			add_to_map(const std::string &line, Mod &mode, Map *data, int nbline);
  void			aff_map(std::vector< std::vector<int> > &map, int width, int length);
  int			get_distance(int x, int y, int x2, int y2);
  int			get_smallest(std::vector< std::vector<int> > &map, int x, int y, Map *data);
  bool			my_look_around(std::vector< std::vector<int> > &map, int x, int y, Map *data);

protected :
  const std::string	_path;
  int			_ID;
  int			_bool;
  gdl::SdlContext	&_context;

public :
  Save(const std::string &path, gdl::SdlContext &_context);
  ~Save(void);

  bool	load_map(const std::string &file, Mod &mode, Map *data);
  bool	generate_map(Mod &mode, Map *data, int width, int length);
  bool	load_saved(const std::string *file, Map *data);
};

#endif /* !SAVE_H_ */
