//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Sun Jun 15 19:13:09 2014 corentin bajdek
//

#ifndef MENU_HPP_
# define MENU_HPP_

#include <vector>
#include <Game.hh>
#include <SdlContext.hh>
#include "../Map.hpp"
#include "Backmenu.hpp"

enum
  {
    HISTOIRE,
    MULT_CH,
    OPTIONS,
    AUTRES,
    MULTIJOUEUR,
    VIDEO,
    SONS,
    HISTOIRE_NG,
    MULT_CHMAP,
    HISTOIRE_LOAD,
    MULT_LOAD,
    AUTRES_CREDIT,
    GLOBAL,
    ENTER,
    ESCAPE,
  };

class ASubmenu;

class Menu
{
private :
  gdl::SdlContext		_context;
  gdl::Clock			_clock;
  gdl::Input			_input;
  gdl::BasicShader		_shader;
  Mod				&_mod;
  Map				*_data;
  ASubmenu			*_Global;
  std::vector<ASubmenu *>	_box;
  Background			*_back;
  int				_width_screen;
  int				_length_screen;

public :
  Menu(Mod &mod, Map *data, int width_screen, int length_screen);
  ~Menu();

  int			initialize();
  int			update();
  void			draw();
  void			set_activate(int wich, bool act);
  Map			*get_data(void) {return (_data);}
  gdl::SdlContext	&get_context(void) { return (_context);}
};

#endif /* !MENU_HPP_*/
