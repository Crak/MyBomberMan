//
// Submenu_mult_chmap.hpp for 22 in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri Jun 13 19:38:04 2014 corentin bajdek
// Last update Sun Jun 15 16:59:51 2014 corentin bajdek
//

#ifndef SUBMENU_MULT_CHMAP_HPP_
# define SUBMENU_MULT_CHMAP_HPP_

#include <dirent.h> // opendir
#include <algorithm>
#include <sys/types.h>
#include "ASubmenu.hpp"
#include "../Save.hpp"
#include "../Mod.hpp"
#include "../GameEngine/GameEngine.hpp"

class Submenu_mult_chmap : public ASubmenu
{
private :
  std::vector<std::string *>	_Maps;
  /* Box de tous les bra */
  std::vector<Bra_title *>	_Box_bra;
  /* Différentes map de players */
  std::vector<Title *>		_Map_choices;
  Bra_title			*_Map_bra;
  /* Nombre de vies */
  std::vector<Title *>		_Vies_choices;
  Bra_title			*_Vies_bra;
  /* Temps en minutes */
  std::vector<Title *>		_Minutes_choices;
  Bra_title			*_Minutes_bra;
  /* Modes */
  std::vector<Title *>		_Modes_choices;
  Bra_title			*_Modes_bra;
  /* Nbplayers */
  std::vector<Title *>		_Nb_players_choices;
  Bra_title			*_Nb_players_bra;
  /* Fréquence bonus*/
  std::vector<Title *>		_Bonus_choices;
  Bra_title			*_Bonus_bra;

  /* Difficulté */
  Title					*_Map;

  Title					*_Temps;
  Title					*_Mn;
  Title					*_Vies;
  Title					*_Mode;
  Title					*_Bonus;
  Title					*_Nb_players;
  Title					*_Launch_game;
  Title					*_Return;


public :
  Submenu_mult_chmap(Map *data, Menu *menu) : ASubmenu(data, menu, 7)
  {
    std::vector<std::string *>	result;
    dirent			*de;
    DIR				*dp;

    dp = opendir("./save/multijoueur");
    if (dp)
      {
	while ((de = readdir(dp)) != NULL)
	  result.push_back (new std::string(de->d_name));
	closedir(dp);
	std::sort(result.begin(), result.end());
      }
    for (size_t i =  0; i < result.size(); i++)
      {
	if (result[i]->at(0) != '.') 
	  {
	    _Maps.push_back(result[i]);
	    _Map_choices.push_back(new Title(_data, result[i]->c_str(), 50));
	  }
      }

    _Map = new Title(_data, "Choix de la map :", 50);
    _Return =  new Title(_data, "Retour", 50);
    _Launch_game = new Title(_data, "Lancer la partie", 50);
    _Mode = new Title(_data, "Mode :", 50);
    _Nb_players = new Title(_data, "Nombre Players :", 50);
    _Bonus = new Title(_data, "Frequence bonus :", 50);
    _Temps = new Title(_data, "Temps :", 40);
    _Vies = new Title(_data, "Nombre de vies :", 40);
    _Mn = new Title(_data, "minutes", 40);

    this->set_backmenu(900, 7 * 140);

    _box.push_back(_Map);
    _box.push_back(_Mode);
    _box.push_back(_Temps);
    _box.push_back(_Vies);
    _box.push_back(_Nb_players);
    _box.push_back(_Bonus);
    _box.push_back(_Launch_game);
    _box.push_back(_Return);

    /* Différents choix de map */
    if (_Map_choices.size() != 0)
      {
	_Map_bra = new Bra_title(_data, _Map_choices, 50);
	_Box_bra.push_back(_Map_bra);
      }
    /* Différents modes*/
    _Modes_choices.push_back(new Title(_data, "Classic", 50));
    _Modes_choices.push_back(new Title(_data, "Temps", 50));
    _Modes_choices.push_back(new Title(_data, "Vies", 50));
    _Modes_bra = new Bra_title(_data, _Modes_choices, 50);
    _Box_bra.push_back(_Modes_bra);
    /* Différents Temps */
    for (int i = 1; i <= 5; i++)
      _Minutes_choices.push_back(new Title(_data, i, 40));
    _Minutes_bra =  new Bra_title(_data, _Minutes_choices, 40);
    _Box_bra.push_back(_Minutes_bra);
    /* Différentes vies */
    for (int i = 1; i <= 5; i++)
      _Vies_choices.push_back(new Title(_data, i, 40));
    _Vies_bra =  new Bra_title(_data, _Vies_choices, 40);
    _Box_bra.push_back(_Vies_bra);
    /* Différents Nb players*/
    for (int i = 1; i <= 2; i++)
      _Nb_players_choices.push_back(new Title(_data, i, 50));      
    _Nb_players_bra = new Bra_title(_data, _Nb_players_choices, 50);
    _Box_bra.push_back(_Nb_players_bra);
    /* Différents bonus */
    _Bonus_choices.push_back(new Title(_data, "Peu", 50));
    _Bonus_choices.push_back(new Title(_data, "Moyen", 50));
    _Bonus_choices.push_back(new Title(_data, "Nombreux", 50));
    _Bonus_bra = new Bra_title(_data, _Bonus_choices, 50);
    _Box_bra.push_back(_Bonus_bra);
    _Bonus_bra->set_current(MOYEN);
    _Bonus_bra->set_selected(true);
    _Bonus_bra->set_selected(false);
    /* regle de visibilité */
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      _Box_bra[i]->set_visible(true);
  }

  virtual ~Submenu_mult_chmap()
  {
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      delete _Box_bra[i];
    for (size_t i = 0; i < _Maps.size() ; i++)
      delete _Maps[i];
  }

  void		set_modes()
  {
    _menu->get_data()->get_mod().setIntMod("HumanPlayers", _Nb_players_bra->get_current() + 1);
    _menu->get_data()->get_mod().setIntMod("GameMode", _Modes_bra->get_current());
    if (_Modes_bra->get_current() == MODE_VIE)
      _menu->get_data()->get_mod().setIntMod("NbLife", _Vies_bra->get_current() + 1);      
    if (_Modes_bra->get_current() == MODE_TEMPS)
      _menu->get_data()->get_mod().setIntMod("NbTime", _Minutes_bra->get_current() + 1);      
    _menu->get_data()->get_mod().setIntMod("NbBonus", _Bonus_bra->get_current());
  }

  virtual void	new_game(const std::string *map)
  {
    Save	save("./save/multijoueur/", _menu->get_context());
    GameEngine	*engine;

    if ((save.load_map(*map, _menu->get_data()->get_mod(), _menu->get_data())) == false)
      {
	std::cerr << "that's bad (map)" << std::endl;
	return ;
      }
    engine = _data->create_gameengine(_data->get_mod(), _menu->get_context());
    
    if (engine->initialize() == false)
      {
	std::cerr << "that's bad (gameengine)" << std::endl;
	return ;
      }
    while (engine->update() == true)
      engine->draw();
    delete engine;
    if (_data->get_mod().getIntMod("Action") == RESTART)
      {
	_data->get_mod().setIntMod("Action", NONE);
	if (_data->get_mod().getIntMod("HumanPlayers") == 1)
	  new_game(map);
      }
  }

  int		handle_activate()
  {
    if (_Return->get_selected() == true)
      {
	_menu->set_activate(MULT_CHMAP, false);
	_menu->set_activate(MULT_CH, true);
      }
    else if (_Launch_game->get_selected() == true && _Map_choices.size() != 0)
      {
	set_modes();
	new_game(_Maps[_Map_bra->get_current()]);
      }
    return (1);
  }

  void		handle_selection(int oldcurrent)
  {
    if (_current != oldcurrent)
      _box[oldcurrent - 1]->set_selected(false);
    _box[_current - 1]->set_selected(true);
    _Modes_bra->set_selected(_Mode->get_selected());
    _Bonus_bra->set_selected(_Bonus->get_selected());
    _Nb_players_bra->set_selected(_Nb_players->get_selected());
    _Minutes_bra->set_selected(_Temps->get_selected());
    _Mn->set_selected(_Temps->get_selected());
    _Vies_bra->set_selected(_Vies->get_selected());
    _Temps->set_visible(false);
    _Minutes_bra->set_visible(false);
    _Mn->set_visible(false);
    _Vies->set_visible(false);
    _Vies_bra->set_visible(false);
    if (_Modes_bra->get_current() == TEMPS)
      {
	_Temps->set_visible(true);
	_Minutes_bra->set_visible(true);
	_Mn->set_visible(true);
      }
    else if (_Modes_bra->get_current() == VIES)
      {
	_Vies->set_visible(true);
	_Vies_bra->set_visible(true);
      }
    if (_Map_choices.size() != 0)
      _Map_bra->set_selected(_Map->get_selected());
  }

  void		handle_input(gdl::Input &input)
  {
    _last_input = get_input(input);
    if (_last_input == UP)
      do
	{
	  _current--;
	  if (_current == 0)
	    _current = _box.size();
	}
      while (_box[_current - 1]->get_visible() == false);
    else if (_last_input == DOWN)
      do
	{
	  _current++;
	  if (_current == (int)_box.size() + 1)
	    _current = 1;
	}
      while (_box[_current - 1]->get_visible() == false);
    else if (_last_input == RIGHT)
      {
	for (size_t i = 0; i < _Box_bra.size(); ++i)
	  if (_box[i]->get_selected() == true)
	    _Box_bra[i]->set_current(_Box_bra[i]->get_current() + 1);
      }
    else if (_last_input == LEFT)
      {
	for (size_t i = 0; i < _Box_bra.size(); ++i)
	  if (_box[i]->get_selected() == true)
	    _Box_bra[i]->set_current(_Box_bra[i]->get_current() - 1);
      }
    if (_last_input == ENTER)
      this->handle_activate();
    _last_input = NONE;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	handle_input(input);
	this->handle_selection(oldcurrent);
      }
    return (true);
  }
  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {
	_backmenu->draw(shader, 0, 100);
	if (_Map_choices.size() != 0)
	  _Map_bra->draw(shader, 225, 50);
	_Modes_bra->draw(shader, 125 * 3, 200);
	_Minutes_bra->draw(shader, 125 * 3 + 50, 220);
	_Vies_bra->draw(shader, 125 * 3 + 50, 400);
	_Nb_players_bra->draw(shader, 125 * 4, 450);
	_Bonus_bra->draw(shader, 125 * 5, 480);
      }
    _Map->draw(shader, 125 * 1, 160, 50);
    _Mode->draw(shader, 125 * 3, 160, 50);
    _Temps->draw(shader, 125 * 3 + 50, 160, 80);
    _Mn->draw(shader, 125 * 3 + 50, 160, 300);
    _Vies->draw(shader, 125 * 3 + 50, 160, 80);
    _Nb_players->draw(shader, 125 * 4, 160, 50);
    _Bonus->draw(shader, 125 * 5, 160, 50);
    _Launch_game->draw(shader, 125 * 6, 160, 50);
    _Return->draw(shader, 125 * 7, 160, 50);
  }
  };

#endif /* */
