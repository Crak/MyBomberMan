//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Thu Jun  5 18:55:30 2014 corentin bajdek
//

#ifndef SUBMENU_VIDEO_HPP_
# define SUBMENU_VIDEO_HPP_

#include "ASubmenu.hpp"

class Submenu_videos : public ASubmenu
{
private :
  Title				*_Overlay;
  Title				*_Return;
  /* Différents overlays */
  std::vector<Title *>		_Ov_choices;
  Bra_title			*_Ov_bra;
  Overlay			*_Ovl1;
  Overlay_long			*_Ovl2;
  Overlay_top			*_Ovl3;

public :
  Submenu_videos(Map *data, Menu *menu) : ASubmenu(data, menu, 2)
  {
    _Overlay = new Title(_data, "Overlay :", 50);
    _Return =  new Title(_data, "Retour", 50);

    this->set_backmenu(600, 2 * 140);

    _Ov_choices.push_back(new Title(_data, "Theme 1", 50));
    _Ov_choices.push_back(new Title(_data, "Theme 2", 50));
    _Ov_bra = new Bra_title(_data, _Ov_choices, 50);
    _Ov_bra->set_visible(true);

    _box.push_back(_Overlay);
    _box.push_back(_Return);

    _Ovl1 = _data->create_overlay();
    _Ovl2 = _data->create_overlay_long();
    _Ovl3 = _data->create_overlay_top();
    _Ovl1->initialize();
    _Ovl2->initialize();
    _Ovl3->initialize();
    _data->get_mod().setIntMod(std::string("Overlay"), _Ov_bra->get_current());
  }
  virtual ~Submenu_videos()
  {
    delete _Ov_bra;
    delete _Ovl1;
    delete _Ovl2;
    delete _Ovl3;
  }

  void		handle_input(gdl::Input &input)
  {
    /* récupere les inputs de touches */
    _last_input = get_input(input);
    if (_last_input == UP)
      _current--;
    else if (_last_input == DOWN)
      _current++;
    else if ((_last_input == RIGHT || _last_input == LEFT) && _Overlay->get_selected() == true)
      {
	if (_last_input == RIGHT)
	  _Ov_bra->set_current(_Ov_bra->get_current() + 1);
	else
	  _Ov_bra->set_current(_Ov_bra->get_current() - 1);
	_data->get_mod().setIntMod("Overlay", _Ov_bra->get_current());
	delete _Ovl1;
	delete _Ovl2;
	delete _Ovl3;
	_Ovl1 = _data->create_overlay();
	_Ovl2 = _data->create_overlay_long();
	_Ovl3 = _data->create_overlay_top();
	_Ovl1->initialize();
	_Ovl2->initialize();
	_Ovl3->initialize();
      }
    else if (_last_input == ENTER)
      if (_Return->get_selected() == true)
	{
	  _menu->set_activate(VIDEO, false);
	  _menu->set_activate(OPTIONS, true);
	}
    _last_input = NONE;
    if (_current == 0)
      _current = _box.size();
    if (_current == (int)_box.size() + 1)
      _current = 1;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	handle_input(input);
	if (_current != oldcurrent)
	  _box[oldcurrent - 1]->set_selected(false);
	_box[_current - 1]->set_selected(true);
	_Ov_bra->set_selected(_Overlay->get_selected());
	_Ovl1->update(clock, input);
	_Ovl2->update(clock, input);
	_Ovl3->update(clock, input);
      }
    return (1);
  }
  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {
	_backmenu->draw(shader, 0, 250);
	_Ov_bra->draw(shader, 280, 300);
	if (_Overlay->get_selected() == true)
	  {
	    _Ovl1->draw(shader);
	    _Ovl2->draw(shader);
	    _Ovl3->draw(shader);
	  }
      }
    _Overlay->draw(shader, 280, 160, 50);
    _Return->draw(shader, 280 + 150 * 1, 160, 50);
  }
};

#endif /* */
