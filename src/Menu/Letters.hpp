//
// Minimap.hpp for header in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May  8 17:41:17 2014 corentin bajdek
// Last update Tue Jun 10 00:10:49 2014 corentin bajdek
//

#ifndef LETTERS_HPP_
# define LETTERS_HPP_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "../Map.hpp"

class Letter
{
private :
  gdl::Texture			*_texture;
  gdl::Geometry			_geometry;
  gdl::BasicShader		_shader;
  glm::vec3			_position;
  glm::vec3			_rotation;
  glm::vec3			_scale;  
  double			_pos_x;
  double			_pos_y;
  const char			_c;
  Map				*_data;
  const float			_size;
  glm::vec4			_color;

public :
  Letter(const char c, Map *data, const float size, const glm::vec4 color)
    : _position(0, 0, 0), _rotation(0, 0, 0), _scale(1, 1, 1), _c(c), _data(data), _size(size), _color(color) {}
  ~Letter() {}

  glm::mat4	getTransformation()
  {
    glm::mat4 transform(1); // On cree une matrice identite

    transform = glm::translate(transform, _position);

    transform = glm::scale(transform, _scale);

    transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
    transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
    transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
    return (transform);
  }

  bool	initialize()
  {
    _texture = _data->getTextContainer()->getTexture("./assets/Letters/Letters.tga");
  
    _geometry.setColor(_color);
    _geometry.pushVertex(glm::vec3(0, _size, 0));
    _geometry.pushVertex(glm::vec3(_size, _size, 0));
    _geometry.pushVertex(glm::vec3(_size, 0, 0));
    _geometry.pushVertex(glm::vec3(0, 0, 0));

    _geometry.pushUv(glm::vec2((float)(_c % 16) / 16.0f,
    			       1.0f - (float)(_c / 16 + 1) / 16.0f)); //coin bas gauche
    _geometry.pushUv(glm::vec2((float)(_c % 16 + 1) / 16.0f,
    			       1.0f - (float)(_c / 16 + 1) / 16.0f)); //coin bas droite
    _geometry.pushUv(glm::vec2((float)(_c % 16 + 1) / 16.0f,
    			       1.0f - (float)(_c / 16) / 16.0f)); //coin haut droite
    _geometry.pushUv(glm::vec2((float)(_c % 16) / 16.0f,
    			       1.0f - (float)(_c / 16) / 16.0f)); //coin haut gauche
    _geometry.build();
    return true;
  }

  void			draw(gdl::AShader &shader, double pos_x, double pos_y)
  {
    int			_width = 900; // a envoyer en param
    int			_length = 1000; // a envoyer en param
    glm::mat4		projection = glm::ortho(0.0f, (float)_width, (float)_length, 0.0f, -1.0f, 1.0f);
    
    shader.bind();
    shader.setUniform("view", glm::mat4(1));
    shader.setUniform("projection", projection);
    _texture->bind();
    _position.x = pos_x;
    _position.y = pos_y;

    /* activation de la transparence et desactivation du test de profondeur (inutile en 2D) */
    glDisable(GL_DEPTH_TEST);
    glAlphaFunc(GL_GREATER, 0.1f);
    glEnable(GL_ALPHA_TEST);

    _geometry.draw(shader, getTransformation(), GL_QUADS);

    /* remise en état */
    glDisable(GL_ALPHA_TEST);
    glEnable(GL_DEPTH_TEST);
  }
};

#endif /* !LETTER_HPP_ */
