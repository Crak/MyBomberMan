//
// ASubmenu.hpp for submenu in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May 22 11:50:52 2014 corentin bajdek
// Last update Sun Jun 15 18:06:58 2014 corentin bajdek
//

#ifndef ASUBMENU_HPP_
# define ASUBMENU_HPP_

#include "../Fire/Fire.hpp"
#include "../Bonus/Bonus.hpp"
#include "Menu.hpp"
#include "Title.hpp"
#include "Bra_title.hpp"

class ASubmenu
{
protected :
  bool				_activate;
  Map				*_data;
  std::vector<Title *>		_box;
  int				_current;
  int				_last_input;
  Menu				*_menu;
  int				_on_hold;
  Backmenu			*_backmenu;
  std::vector<bool>		_inputs;
  std::vector<int>		_directions;

public :
  ASubmenu(Map *data, Menu *menu, int nbitems) :  _activate(false), _data(data), _current(1), _last_input(NONE), _menu(menu), _on_hold(NONE) 
  {
    _backmenu = new Backmenu("./assets/White.tga", _data, 500, nbitems * 140);
    _backmenu->initialize();
    
    _directions.push_back(UP);
    _directions.push_back(DOWN);
    _directions.push_back(ENTER);
    _directions.push_back(LEFT);
    _directions.push_back(RIGHT);
    _directions.push_back(ESCAPE);

    for (size_t i = 0; i < 6; ++i)
      _inputs.push_back(false);
  }
  virtual ~ASubmenu()
  {
    for (size_t i = 0; i < _box.size(); ++i)
      delete _box[i];
  }

  int	get_input(gdl::Input &input)
  {
    _inputs[0] = input.getKey(SDLK_UP, false);
    _inputs[1] = input.getKey(SDLK_DOWN, false);
    _inputs[2] = input.getKey(SDLK_RETURN, false);
    _inputs[3] = input.getKey(SDLK_LEFT, false);
    _inputs[4] = input.getKey(SDLK_RIGHT, false);
    _inputs[5] = input.getKey(SDLK_ESCAPE, false);

    for (size_t i = 0; i < 6; i++)
      if (_inputs[i] == true)
	_on_hold = _directions[i];

    for (size_t i = 0; i < 6; i++)
      if (_inputs[i] == false && _on_hold == _directions[i])
	{
	  _on_hold = NONE;
	  return (_directions[i]);
	}
    return (NONE);
  }

  virtual int  	update(gdl::Clock const &clock, gdl::Input &input)
  {
    (void)clock;
    (void)input;
    return (1);
  }

  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      _backmenu->draw(shader, 0, 250);
    for (size_t i = 0; i < _box.size(); ++i)
      _box[i]->draw(shader, 280 + (i * 150), 160);
  }

  virtual void		set_activate(bool act) 
  {
    if (act == true && _activate == false)
      {
	_activate = act;
	_box[0]->set_selected(true);
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->set_visible(true);
      }
    else if (act == false && _activate == true)
      {
	_activate = act;
	_current = 1;
	for (size_t i = 0; i < _box.size(); ++i)
	  {
	    _box[i]->set_visible(false);
	    _box[i]->set_selected(false);
	  }
      }
  }

  bool		get_activate(void) { return _activate ;}
  
  void		set_backmenu_visibility(bool act)
  {
    _backmenu->set_visible(act);
  }

  void		set_backmenu(int width, int length)
  {
    delete _backmenu;
    _backmenu = new Backmenu("./assets/White.tga", _data, width, length);
    _backmenu->initialize();
  }
};

#endif /* */
