//
// Backmenu.hpp for Backmenu in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Tue May 20 00:28:50 2014 corentin bajdek
// Last update Sun Jun 15 18:31:40 2014 corentin bajdek
//

#ifndef BACKMENU_HPP_
# define BACKMENU_HPP_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>

class Backmenu
{
private :
  gdl::Texture	 		*_texture;
  gdl::Geometry			_geometry;
  gdl::BasicShader		_shader;
  glm::vec3			_position;
  glm::vec3			_rotation;
  glm::vec3			_scale;  
  Map				*_data;
  const std::string		_path;
  int				_width;
  int				_len;
  bool				_invis;

public :
  Backmenu(const std::string path, Map *data, int width, int len) : _position(0, 0, 0), _rotation(0, 0, 0), _scale(1, 1, 1), _data(data), _path(path), _width(width), _len(len), _invis(true) {}
  ~Backmenu() {}

  glm::mat4	getTransformation()
  {
    glm::mat4 transform(1); // On cree une matrice identite

    transform = glm::translate(transform, _position);

    transform = glm::scale(transform, _scale);

    transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
    transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
    transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
    return (transform);
  }

  bool	initialize()
  {
    _texture = _data->getTextContainer()->getTexture(_path);

    _geometry.setColor(glm::vec4(193.0f / 255.0f, 205.0f / 255.0f, 205.0f / 255.0f, 0.66));
    _geometry.pushVertex(glm::vec3(0, _len, 0));
    _geometry.pushVertex(glm::vec3(_width, _len, 0));
    _geometry.pushVertex(glm::vec3(_width, 0, 0));
    _geometry.pushVertex(glm::vec3(0, 0, 0));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));
    _geometry.build();
    return true;
  }

  void			draw(gdl::AShader &shader, double pos_x, double pos_y)
  {
    int			_width = 1000; // a envoyer en param
    int			_length = 1000; // a envoyer en param
    glm::mat4		projection = glm::ortho(0.0f, (float)_width, (float)_length, 0.0f, -1.0f, 1.0f);
    
    shader.bind();
    shader.setUniform("view", glm::mat4(1));
    shader.setUniform("projection", projection);
    _texture->bind();
    _position.x = pos_x;
    _position.y = pos_y;

    /* activation de la transparence et desactivation du test de profondeur (inutile en 2D) */

    if (_invis == true)
      {
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
      }
    else
      glDisable(GL_DEPTH_TEST);
    _geometry.draw(shader, getTransformation(), GL_QUADS);
    /* remise en état */
    if (_invis == true)
      glDisable(GL_BLEND);
    else
      glEnable(GL_DEPTH_TEST);      
  }
  
  void			set_visible(bool act)
  {
    _invis = act;
  }
};

#endif /* !LETTER_HPP_ */
