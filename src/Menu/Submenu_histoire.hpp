//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Sun Jun 15 14:26:18 2014 corentin bajdek
//

#ifndef SUBMENU_HISTOIRE_HPP_
# define SUBMENU_HISTOIRE_HPP_

#include "ASubmenu.hpp" 

class Submenu_histoire : public ASubmenu
{
private :
  Title				*_New_game;
  Title				*_Load;
  Title				*_Return;

public :
  Submenu_histoire(Map *data, Menu *menu) : ASubmenu(data, menu, 3)
  {
    _New_game  = new Title(_data, "Nouvelle partie", 50);
    _Load = new Title(_data, "Charger", 50);
    _Return =  new Title(_data, "Retour", 50);

    _box.push_back(_New_game);
    _box.push_back(_Load);
    _box.push_back(_Return);
  }
  virtual ~Submenu_histoire()
  {
  }
  
  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	/* récupere les inputs de touches */
	_last_input = get_input(input);
	if (_last_input == UP)
	  _current--;
	else if (_last_input == DOWN)
	  _current++;
	if (_current == 0)
	  _current = _box.size();
	if (_current == (int)_box.size() + 1)
	  _current = 1;
	if (_last_input == ENTER)
	  {
	    if (_Return->get_selected() == true)
	      {
		_menu->set_activate(HISTOIRE, false);
		_menu->set_activate(GLOBAL, true);
	      }
	    else if (_New_game->get_selected() == true)
	      {
		_menu->set_activate(HISTOIRE, false);
		_menu->set_activate(HISTOIRE_NG, true);
	      }
	    else if (_Load->get_selected() == true)
	      {
		_menu->set_activate(HISTOIRE, false);
		_menu->set_activate(HISTOIRE_LOAD, true);
	      }
	  }
	_last_input = NONE;
	if (_current != oldcurrent)
	  _box[oldcurrent - 1]->set_selected(false);
	_box[_current - 1]->set_selected(true);
      }
    return (1);
  }
};

#endif /* */
