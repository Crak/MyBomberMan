//
// Submenu_pause.hpp for submenu in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 25 16:56:57 2014 corentin bajdek
// Last update Thu Jun 12 07:14:37 2014 corentin bajdek
//

#ifndef SUBMENU_LOADING_HPP_
# define SUBMENU_LOADING_HPP_

#include "ASubmenu.hpp"

class Submenu_loading : public ASubmenu
{
private :
  Texte					*_Loading;
  int					_Percentage_int;
  Texte					*_Percentage_txt;
  bool					_dying;

public :
  Submenu_loading(Map *data, Menu *menu) : ASubmenu(data, menu, 3), _dying(false)
  {
    _Loading = data->create_texte("Generation de la map :", 55, glm::vec4(1, 0, 0, 1)); 
    _Percentage_int = 0;
    _Percentage_txt = data->create_texte(_Percentage_int, 55, 2, glm::vec4(1, 0, 0, 1)); 
  }

  virtual ~Submenu_loading()
  {
    delete _Loading;
    delete _Percentage_txt;
  }


  virtual void		set_activate(bool act) 
  {
    if (act == true && _activate == false)
      {
	_activate = act;
	_dying = false;
      }
    else if (act == false && _activate == true)
      {
	_activate = act;
	_dying = false;
      }
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	
      }
    if (_dying == true)
      {
	this->set_activate(false);
	return (false);
      }
    return (true);
  }

  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {  
	_backmenu->draw(shader, 305, 150);
	_Loading->draw(shader, 500, 500);
	_Percentage_txt->draw(shader, 500, 700);
      }
  }
  };

#endif /* */
