//
// Submenu_mult_ch.hpp for submenu in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 25 16:56:57 2014 corentin bajdek
// Last update Sat Jun 14 00:30:51 2014 corentin bajdek
//

#ifndef SUBMENU_MULT_CH_HPP_
# define SUBMENU_MULT_CH_HPP_

#include "ASubmenu.hpp"

class Submenu_mult_ch : public ASubmenu
{
private :
  Title				*_Random_map;
  Title				*_Existing_map;
  Title				*_Load_save;
  Title				*_Return;

public :
  Submenu_mult_ch(Map *data, Menu *menu) : ASubmenu(data, menu, 4)
  {
    _Random_map  = new Title(_data, "Map aleatoire", 50);
    _Existing_map  = new Title(_data, "Choisir une map", 50);
    _Load_save  = new Title(_data, "Charger", 50);
    _Return =  new Title(_data, "Retour", 50);

    this->set_backmenu(550, 4 * 140);

    _box.push_back(_Random_map);
    _box.push_back(_Existing_map);
    _box.push_back(_Load_save);
    _box.push_back(_Return);
  }
  virtual ~Submenu_mult_ch()
  {
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	/* récupere les inputs de touches */
	_last_input = get_input(input);
	if (_last_input == UP)
	  _current--;
	else if (_last_input == DOWN)
	  _current++;
	if (_last_input == ENTER)
	  {
	    if (_Return->get_selected() == true)
	      {
		_menu->set_activate(MULT_CH, false);
		_menu->set_activate(GLOBAL, true);
	      }
	    else if (_Random_map->get_selected() == true)
	      {
		_menu->set_activate(MULT_CH, false);
		_menu->set_activate(MULTIJOUEUR, true);
	      }
	    else if (_Existing_map->get_selected() == true)
	      {
		_menu->set_activate(MULT_CH, false);
		_menu->set_activate(MULT_CHMAP, true);
	      }
	    else
	      {
		_menu->set_activate(MULT_CH, false);
		_menu->set_activate(MULT_LOAD, true);
	      }
	  }
	_last_input = NONE;
	if (_current == 0)
	  _current = _box.size();
	if (_current == (int)_box.size() + 1)
	  _current = 1;
	if (_current != oldcurrent)
	  _box[oldcurrent - 1]->set_selected(false);
	_box[_current - 1]->set_selected(true);
      }
    return (1);
  }

  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      _backmenu->draw(shader, 0, 250);
    for (size_t i = 0; i < _box.size(); ++i)
      _box[i]->draw(shader, 280 + (i * 150), 200);
  }

};

#endif /* */
