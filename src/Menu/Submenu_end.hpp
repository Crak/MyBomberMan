//
// Submenu_pause.hpp for submenu in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 25 16:56:57 2014 corentin bajdek
// Last update Fri Jun 13 18:57:06 2014 corentin bajdek
//

#ifndef SUBMENU_END_HPP_
# define SUBMENU_END_HPP_

#include "Submenu_pause.hpp"
#include "ASubmenu.hpp"

class Submenu_end : public ASubmenu
{
private :
  Title					*_Restart;
  Title					*_Quit;
  Texte					*_Winner;
  int					_ret;

public :
  Submenu_end(Map *data, Menu *menu, const std::string &winner)
    : ASubmenu(data, menu, 3), _ret(-1)
  {
    _Restart =  new Title(_data, "Recommencer", 50);
    _Quit =  new Title(_data, "Quitter", 50);
    
    _Winner = _data->create_texte(winner, 60, glm::vec4(1.0f, 0.0f, 0.0f, 1));
    _Winner->initialize();
    this->set_backmenu(620, 4 * 140 - 40);

    _box.push_back(_Restart);
    _box.push_back(_Quit);
  }

  virtual ~Submenu_end() {}

  int		handle_activate()
  {
    if (_Restart->get_selected() == true)
      {
	_data->get_mod().setIntMod("Action", RESTART);
	_ret = QUITTER;
      }
    else if (_Quit->get_selected() == true)
      _ret = QUITTER;
    return (1);
  }

  void		handle_selection(int oldcurrent)
  {
    if (_current != oldcurrent)
      _box[oldcurrent - 1]->set_selected(false);
    _box[_current - 1]->set_selected(true);
  }

  void		handle_input(gdl::Input &input)
  {
    _last_input = get_input(input);
    if (_last_input == UP)
      {
	_current--;
	if (_current == 0)
	  _current = _box.size();
      }
    else if (_last_input == DOWN)
      {
	_current++;
	if (_current == (int)_box.size() + 1)
	  _current = 1;
      }
    if (_last_input == ENTER)
      this->handle_activate();
    _last_input = NONE;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	handle_input(input);
	this->handle_selection(oldcurrent);
      }
    return (_ret);
  }

  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      _backmenu->draw(shader, 200, 150);
    _Winner->draw(shader, 220, 200);
    for (size_t i = 0; i < _box.size(); ++i)
      _box[i]->draw(shader, 230 + ((i + 1) * 140), 425);
  }
  };

#endif /* */
