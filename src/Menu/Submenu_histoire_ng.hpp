//
// Submenu_histoire_ng.hpp for submenu in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 25 16:56:57 2014 corentin bajdek
// Last update Sun Jun 15 21:22:00 2014 theo richard
//

#ifndef SUBMENU_HISTOIRE_NG_HPP_
# define SUBMENU_HISTOIRE_NG_HPP_

#include "ASubmenu.hpp"
#include "../Save.hpp"
#include "../Mod.hpp"
#include "../GameEngine/GameEngine.hpp"

class Submenu_histoire_ng : public ASubmenu
{
private :
  std::vector<std::string>		_maps;
  /* Box de tous les bra */
  std::vector<Bra_title *>		_Box_bra;
  /* Nombre de players */
  std::vector<Title *>			_Nb_player_choices;
  Bra_title				*_Nb_player_bra;
  /* Difficulté */
  std::vector<Title *>			_Difficulty_choices;
  Bra_title				*_Difficulty_bra;
  Title					*_Nbplayer;
  Title					*_Niveau1;
  Title					*_Niveau2;
  Title					*_Niveau3;
  Title					*_Niveau4;
  Title					*_Niveau5;
  Title					*_Difficulty;
  Title					*_Return;


public :
  Submenu_histoire_ng(Map *data, Menu *menu) : ASubmenu(data, menu, 7)
  {
    _Nbplayer =  new Title(_data, "Nombre de joueurs :", 50);
    _Niveau1 =  new Title(_data, "Niveau 1", 50);
    _Niveau2 =  new Title(_data, "Niveau 2", 50);
    _Niveau3 =  new Title(_data, "Niveau 3", 50);
    _Niveau4 =  new Title(_data, "Niveau 4", 50);
    _Niveau5 =  new Title(_data, "Niveau 5", 50);

    _Difficulty =  new Title(_data, "Difficulte :", 40);

    _Return =  new Title(_data, "Retour", 50);

    this->set_backmenu(900, 7 * 140);

    _box.push_back(_Nbplayer);
    _box.push_back(_Niveau1);
    _box.push_back(_Niveau2);
    _box.push_back(_Niveau3);
    _box.push_back(_Niveau4);
    _box.push_back(_Niveau5);
    _box.push_back(_Return);

    /* ici on rempli tous les choix possibles*/
    /* Différents nbplayer */
    _Nb_player_choices.push_back(new Title(_data, "1 Player", 50));
    _Nb_player_choices.push_back(new Title(_data, "2 Players", 50));
    _Nb_player_bra = new Bra_title(_data, _Nb_player_choices, 50);
    _Box_bra.push_back(_Nb_player_bra);

    /* Différents nbplayer */
    _Difficulty_choices.push_back(new Title(_data, "facile", 40));
    _Difficulty_choices.push_back(new Title(_data, "moyen", 40));
    _Difficulty_choices.push_back(new Title(_data, "Difficile", 40));
    _Difficulty_bra = new Bra_title(_data, _Difficulty_choices, 40);
    _Box_bra.push_back(_Difficulty_bra);
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      _Box_bra[i]->set_visible(true);

    _maps.push_back("Map_1.bbm");
    _maps.push_back("Map_2.bbm");
    _maps.push_back("Map_3.bbm");
    _maps.push_back("Map_4.bbm");
    _maps.push_back("Map_5.bbm");
  }

  virtual ~Submenu_histoire_ng()
  {
    delete _Difficulty;
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      delete _Box_bra[i];
  }

  virtual void	new_game(const std::string &map)
  {
    Save	save("./save/", _menu->get_context());
    GameEngine	*engine;

    _data->get_mod().setIntMod("Difficulty", _Difficulty_bra->get_current());
    if ((save.load_map(map, _menu->get_data()->get_mod(), _menu->get_data())) == false)
      {
	//	return (EXIT_FAILURE);
	std::cerr << "that's bad (map)" << std::endl;
      }
    engine = _data->create_gameengine(_data->get_mod(), _menu->get_context());
    
    if (engine->initialize() == false)
      {
	std::cerr << "that's bad (gameengine)" << std::endl;
	//	return (EXIT_FAILURE);
      }
    while (engine->update() == true)
      engine->draw();
    delete engine;
    if (_data->get_mod().getIntMod("Action") == RESTART)
      {
	_data->get_mod().setIntMod("Action", NONE);
	if (_data->get_mod().getIntMod("HumanPlayers") == 1)
	  new_game(_maps[_current - 2]);
	else
	  new_game("Coop_" + _maps[_current - 2]);
      }
    else if (_data->get_mod().getIntMod("Action") == NEXTLVL)
      {
	_data->get_mod().setIntMod("Action", NONE);
	_current++;
	if ((unsigned int)_current == _box.size())
	  _current--;
	if (_data->get_mod().getIntMod("HumanPlayers") == 1)
	  new_game(_maps[_current - 2]);
	else
	  new_game("Coop_" + _maps[_current - 2]);
      }
  }

  int		handle_activate()
  {
    if (_Return->get_selected() == true)
      {
	_menu->set_activate(HISTOIRE_NG, false);
	_menu->set_activate(HISTOIRE, true);
      }
    else if (_Nbplayer->get_selected() == false)
      {
	_menu->get_data()->get_mod().setIntMod("HumanPlayers", _Nb_player_bra->get_current() + 1);
	_menu->get_data()->get_mod().setIntMod("GameMode", MODE_HISTOIRE);
	if (_menu->get_data()->get_mod().getIntMod("HumanPlayers") == 1)
	  new_game(_maps[_current - 2]);
	else
	  new_game("Coop_" + _maps[_current - 2]);
      }
    return (1);
  }

  void		handle_selection(int oldcurrent)
  {
    if (_current != oldcurrent)
      _box[oldcurrent - 1]->set_selected(false);
    _box[_current - 1]->set_selected(true);
    _Difficulty->set_visible(_Return->get_visible());
    if (_Return->get_selected() == false && _Nbplayer->get_selected() == false)
      _Difficulty_bra->set_selected(true);
    _Nb_player_bra->set_selected(_Nbplayer->get_selected());
  }

  void		handle_input(gdl::Input &input)
  {
    _last_input = get_input(input);
    if (_last_input == UP)
      {
	_current--;
	if (_current == 0)
	  _current = _box.size();
      }
    else if (_last_input == DOWN)
      {
	_current++;
	if (_current == (int)_box.size() + 1)
	  _current = 1;
      }
    else if (_last_input == RIGHT)
      {
	for (size_t i = 0; i < _Box_bra.size(); ++i)
	  if (_Box_bra[i]->get_selected() == true)
	    _Box_bra[i]->set_current(_Box_bra[i]->get_current() + 1);
      }
    else if (_last_input == LEFT)
      {
	for (size_t i = 0; i < _Box_bra.size(); ++i)
	  if (_Box_bra[i]->get_selected() == true)
	    _Box_bra[i]->set_current(_Box_bra[i]->get_current() - 1);
      }
    if (_last_input == ENTER)
      this->handle_activate();
    _last_input = NONE;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	handle_input(input);
	this->handle_selection(oldcurrent);
      }
    return (1);
  }
  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {
	_backmenu->draw(shader, 0, 130);
	_Nb_player_bra->draw(shader, 180, 525);
      }
    _Nbplayer->draw(shader, 180, 160, 50);
    _Niveau1->draw(shader, 325 + 100 * 0, 160, 50);
    _Niveau2->draw(shader, 325 + 100 * 1, 160, 50);
    _Niveau3->draw(shader, 325 + 100 * 2, 160, 50);
    _Niveau4->draw(shader, 325 + 100 * 3, 160, 50);
    _Niveau5->draw(shader, 325 + 100 * 4, 160, 50);

    if (_Return->get_selected() == false && _Nbplayer->get_selected() == false)
      for (size_t i = 0; i < _box.size(); ++i)
	if (_box[i]->get_selected() == true)
	  {
	    _Difficulty->draw(shader, 330 + 100 * (i - 1), 400); 
	    _Difficulty_bra->draw(shader, 330 + 100 * (i - 1), 550); 
	  }
    _Return->draw(shader, 280 + 125 * 5, 160, 50);
  }
  };

#endif /* */
