//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Sun Jun 15 22:40:48 2014 corentin bajdek
//

#ifndef SUBMENU_MULT_HPP_
# define SUBMENU_MULT_HPP_

#include "ASubmenu.hpp"
#include "../Save.hpp"
#include "../Mod.hpp"
#include "../GameEngine/GameEngine.hpp"

#define NB_IA_MAX		150
#define TAILLE_MAP_MAX		150

enum
  {
    CLASSIC,
    TEMPS,
    VIES,
  };

class Submenu_mult : public ASubmenu
{
private :
  Title				*_Launch_game;
  Title				*_Nb_players;
  Title				*_Longtaille_map;
  Title				*_Largtaille_map;
  Title				*_Nb_ia;
  Title				*_Bonus;
  Title				*_Mode;
  Title				*_Return;
  Title				*_Temps;
  Title				*_Mn;
  Title				*_Vies;
  /* Box de tous les bra */
  std::vector<Bra_title *>	_Box_bra;
  /* Nombre de vies */
  std::vector<Title *>		_Vies_choices;
  Bra_title			*_Vies_bra;
  /* Temps en minutes */
  std::vector<Title *>		_Minutes_choices;
  Bra_title			*_Minutes_bra;
  /* largeur Map */
  std::vector<Title *>		_Largtaille_choices;
  Bra_title			*_Largtaille_bra;
  /* longueur Map */
  std::vector<Title *>		_Longtaille_choices;
  Bra_title			*_Longtaille_bra;
  /* Modes */
  std::vector<Title *>		_Modes_choices;
  Bra_title			*_Modes_bra;
  /* Nbplayers */
  std::vector<Title *>		_Nb_players_choices;
  Bra_title			*_Nb_players_bra;
  /* Nbia */
  std::vector<Title *>		_Nb_ia_choices;
  Bra_title			*_Nb_ia_bra;
  /* Fréquence bonus*/
  std::vector<Title *>		_Bonus_choices;
  Bra_title			*_Bonus_bra;
public :
  Submenu_mult(Map *data, Menu *menu) : ASubmenu(data, menu, 5)
  {
    _Mode = new Title(_data, "Mode :", 50);
    _Nb_players = new Title(_data, "Nombre Players :", 50);
    _Nb_ia = new Title(_data, "Nombre d'IA :", 50);
    _Bonus = new Title(_data, "Frequence bonus :", 50);
    _Launch_game  = new Title(_data, "Lancer la partie", 50);
    _Return =  new Title(_data, "Retour", 50);
    _Longtaille_map =  new Title(_data, "Longueur de la map :", 50);
    _Largtaille_map =  new Title(_data, "Largeur de la map :", 50);
    _Temps = new Title(_data, "Temps :", 40);
    _Vies = new Title(_data, "Nombre de vies :", 40);
    _Mn = new Title(_data, "minutes", 40);

    this->set_backmenu(900, 8 * 140);

    _box.push_back(_Mode);
    _box.push_back(_Temps);
    _box.push_back(_Vies);
    _box.push_back(_Longtaille_map);
    _box.push_back(_Largtaille_map);
    _box.push_back(_Nb_players);
    _box.push_back(_Nb_ia);
    _box.push_back(_Bonus);
    _box.push_back(_Launch_game);
    _box.push_back(_Return);

    /* ici on rempli tous les choix possibles*/
    /* Différents modes */
    _Modes_choices.push_back(new Title(_data, "Classic", 50));
    _Modes_choices.push_back(new Title(_data, "Temps", 50));
    _Modes_choices.push_back(new Title(_data, "Vies", 50));
    _Modes_bra = new Bra_title(_data, _Modes_choices, 50);
    _Box_bra.push_back(_Modes_bra);
    /* Différents Temps */
    for (int i = 1; i <= 5; i++)
      _Minutes_choices.push_back(new Title(_data, i, 40));
    _Minutes_bra =  new Bra_title(_data, _Minutes_choices, 40);
    _Box_bra.push_back(_Minutes_bra);
    /* Différentes vies */
    for (int i = 1; i <= 5; i++)
      _Vies_choices.push_back(new Title(_data, i, 40));
    _Vies_bra =  new Bra_title(_data, _Vies_choices, 40);
    _Box_bra.push_back(_Vies_bra);
    /* Différentes taille de map */
    for (size_t i = 5; i <= TAILLE_MAP_MAX; i++)
      {
	_Longtaille_choices.push_back(new Title(_data, i, 50)); 
	_Largtaille_choices.push_back(new Title(_data, i, 50)); 
      }
    _Longtaille_bra = new Bra_title(_data, _Longtaille_choices, 50);
    _Largtaille_bra = new Bra_title(_data, _Largtaille_choices, 50);
    _Box_bra.push_back(_Longtaille_bra);
    _Box_bra.push_back(_Largtaille_bra);
    /* Différents Nb players*/
    for (int i = 1; i <= 2; i++)
      _Nb_players_choices.push_back(new Title(_data, i, 50));      
    _Nb_players_bra = new Bra_title(_data, _Nb_players_choices, 50);
    _Box_bra.push_back(_Nb_players_bra);
    /* Différents Nb d'ia*/
    for (size_t i = 0; i <= NB_IA_MAX; i++)
      _Nb_ia_choices.push_back(new Title(_data, i, 50));      
    _Nb_ia_bra = new Bra_title(_data, _Nb_ia_choices, 50);
    _Box_bra.push_back(_Nb_ia_bra);
    /* Différents bonus */
    _Bonus_choices.push_back(new Title(_data, "Peu", 50));
    _Bonus_choices.push_back(new Title(_data, "Moyen", 50));
    _Bonus_choices.push_back(new Title(_data, "Nombreux", 50));
    _Bonus_bra = new Bra_title(_data, _Bonus_choices, 50);
    _Box_bra.push_back(_Bonus_bra);
    _Bonus_bra->set_current(MOYEN);
    _Bonus_bra->set_selected(true);
    _Bonus_bra->set_selected(false);
    /* regle de visibilité */
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      _Box_bra[i]->set_visible(true);
  }

  virtual ~Submenu_mult()
  {
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      delete _Box_bra[i];
  }

  void		new_game()
  {
    Save	save("./save/", _menu->get_context());
    GameEngine	*engine;

    set_mode();
    
    if (save.generate_map(_data->get_mod(), _data, _Largtaille_bra->get_current() + 5, _Longtaille_bra->get_current() + 5) == false)
      {
	std::cout << "impossible de générer une telle map" << std::endl;
	return ;
      }
    
    engine = _data->create_gameengine(_data->get_mod(), _menu->get_context());

    if (engine->initialize() == false)
      {
	std::cerr << "that's bad" << std::endl;
	return ;
      }
    while (engine->update() == true)
      engine->draw();
    delete engine;
    if (_data->get_mod().getIntMod("Action") == RESTART)
      {
	_data->get_mod().setIntMod("Action", NONE);
	this->new_game();
      }
  }
  
  void		set_mode()
  {
    _menu->get_data()->get_mod().setIntMod("HumanPlayers", _Nb_players_bra->get_current() + 1);
    _menu->get_data()->get_mod().setIntMod("GameMode", _Modes_bra->get_current());
    if (_Modes_bra->get_current() == MODE_VIE)
      _menu->get_data()->get_mod().setIntMod("NbLife", _Vies_bra->get_current() + 1);      
    if (_Modes_bra->get_current() == MODE_TEMPS)
      _menu->get_data()->get_mod().setIntMod("NbTime", _Minutes_bra->get_current() + 1);      
    _menu->get_data()->get_mod().setIntMod("NbBonus", _Bonus_bra->get_current());
    _menu->get_data()->get_mod().setIntMod("NbIa", _Nb_ia_bra->get_current());
    _data->get_mod().setIntMod("Difficulty", NOMBREUX);
  }

  int		handle_activate()
  {
    if (_Return->get_selected() == true)
      {
	_menu->set_activate(MULTIJOUEUR, false);
	_menu->set_activate(MULT_CH, true);
      }
    else if (_Launch_game->get_selected() == true)
      new_game();
    return (1);
  }

  void		handle_selection(int oldcurrent)
  {
    if (_current != oldcurrent)
      _box[oldcurrent - 1]->set_selected(false);
    _box[_current - 1]->set_selected(true);
    _Modes_bra->set_selected(_Mode->get_selected());
    _Bonus_bra->set_selected(_Bonus->get_selected());
    _Nb_players_bra->set_selected(_Nb_players->get_selected());
    _Nb_ia_bra->set_selected(_Nb_ia->get_selected());
    _Longtaille_bra->set_selected(_Longtaille_map->get_selected());
    _Largtaille_bra->set_selected(_Largtaille_map->get_selected());
    _Minutes_bra->set_selected(_Temps->get_selected());
    _Mn->set_selected(_Temps->get_selected());
    _Vies_bra->set_selected(_Vies->get_selected());
    /* On mets tout a false de base */
    _Temps->set_visible(false);
    _Minutes_bra->set_visible(false);
    _Mn->set_visible(false);
    _Vies->set_visible(false);
    _Vies_bra->set_visible(false);
    if (_Modes_bra->get_current() == TEMPS)
      {
	_Temps->set_visible(true);
	_Minutes_bra->set_visible(true);
	_Mn->set_visible(true);
      }
    else if (_Modes_bra->get_current() == VIES)
      {
	_Vies->set_visible(true);
	_Vies_bra->set_visible(true);
      }
  }

  void		handle_input(gdl::Input &input)
  {
    _last_input = get_input(input);
    if (_last_input == UP)
      do
	{
	  _current--;
	  if (_current == 0)
	    _current = _box.size();
	}
      while (_box[_current - 1]->get_visible() == false);
    else if (_last_input == DOWN)
      do
	{
	  _current++;
	  if (_current == (int)_box.size() + 1)
	    _current = 1;
	}
      while (_box[_current - 1]->get_visible() == false);
    else if (_last_input == RIGHT)
      {
	for (size_t i = 0; i < _Box_bra.size(); ++i)
	  if (_box[i]->get_selected() == true)
	    _Box_bra[i]->set_current(_Box_bra[i]->get_current() + 1);
      }
    else if (_last_input == LEFT)
      {
	for (size_t i = 0; i < _Box_bra.size(); ++i)
	  if (_box[i]->get_selected() == true)
	    _Box_bra[i]->set_current(_Box_bra[i]->get_current() - 1);
      }
    if (_last_input == ENTER)
      this->handle_activate();
    _last_input = NONE;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	/* récupere les inputs de touches */
	handle_input(input);
	this->handle_selection(oldcurrent);
      }
    return (1);
  }

  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {
	_backmenu->draw(shader, 0, 0);
	_Modes_bra->draw(shader, 40, 200);
	_Minutes_bra->draw(shader, 120, 220);
	_Vies_bra->draw(shader, 120, 400);
	_Nb_players_bra->draw(shader, 80 + 125 * 3, 450);
	_Nb_ia_bra->draw(shader, 80 + 125 * 4, 380);
	_Bonus_bra->draw(shader, 80 + 125 * 5, 480);
	_Longtaille_bra->draw(shader, 80 + 125 * 1, 550);
	_Largtaille_bra->draw(shader, 80 + 125 * 2, 520);
      }
    _Mode->draw(shader, 40, 160, 50);
    _Temps->draw(shader, 120, 160, 80);
    _Mn->draw(shader, 120, 160, 300);
    _Vies->draw(shader, 120, 160, 80);
    _Longtaille_map->draw(shader, 80 + 125 * 1, 160, 50);
    _Largtaille_map->draw(shader, 80 + 125 * 2, 160, 50);
    _Nb_players->draw(shader, 80 + 125 * 3, 160, 50);
    _Nb_ia->draw(shader, 80 + 125 * 4, 160, 50);
    _Bonus->draw(shader, 80 + 125 * 5, 160, 50);
    _Launch_game->draw(shader, 80 + 125 * 6, 160, 50);
    _Return->draw(shader, 80 + 125 * 7 - 25, 160, 50);
  }
};

#endif /* !SUBMENU_MULT_HPP_ */
