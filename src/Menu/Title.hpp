//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Sun May 25 15:52:17 2014 corentin bajdek
//

#ifndef TITLE_HPP_
# define TITLE_HPP_

#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "../Map.hpp"
#include "Texte.hpp"

class Title
{
protected :
  Map		*_data;
  Texte		*_title;
  float		_size;
  std::string	_name;
  bool		_selected;
  bool		_visible;
  int		_name_int;

public :
  Title(Map *data, std::string name, float size)
    : _data(data), _size(size), _name(name), _selected(false), _visible(false), _name_int(-42)
  {
    _title = data->create_texte(name, size, glm::vec4(64.0f / 255.0f, 64.0f / 255.0f, 64.0f / 255.0f, 1));
    _title->initialize();
  }
  Title(Map *data, int name, float size)
    : _data(data), _size(size), _selected(false), _visible(false), _name_int(name)
  {
    _title = data->create_texte(name, size, 0, glm::vec4(64.0f / 255.0f, 64.0f / 255.0f, 64.0f / 255.0f, 1));
    _title->initialize();
  }
  virtual ~Title() { delete _title ;}

  int		update(gdl::Clock const &clock, gdl::Input &input)
  {
    (void)clock;
    (void)input;
    return (0);
  }

  virtual void		draw(gdl::AShader &shader, double pos_y, double mid_pos_x, double pos_x = -42)
  {
    if (_visible == true)
      {
	if (pos_x == -42)
	  pos_x = (int)((float)mid_pos_x - ((float)_name.size() / 2.0f - 1.0f) * ((float)_size / 2.0f));
	_title->draw(shader, pos_x, pos_y);
      }
  }

  void		set_visible(bool act) {_visible = act;}
  bool		get_visible(void) { return _visible ;}
  void		set_selected(bool act)
  {

    if (act == true && _selected == false)
      {
	_selected = act;
	delete _title;
	if (_name_int == -42)
	  _title = _data->create_texte(_name, _size, glm::vec4(1, 153.0f / 255.0f, 51.0f / 255.0f, 1));
	else
	  _title = _data->create_texte(_name_int, _size, 0, glm::vec4(1, 153.0f / 255.0f, 51.0f / 255.0f, 1));
	_title->initialize();
      }
    if (act == false && _selected == true)
      {
	_selected = act;
	delete _title;
	if (_name_int == -42)
	  _title = _data->create_texte(_name, _size, glm::vec4(64.0f / 255.0f, 64.0f / 255.0f, 64.0f / 255.0f, 1));
	else
	  _title = _data->create_texte(_name_int, _size, 0, glm::vec4(64.0f / 255.0f, 64.0f / 255.0f, 64.0f / 255.0f, 1));
	_title->initialize();
      }
  }

  bool		get_selected(void) { return _selected ;}
  int		get_strlen(void)
  {
    if (_name_int == -42)
      return _name.size() ;
    else if (_name_int > 0 && _name_int < 10)
      return (1);
    else if (_name_int >= 10 && _name_int < 100)
      return (2);
    else if (_name_int >= 100)
      return (3);
    return (1);
  }
};

#endif /* !TITLE_HPP_ */
