//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Sun Jun 15 22:39:50 2014 corentin bajdek
//

#ifndef SUBMENU_OK_HPP_
# define SUBMENU_OK_HPP_

#include "ASubmenu.hpp" 

class Submenu_ok : public ASubmenu
{
private :
  Title			*_Saved;
  Title			*_Ok;
  double		_invinc;

public :
  Submenu_ok(Map *data, Menu *menu) : ASubmenu(data, menu, 3), _invinc(0.00)
  {
    _Saved = new Title(_data, "Partie sauvegardée !", 50);
    _Ok = new Title(_data, "Ok", 50);

    _box.push_back(_Ok);
  }
  virtual ~Submenu_ok()
  {
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	/* récupere les inputs de touches */
	_last_input = get_input(input);
	if (_last_input == UP)
	  _current--;
	else if (_last_input == DOWN)
	  _current++;
	if (_current == 0)
	  _current = _box.size();
	if (_current == (int)_box.size() + 1)
	  _current = 1;
	if (_last_input == ENTER)
	  return (false);
	_last_input = NONE;
	if (_current != oldcurrent)
	  _box[oldcurrent - 1]->set_selected(false);
	_box[_current - 1]->set_selected(true);
      }
    return (true);
  }
  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {
	_backmenu->draw(shader, 305, 150);
	_Saved->draw(shader, 300, 160, 300);
      }
    _Ok->draw(shader, 300, 160, 400);
  }

  virtual void		set_activate(bool act) 
  {
    if (act == true && _activate == false)
      {
	_activate = act;
	_box[0]->set_selected(true);
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->set_visible(true);
      }
    else if (act == false && _activate == true)
      {
	_activate = act;
	_current = 1;
	for (size_t i = 0; i < _box.size(); ++i)
	  {
	    _box[i]->set_visible(false);
	    _box[i]->set_selected(false);
	  }
      }
  }
};

#endif /* */
