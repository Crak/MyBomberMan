//
// Submenu_pause.hpp for submenu in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 25 16:56:57 2014 corentin bajdek
// Last update Sun Jun 15 18:56:32 2014 corentin bajdek
//

#ifndef SUBMENU_PAUSE_HPP_
# define SUBMENU_PAUSE_HPP_

#include <dirent.h> // opendir
#include <sys/types.h>
#include <time.h>  // time
#include "ASubmenu.hpp"
#include "Submenu_ok.hpp"

enum
  {
    NOTHING,
    RETOUR,
    QUITTER,
  };

class Submenu_pause : public ASubmenu
{
private :
  Title					*_Restart;
  Title					*_Save;
  Title					*_Quit;
  Title					*_Return;
  int					_ret;
  bool					_dying;

public :
  Submenu_pause(Map *data, Menu *menu) : ASubmenu(data, menu, 3), _ret(-1), _dying(false)
  {
    _Restart =  new Title(_data, "Recommencer", 50);
    _Save =  new Title(_data, "Sauvergarder", 50);
    _Quit =  new Title(_data, "Quitter", 50);
    _Return =  new Title(_data, "Retour", 50);

    this->set_backmenu(405, 4 * 140);

    _box.push_back(_Restart);
    _box.push_back(_Save);
    _box.push_back(_Quit);
    _box.push_back(_Return);
  }

  virtual ~Submenu_pause()
  {

  }

  int		handle_activate()
  {
    if (_Return->get_selected() == true)
      _ret = RETOUR;
    else if (_Quit->get_selected() == true)
      _ret = QUITTER;
    else if (_Restart->get_selected() == true)
      {
	_ret = QUITTER;
	_data->get_mod().setIntMod("Action", RESTART);
      }
    else if (_Save->get_selected() == true)
      {
	this->save();
      }
    return (1);
  }


  void		save()
  {
    time_t		rawtime;
    struct tm		*timeinfo;
    std::string		target("./save/");
    
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    if (_data->get_mod().getIntMod("GameMode") == MODE_HISTOIRE)
      target.append("histoire_load/");
    else
      target.append("multijoueur_load/");

    target.append(asctime(timeinfo));
    target.erase(target.find("2014"));

    std::ofstream		ofs(target.c_str());
    
    if (ofs.is_open())
      {
	_data->get_mod().save(ofs);
	for (size_t i = 0; i < _data->Objects.size(); ++i)
	  {
	    if (_data->Objects[i]->get_type() == BONHOMME ||
		_data->Objects[i]->get_type() == BONUS ||
		_data->Objects[i]->get_type() == WALL ||
		_data->Objects[i]->get_type() == BOMBE ||
		_data->Objects[i]->get_type() == FIRE)
	      {
		_data->Objects[i]->save(ofs);
		ofs << "\n";
	      }
	  }
	ofs.close();
      }
    else
      std::cout << "unable to save" << std::endl;
    _dying = true;
  }

    void		handle_selection(int oldcurrent)
    {
      if (_current != oldcurrent)
	_box[oldcurrent - 1]->set_selected(false);
      _box[_current - 1]->set_selected(true);
    }

  void		handle_input(gdl::Input &input)
  {
    _last_input = get_input(input);
    if (_last_input == UP)
      {
	_current--;
	if (_current == 0)
	  _current = _box.size();
      }
    else if (_last_input == DOWN)
      {
	_current++;
	if (_current == (int)_box.size() + 1)
	  _current = 1;
      }
    if (_last_input == ENTER)
      this->handle_activate();
    if (_last_input == ESCAPE)
      _dying = true;
    _last_input = NONE;
  }

  virtual void		set_activate(bool act) 
  {
    if (act == true && _activate == false)
      {
	_activate = act;
	_ret = -1;
	_dying = false;
	_box[0]->set_selected(true);
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->set_visible(true);
      }
    else if (act == false && _activate == true)
      {
	_activate = act;
	_ret = -1;
	_dying = false;
	_current = 1;
	for (size_t i = 0; i < _box.size(); ++i)
	  {
	    _box[i]->set_visible(false);
	    _box[i]->set_selected(false);
	  }
      }
  
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	handle_input(input);
	this->handle_selection(oldcurrent);
      }
    if (_dying == true)
      _ret = RETOUR;
    return (_ret);
  }

  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {
	_backmenu->draw(shader, 305, 150);
      }
    for (size_t i = 0; i < _box.size(); ++i)
      _box[i]->draw(shader, 180 + i * 140, 425);
  }
  };

#endif /* */
