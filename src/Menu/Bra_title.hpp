//
// Bra_Title.hpp for bra in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May 23 14:09:29 2014 corentin bajdek
// Last update Fri Jun 13 20:02:54 2014 corentin bajdek
//

#ifndef BRA_TITLE_HPP
# define BRA_TITLE_HPP

#include "Title.hpp"

class Bra_title
{
private :
  Map				*_data;
  float				_size;
  Title				*_rbra;
  Title				*_lbra;
  std::vector<Title *>		&_choices;
  bool				_selected;
  bool				_visible;
  int				_current;

public : 
  Bra_title(Map *data, std::vector<Title *> &choices, float size)
    : _data(data), _size(size), _choices(choices),  _selected(false), _visible(false), _current(0)
  {
    _rbra = new Title(_data, ">", size);
    _lbra = new Title(_data, "<", size);
    for (size_t i = 0; i < _choices.size(); ++i)
      _choices[i]->set_visible(true);
    _lbra->set_visible(true);
    _rbra->set_visible(true);
  }
  virtual ~Bra_title() 
  {
    delete _rbra;
    delete _lbra;
  }

  void		draw(gdl::AShader &shader, double pos_y, double pos_x)
  {
    if (_visible == true)
      {
	_lbra->draw(shader, pos_y, 0, pos_x);
	_choices[_current]->draw(shader, pos_y, 0, pos_x + 25);
	_rbra->draw(shader, pos_y, 0, pos_x + (_choices[_current]->get_strlen() * (_size / 2)) + (_size / 2 + 5));
      }

  }
  void		set_visible(bool act) {_visible = act;}
  bool		get_visible(void) { return _visible ;}
  void		set_selected(bool act)
  {
    if (act == true && _selected == false)
      {
	_rbra->set_selected(true);
	_choices[_current]->set_selected(true);
	_lbra->set_selected(true);
	_selected = act;
      }
    if (act == false && _selected == true)
      {
	_rbra->set_selected(false);
	_choices[_current]->set_selected(false);
	_lbra->set_selected(false);
	_selected = act;
      }
  }

  bool		get_selected(void) { return _selected ;}
  void		set_current(int nb)
  {
    int		oldcurrent;

    oldcurrent = _current;
    if (nb == (int)_choices.size())
      _current = 0;
    else if (nb == -1)
      _current = (int)_choices.size() - 1;
    else
      _current = nb;
    if (oldcurrent != _current)
      {
	_choices[oldcurrent]->set_selected(false);
	_choices[_current]->set_selected(true);
      }
  }
  int		get_current(void) { return _current ;}
};

#endif /* */
