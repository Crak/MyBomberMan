//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Sun Jun 15 19:41:26 2014 corentin bajdek
//

#ifndef SUBMENU_CREDITS_HPP_
# define SUBMENU_CREDITS_HPP_

#include "ASubmenu.hpp"

class Submenu_credits : public ASubmenu
{
private :
  Title				*_Merci;
  Title				*_Thomas;
  Title				*_Axel;
  Title				*_Return;

public :
  Submenu_credits(Map *data, Menu *menu) : ASubmenu(data, menu, 5)
  {
    _Merci  = new Title(_data, "Merci a :", 50);
    _Thomas = new Title(_data, "Thomas Vitello pour son soutien", 40);
    _Axel =  new Title(_data, "Axel Nieddu pour la musique", 40);
    _Return = new Title(_data, "Retour", 50);

    _Merci->set_visible(true);
    _Thomas->set_visible(true);
    _Axel->set_visible(true);
    this->set_backmenu(800, 140 * 5);

    _box.push_back(_Return);
  }
  virtual ~Submenu_credits()
  {
    delete _Merci;
    delete _Thomas;
    delete _Axel;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	/* récupere les inputs de touches */
	_last_input = get_input(input);
	if (_last_input == UP)
	  _current--;
	else if (_last_input == DOWN)
	  _current++;
	if (_last_input == ENTER)
	  if (_Return->get_selected() == true)
	    {
	      _menu->set_activate(AUTRES_CREDIT, false);
	      _menu->set_activate(GLOBAL, true);
	    }
	_last_input = NONE;
	if (_current == 0)
	  _current = _box.size();
	if (_current == (int)_box.size() + 1)
	  _current = 1;
	if (_current != oldcurrent)
	  _box[oldcurrent - 1]->set_selected(false);
	_box[_current - 1]->set_selected(true);
      }
    return (1);
  }

  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {
	_backmenu->draw(shader, 200, 250);
	_Merci->draw(shader, 280, 160, 250);
	_Axel->draw(shader, 280 + 2 * 150, 160, 250);
	_Thomas->draw(shader, 280 + 3 * 150, 160, 250);
      }
    _Return->draw(shader, 280 + (4 * 150), 160, 250);
  }

};

#endif /* */
