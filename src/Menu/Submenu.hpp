//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Sun Jun 15 23:03:49 2014 corentin bajdek
//

#ifndef SUBMENU_HPP_
# define SUBMENU_HPP_

#include "ASubmenu.hpp"
#include "../Save.hpp"
#include "../Mod.hpp"
#include "../GameEngine/GameEngine.hpp"
#include "../Intro.hpp"

class Submenu : public ASubmenu
{
private :
  Title				*_Rapid;
  Title				*_Histoire;
  Title				*_Multijoueur;
  Title				*_Options;
  Title				*_Autres;
  Intro				*_intro;
  bool				_started;

public :
  Submenu(Map *data, Menu *menu) : ASubmenu(data, menu, 5), _started(false)
  {
    _Rapid = new Title(_data, " Partie rapide", 60);
    _Histoire = new Title(_data, "Histoire", 60);
    _Multijoueur = new Title(_data, "Multijoueur", 60);
    _Options = new Title(_data, "Options", 60);
    _Autres = new Title(_data, "Autres", 60);
    
    _intro = new Intro(data);
    _intro->initialize();

    _box.push_back(_Rapid);
    _box.push_back(_Histoire);
    _box.push_back(_Multijoueur);
    _box.push_back(_Options);
    _box.push_back(_Autres);
  }

  virtual ~Submenu()
  {
    delete _intro;
  }

  void		set_mode()
  {
    _menu->get_data()->get_mod().setIntMod("HumanPlayers", 1);
    _menu->get_data()->get_mod().setIntMod("NbIa", 1);
    _menu->get_data()->get_mod().setIntMod("GameMode", MODE_CLASSIC);
  }

  virtual void	new_game()
  {
    Save	save("./save/", _menu->get_context());
    GameEngine	*engine;

    set_mode();
    if ((save.load_map("Default_Map.bbm", _menu->get_data()->get_mod(), _menu->get_data())) == false)
      {
	std::cerr << "that's bad" << std::endl;
	return ;
      }
    engine = _data->create_gameengine(_data->get_mod(), _menu->get_context());
    
    if (engine->initialize() == false)
      {
	std::cerr << "that's bad" << std::endl;
	return ;
      }
    while (engine->update() == true)
      engine->draw();
    delete engine;
    if (_data->get_mod().getIntMod("Action") == RESTART)
      {
	_data->get_mod().setIntMod("Action", NONE);
	this->new_game();
      }
  }
  
  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int	oldcurrent;
    
    if (_started == false)
      {
	if ((_intro->update(clock, input)) == true)
	  return (true);
	else
	  _started = true;
      }
    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	/* gestion des inputs de touche */
	_last_input = get_input(input);
	if (_last_input == UP)
	  _current--;
	else if (_last_input == DOWN)
	  _current++;
	if (_current == 0)
	  _current = _box.size();
	if (_current == (int)_box.size() + 1)
	  _current = 1;
	if (_last_input == ENTER)
	  {
	    if (_Rapid->get_selected() == false)
	      {
		_menu->set_activate(_current - 2, true);
		_menu->set_activate(GLOBAL, false);
	      }
	    else
	      new_game();
	  }
	_last_input = NONE;
	if (_current != oldcurrent)
	  _box[oldcurrent - 1]->set_selected(false);
	_box[_current - 1]->set_selected(true);
      }
    return (1);
  }

  virtual void		draw(gdl::AShader &shader)
  {
    if (_started == false)
      {
	_intro->draw(shader);
	return ;
      }
    if (_activate == true)
      _backmenu->draw(shader, 0, 250);
    for (size_t i = 0; i < _box.size(); ++i)
      _box[i]->draw(shader, 280 + (i * 150), 160);
  }
};

#endif /* !SUBMENU_HPP_ */
