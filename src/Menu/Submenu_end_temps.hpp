//
// Submenu_pause.hpp for submenu in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sun May 25 16:56:57 2014 corentin bajdek
// Last update Fri Jun 13 18:57:07 2014 corentin bajdek
//

#ifndef SUBMENU_END_TEMPS_HPP_
# define SUBMENU_END_TEMPS_HPP_

#include "Submenu_pause.hpp"
#include "ASubmenu.hpp"

class Submenu_end_temps : public ASubmenu
{
private :
  Title					*_Restart;
  Title					*_Quit;

  Texte					*_Winner;
  Texte					*_Kills_txt;
  Texte					*_Death_txt;
  Texte					*_Kills_nb;
  Texte					*_Death_nb;
  Texte					*_Parenthesis;
  int					_ret;

public :
  Submenu_end_temps(Map *data, Menu *menu, const std::string &winner, int bestkill, int bestdeath)
    : ASubmenu(data, menu, 3), _ret(-1)
  {
    _Restart =  new Title(_data, "Recommencer", 50);
    _Quit =  new Title(_data, "Quitter", 50);
    
    _Winner = _data->create_texte(winner, 60, glm::vec4(1.0f, 0.0f, 0.0f, 1));
    _Kills_txt = _data->create_texte("(Kills:", 40, glm::vec4(1.0f, 0.0f, 0.0f, 1));
    _Death_txt = _data->create_texte("Death:", 40, glm::vec4(1.0f, 0.0f, 0.0f, 1));
    _Parenthesis = _data->create_texte(")", 40, glm::vec4(1.0f, 0.0f, 0.0f, 1));
    _Kills_nb =  _data->create_texte(bestkill, 40, 2, glm::vec4(1.0f, 0.0f, 0.0f, 1));
    _Death_nb =  _data->create_texte(bestdeath, 40, 2, glm::vec4(1.0f, 0.0f, 0.0f, 1));

    _Parenthesis->initialize();
    _Winner->initialize();
    _Kills_txt->initialize();
    _Death_txt->initialize();
    _Kills_nb->initialize();
    _Death_nb->initialize();

    this->set_backmenu(620, 4 * 140);

    _box.push_back(_Restart);
    _box.push_back(_Quit);
  }

  virtual ~Submenu_end_temps() {}

  int		handle_activate()
  {
    if (_Restart->get_selected() == true)
      {
	_data->get_mod().setIntMod("Action", RESTART);
	_ret = QUITTER;
      }
    else if (_Quit->get_selected() == true)
      _ret = QUITTER;
    return (1);
  }

  void		handle_selection(int oldcurrent)
  {
    if (_current != oldcurrent)
      _box[oldcurrent - 1]->set_selected(false);
    _box[_current - 1]->set_selected(true);
  }

  void		handle_input(gdl::Input &input)
  {
    _last_input = get_input(input);
    if (_last_input == UP)
      {
	_current--;
	if (_current == 0)
	  _current = _box.size();
      }
    else if (_last_input == DOWN)
      {
	_current++;
	if (_current == (int)_box.size() + 1)
	  _current = 1;
      }
    if (_last_input == ENTER)
      this->handle_activate();
    _last_input = NONE;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	handle_input(input);
	this->handle_selection(oldcurrent);
      }
    return (_ret);
  }

  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      _backmenu->draw(shader, 200, 150);
    _Winner->draw(shader, 220, 200);

    _Kills_txt->draw(shader, 265, 300);
    _Kills_nb->draw(shader, 265 + 140, 300);
    _Death_txt->draw(shader, 490, 300);
    _Death_nb->draw(shader, 490 + 120, 300);
    _Parenthesis->draw(shader, 490 + 160, 300);

    for (size_t i = 0; i < _box.size(); ++i)
      _box[i]->draw(shader, 280 + ((i + 1) * 140), 425);
  }
  };

#endif /* */
