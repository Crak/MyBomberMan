//
// Menu.cpp for menu in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Tue May 20 21:23:32 2014 corentin bajdek
// Last update Sun Jun 15 22:55:06 2014 corentin bajdek
//

#include "../Background.hpp"
#include "Menu.hpp"
#include "Submenu.hpp"
#include "Submenu_histoire.hpp"
#include "Submenu_mult.hpp"
#include "Submenu_mult_ch.hpp"
#include "Submenu_options.hpp"
#include "Submenu_video.hpp"
#include "Submenu_histoire_ng.hpp"
#include "Submenu_sons.hpp"
#include "Submenu_autres.hpp"
#include "Submenu_mult_chmap.hpp"
#include "Submenu_histoire_load.hpp"
#include "Submenu_mult_load.hpp"
#include "Submenu_credits.hpp"

Menu::Menu(Mod &mod, Map *data, int width_screen, int length_screen)
  : _mod(mod), _data(data), _width_screen(width_screen), _length_screen(length_screen)
{
}

Menu::~Menu()
{
  for (size_t i = 0; i < _box.size(); ++i)
    delete _box[i];
}

int	Menu::initialize()
{
  if (!_context.start(_width_screen, _length_screen, "My bomberman!"))
    return false;
  
  glEnable(GL_DEPTH_TEST);

  if (!_shader.load("./shaders/basic.fp", GL_FRAGMENT_SHADER)
      || !_shader.load("./shaders/basic.vp", GL_VERTEX_SHADER)
      || !_shader.build())
    return false;

  if (_data->getTextContainer()->Load_everything() == false)
    {
      std::cerr << "Textures manquantes" << std::endl;
      return (false);
    }

  glm::mat4	projection = glm::ortho(0.0f, (float)_width_screen, (float)_length_screen, 0.0f, -1.0f, 1.0f);

  _shader.bind();
  _shader.setUniform("view", glm::mat4(1));
  _shader.setUniform("projection", projection);

  _Global = new Submenu(_data, this);

  _back = _data->create_background("./assets/Backmenu.tga");

  _box.push_back(new Submenu_histoire(_data, this));
  _box.push_back(new Submenu_mult_ch(_data, this));
  _box.push_back(new Submenu_options(_data, this));
  _box.push_back(new Submenu_autres(_data, this));
  _box.push_back(new Submenu_mult(_data, this));
  _box.push_back(new Submenu_videos(_data, this));
  _box.push_back(new Submenu_sons(_data, this));
  _box.push_back(new Submenu_histoire_ng(_data, this));
  _box.push_back(new Submenu_mult_chmap(_data, this));
  _box.push_back(new Submenu_histoire_load(_data, this));
  _box.push_back(new Submenu_mult_load(_data, this));
  _box.push_back(new Submenu_credits(_data, this));

  _box.push_back(_Global);
  if (_back->initialize() == false)
    return (false);
  for (size_t i = 0; i < _box.size(); ++i)
    _box[i]->set_activate(false);
  _box[_box.size() - 1]->set_activate(true);
  return (true);
}

int	Menu::update()
{

  if (_input.getKey(SDLK_ESCAPE) || _input.getInput(SDL_QUIT))
    return false;
  _context.updateClock(_clock);
  _context.updateInputs(_input);
  for (size_t i = 0; i < _box.size(); ++i)
    _box[i]->update(_clock, _input);
  return (true);
}

void	Menu::draw()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glViewport(0, 0, 1800, 1000);
  _back->draw(_shader);
  for (size_t i = 0; i < _box.size(); ++i)
    _box[i]->draw(_shader);
  _context.flush();
}

void	Menu::set_activate(int wich, bool act)
{
  if (wich == GLOBAL)
    _Global->set_activate(act);
  else
    _box[wich]->set_activate(act);
}
