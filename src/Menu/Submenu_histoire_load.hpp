//
// Submenu_histoire_load.hpp for 22 in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri Jun 13 19:38:04 2014 corentin bajdek
// Last update Sun Jun 15 15:50:01 2014 corentin bajdek
//

#ifndef SUBMENU_HISTOIRE_LOAD_HPP_
# define SUBMENU_HISTOIRE_LOAD_HPP_

#include <dirent.h> // opendir
#include <algorithm>
#include <sys/types.h>
#include "ASubmenu.hpp"
#include "../Save.hpp"
#include "../Mod.hpp"
#include "../GameEngine/GameEngine.hpp"

class Submenu_histoire_load : public ASubmenu
{
private :
  std::vector<std::string *>	_Maps;
  /* Box de tous les bra */
  std::vector<Bra_title *>	_Box_bra;
  /* Différentes map de players */
  std::vector<Title *>		_Map_choices;
  Bra_title			*_Map_bra;
  /* nb players */
  std::vector<Title *>		_Nb_players_choices;
  Bra_title			*_Nb_players_bra;

  Title				*_Map;
  Title				*_Launch_game;
  Title				*_Nb_players;
  Title				*_Return;

public :
  Submenu_histoire_load(Map *data, Menu *menu) : ASubmenu(data, menu, 7)
  {
    std::vector<std::string *>	result;
    dirent			*de;
    DIR				*dp;

    dp = opendir("./save/histoire_load");
    if (dp)
      {
	while ((de = readdir(dp)) != NULL)
	  result.push_back (new std::string(de->d_name));
	closedir(dp);
	std::sort(result.begin(), result.end());
      }
    for (size_t i =  0; i < result.size(); i++)
      {
	if ((result[i])->find_first_of('.', 2) != std::string::npos) 
	  {
	    _Maps.push_back(result[i]);
	    _Map_choices.push_back(new Title(_data, result[i]->c_str(), 50));
	  }
      }

    _Map = new Title(_data, "Choix du fichier :", 50);
    _Nb_players = new Title(_data, "Nombre Players :", 50);
    _Return =  new Title(_data, "Retour", 50);
    _Launch_game = new Title(_data, "Lancer la partie", 50);

    this->set_backmenu(900, 6 * 140);

    _box.push_back(_Map);
    _box.push_back(_Launch_game);
    _box.push_back(_Return);

    _Nb_players->set_visible(true);
    /* Différents Nb players*/
    for (int i = 1; i <= 2; i++)
      _Nb_players_choices.push_back(new Title(_data, i, 50));      
    _Nb_players_bra = new Bra_title(_data, _Nb_players_choices, 50);
    _Nb_players_bra->set_visible(true);
    /* Différents choix de map */
    if (_Map_choices.size() != 0)
      {
	_Map_bra = new Bra_title(_data, _Map_choices, 50);
	_Box_bra.push_back(_Map_bra);
	this->update_info();
      }

    /* regle de visibilité */
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      _Box_bra[i]->set_visible(true);
  }

  virtual ~Submenu_histoire_load()
  {
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      delete _Box_bra[i];
    for (size_t i = 0; i < _Maps.size() ; i++)
      delete _Maps[i];
  }

  void		set_modes()
  {
    _menu->get_data()->get_mod().setIntMod("GameMode", MODE_HISTOIRE);
  }

  virtual void	new_game(const std::string *map)
  {
    Save	save("./save/histoire_load/", _menu->get_context());
    GameEngine	*engine;

    std::cout << "je passe le if \n";
    std::cout << map->c_str() << std::endl;
    if ((save.load_saved(_Maps[_Map_bra->get_current()], _data)) == false)
      {
	std::cerr << "Error loading the save" << std::endl;
	return ;
      }
    engine = _data->create_gameengine(_data->get_mod(), _menu->get_context());
    
    if (engine->initialize() == false)
      {
	std::cerr << "that's bad (gameengine)" << std::endl;
	return ;
      }
    while (engine->update() == true)
      engine->draw();
    delete engine;
    if (_data->get_mod().getIntMod("Action") == RESTART)
      {
	_data->get_mod().setIntMod("Action", NONE);
	new_game(map);
      }
  }

  int		handle_activate()
  {
    if (_Return->get_selected() == true)
      {
	_menu->set_activate(HISTOIRE_LOAD, false);
	_menu->set_activate(HISTOIRE, true);
      }
    else if (_Launch_game->get_selected() == true && _Map_choices.size() != 0)
      {
	if (_Map_choices.size() != 0)
	  new_game(_Maps[_Map_bra->get_current()]);
      }
    return (1);
  }

  void		handle_selection(int oldcurrent)
  {
    if (_current != oldcurrent)
      _box[oldcurrent - 1]->set_selected(false);
    _box[_current - 1]->set_selected(true);
    if (_Map_choices.size() != 0)
      _Map_bra->set_selected(_Map->get_selected());
    _Nb_players_bra->set_selected(_Map->get_selected());
  }

  void		handle_input(gdl::Input &input)
  {
    _last_input = get_input(input);
    if (_last_input == UP)
      {
	_current--;
	if (_current == 0)
	  _current = _box.size();
      }
    else if (_last_input == DOWN)
      {
	_current++;
	if (_current == (int)_box.size() + 1)
	  _current = 1;
      }
    else if (_last_input == RIGHT)
      {
	for (size_t i = 0; i < _Box_bra.size(); ++i)
	  if (_box[i]->get_selected() == true)
	    _Box_bra[i]->set_current(_Box_bra[i]->get_current() + 1);
	if (_Map_choices.size() != 0)
	  update_info();
      }
    else if (_last_input == LEFT)
      {
	for (size_t i = 0; i < _Box_bra.size(); ++i)
	  if (_box[i]->get_selected() == true)
	    _Box_bra[i]->set_current(_Box_bra[i]->get_current() - 1);
	if (_Map_choices.size() != 0)
	  update_info();
      }
    if (_last_input == ENTER)
      this->handle_activate();
    _last_input = NONE;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	handle_input(input);
	this->handle_selection(oldcurrent);
      }
    return (true);
  }
  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {
	_backmenu->draw(shader, 0, 100);
	if (_Map_choices.size() != 0)
	  _Map_bra->draw(shader, 225, 50);
	_Nb_players->draw(shader, 125 * 4, 150, 100);
	_Nb_players_bra->draw(shader, 125 * 4, 500);
      }
    _Map->draw(shader, 125 * 1, 160, 50);
    _Launch_game->draw(shader, 125 * 5, 160, 50);
    _Return->draw(shader, 125 * 6, 160, 50);
  }
  void					update_info()
  {
    std::string				target("./save/histoire_load/");
    target.append(_Maps[_Map_bra->get_current()]->c_str());
    std::ifstream			my_file(target.c_str());
    std::string				line;
    size_t			       	found;
    char				buffer[50];
    int					param;
    std::string				oldinter;

    if (my_file.is_open())
      {
	while (getline(my_file, line) && line.compare("MODE_END::") != 0)
	  {
	    found = 0;
	    param = 0;
	    while ((found = line.find("::")) != std::string::npos)
	      {
		std::size_t		length = line.copy(buffer, found);
		buffer[length] = 0;
		std::string		inter(buffer);
		for (int i = 0; i < 50; i++)
		  buffer[i] = 0;
		line.erase(0, found + 2);
		if (param == 1)
		  {
		    double		ret = atof(inter.c_str());
		    _data->get_mod().setIntMod(oldinter, (int)ret);
		  }
		oldinter = inter;
		param++;
	      }
	  }
	my_file.close();
      }
    _Nb_players_bra->set_current(_data->get_mod().getIntMod("HumanPlayers") - 1);
  }

  virtual void		set_activate(bool act) 
  {
    if (act == true && _activate == false)
      {
	_activate = act;
	_box[0]->set_selected(true);
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->set_visible(true);
	refresh();
      }
    else if (act == false && _activate == true)
      {
	_activate = act;
	_current = 1;
	for (size_t i = 0; i < _box.size(); ++i)
	  {
	    _box[i]->set_visible(false);
	    _box[i]->set_selected(false);
	  }
      }
  }

  void					refresh()
  {
    std::vector<std::string *>		result;
    dirent				*de;
    DIR					*dp;

    for (size_t i =  0; i < _Maps.size(); i++)
      delete _Maps[i];
    _Maps.clear();
    _Map_choices.clear();
    dp = opendir("./save/histoire_load");
    if (dp)
      {
	while ((de = readdir(dp)) != NULL)
	  {
	    result.push_back (new std::string(de->d_name));
	  }
	closedir(dp);
	std::sort(result.begin(), result.end());
      }
    for (size_t i =  0; i < result.size(); i++)
      {
	if (result[i]->at(0) != '.') 
	  {
	    _Maps.push_back(result[i]);
	    _Map_choices.push_back(new Title(_data, result[i]->c_str(), 50));
	  }
      }
    if (_Map_choices.size() != 0)
      {
	if (_Map_bra != NULL)
	  delete _Map_bra;
	_Map_bra = new Bra_title(_data, _Map_choices, 50);
	this->update_info();	
      }
    else
      _Map_bra = NULL;
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      _Box_bra[i]->set_visible(true);
  }

  };

#endif /* */
