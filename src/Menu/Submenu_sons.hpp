//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Sun Jun 15 20:30:38 2014 corentin bajdek
//

#ifndef SUBMENU_SONS_HPP_
# define SUBMENU_SONS_HPP_

#include "ASubmenu.hpp"

class Submenu_sons : public ASubmenu
{
private :
  Title				*_Music;
  Title				*_Sons;
  Title				*_Return;
  /* Box de tous les bra */
  std::vector<Bra_title *>	_Box_bra;
  /* Différtes musiques */
  std::vector<Title *>		_Music_choices;
  Bra_title			*_Music_bra;
  /* Differents sons*/
  std::vector<Title *>		_Sons_choices;
  Bra_title			*_Sons_bra;

public :
  Submenu_sons(Map *data, Menu *menu) : ASubmenu(data, menu, 3)
  {
    _Music = new Title(_data, "Musique :", 50);
    _Sons = new Title(_data, "Sons :", 50);
    _Return =  new Title(_data, "Retour", 50);

    _Music_choices.push_back(new Title(_data, "On", 50));
    _Music_choices.push_back(new Title(_data, "Off", 50));
    _Sons_choices.push_back(new Title(_data, "On", 50));
    _Sons_choices.push_back(new Title(_data, "Off", 50));
    _Music_bra = new Bra_title(_data, _Music_choices, 50);
    _Sons_bra = new Bra_title(_data, _Sons_choices, 50);
    _Box_bra.push_back(_Music_bra);
    _Box_bra.push_back(_Sons_bra);

    for (size_t i = 0; i < _Box_bra.size(); ++i)
      _Box_bra[i]->set_visible(true);

    _box.push_back(_Music);
    _box.push_back(_Sons);
    _box.push_back(_Return);

    _data->get_mod().setIntMod("Music", _Music_bra->get_current());
    _data->get_mod().setIntMod("Sons", _Sons_bra->get_current());
  }
  virtual ~Submenu_sons()
  {
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      delete _Box_bra[i];
  }

  void		handle_input(gdl::Input &input)
  {
    /* récupere les inputs de touches */
    _last_input = get_input(input);
    if (_last_input == UP)
      _current--;
    else if (_last_input == DOWN)
      _current++;
    for (size_t i = 0; i < _Box_bra.size(); ++i)
      if (_box[i]->get_selected() == true && _last_input == LEFT)
	_Box_bra[i]->set_current(_Box_bra[i]->get_current() - 1);
      else if (_box[i]->get_selected() == true && _last_input == RIGHT)
	_Box_bra[i]->set_current(_Box_bra[i]->get_current() + 1);
    if (_last_input == ENTER && _Return->get_selected() == true)
      {
	_menu->set_activate(SONS, false);
	_menu->set_activate(OPTIONS, true);
      }
    _last_input = NONE;
    if (_current == 0)
      _current = _box.size();
    if (_current == (int)_box.size() + 1)
      _current = 1;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
      	oldcurrent = _current;
      	for (size_t i = 0; i < _box.size(); ++i)
      	  _box[i]->update(clock, input);
      	handle_input(input);
      	if (_current != oldcurrent)
      	  _box[oldcurrent - 1]->set_selected(false);
      	_box[_current - 1]->set_selected(true);
      	_Music_bra->set_selected(_Music->get_selected());
      	_Sons_bra->set_selected(_Sons->get_selected());
      	_Music_bra->get_current() == 0 ? _data->get_mod().setIntMod("Music", 1) : _data->get_mod().setIntMod("Music", 0);
      	_Sons_bra->get_current() == 0 ? _data->get_mod().setIntMod("Sons", 1) : _data->get_mod().setIntMod("Sons", 0);
        _Music_bra->get_current() == 0 ? _data->get_audio().set_music(1) : _data->get_audio().set_music(0);
        _Sons_bra->get_current() == 0 ? _data->get_audio().set_sound(1) : _data->get_audio().set_sound(0);

     }
        // _data->get_audio().set_sound(_data->get_mod().getIntMod("Sons"));
    return (1);
  }
  virtual void		draw(gdl::AShader &shader)
  {
    if (_activate == true)
      {
	_backmenu->draw(shader, 0, 250);
	_Music_bra->draw(shader, 280, 280);
	_Sons_bra->draw(shader, 280 + 150 * 1, 200);
      }
    _Music->draw(shader, 280, 160, 50);
    _Sons->draw(shader, 280 + 150 * 1, 160, 50);
    _Return->draw(shader, 280 + 150 * 2, 160, 50);
  }
};

#endif /* */
