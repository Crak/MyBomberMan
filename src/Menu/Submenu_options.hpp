//
// Title.hpp for Title in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Mon May 19 20:02:33 2014 corentin bajdek
// Last update Tue May 27 22:10:32 2014 corentin bajdek
//

#ifndef SUBMENU_OPTION_HPP_
# define SUBMENU_OPTION_HPP_

#include "ASubmenu.hpp"

class Submenu_options : public ASubmenu
{
private :
  Title				*_Son;
  Title				*_Video;
  Title				*_Return;

public :
  Submenu_options(Map *data, Menu *menu) : ASubmenu(data, menu, 3)
  {
    _Video = new Title(_data, "Video", 50);
    _Son = new Title(_data, "Son", 50);
    _Return =  new Title(_data, "Retour", 50);

    _box.push_back(_Son);
    _box.push_back(_Video);
    _box.push_back(_Return);
  }
  virtual ~Submenu_options()
  {
  }

  void		handle_input(gdl::Input &input)
  {
    _last_input = get_input(input);
    if (_last_input == UP)
      _current--;
    else if (_last_input == DOWN)
      _current++;
    if (_last_input == ENTER)
      {
	if (_Return->get_selected() == true)
	  {
	    _menu->set_activate(OPTIONS, false);
	    _menu->set_activate(GLOBAL, true);
	  }
	else if (_Video->get_selected() == true)
	  {
	    _menu->set_activate(OPTIONS, false);
	    _menu->set_activate(VIDEO, true);
	  }
	else
	  {
	    _menu->set_activate(OPTIONS, false);
	    _menu->set_activate(SONS, true);
	  }
      }
    _last_input = NONE;
  }

  virtual int	update(gdl::Clock const &clock, gdl::Input &input)
  {
    int		oldcurrent;

    if (_activate == true)
      {
	oldcurrent = _current;
	for (size_t i = 0; i < _box.size(); ++i)
	  _box[i]->update(clock, input);
	/* récupere les inputs de touches */
	handle_input(input);
	if (_current == 0)
	  _current = _box.size();
	if (_current == (int)_box.size() + 1)
	  _current = 1;
	if (_current != oldcurrent)
	  _box[oldcurrent - 1]->set_selected(false);
	_box[_current - 1]->set_selected(true);
      }
    return (1);
  }
};

#endif /* */
