//
// Minimap.hpp for header in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May  8 17:41:17 2014 corentin bajdek
// Last update Mon Jun  9 14:01:43 2014 corentin bajdek
//


#ifndef TEXTE_HPP_
# define TEXTE_HPP_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "../Map.hpp"
#include "Letters.hpp"

class Texte
{
private :
  std::vector<Letter *>		_sentence;
  float				_size;

public :
  Texte(const std::string ph, Map *data, const float size, glm::vec4 color)
    : _size(size)
  {
    Letter	*lettre;

    for (size_t i = 0; i < ph.length(); ++i)
      {
	lettre = new Letter(ph[i], data, size, color);
	_sentence.push_back(lettre);
      }
  }
  ~Texte()
  {
    for (size_t i = 0; i < _sentence.size(); ++i)
      delete _sentence[i];
  }
  
  bool	initialize()
  {
    for (size_t i = 0; i < _sentence.size(); ++i)
      if ((_sentence[i]->initialize()) == false)
	return (false);
    return (true);
}

  void	draw(gdl::AShader &shader, double pos_x, double pos_y)
  {
    for (size_t i = 0; i < _sentence.size(); ++i)
      _sentence[i]->draw(shader, pos_x + (i * (_size) / 2), pos_y);
  }
  };


#endif /* !TEXTE_HPP_ */
