//
// Splitter.hpp for Splitter in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by Lucas SARKADI
// Login   <sarkad_l@epitech.net>
// 
// Started on  Sat Jun 14 00:22:26 2014 Lucas SARKADI
// Last update Sun Jun 15 22:43:45 2014 corentin bajdek
//

#ifndef SPLITTER_HPP_
# define SPLITTER_HPP_

#include <Game.hh>
#include <SdlContext.hh>
#include <Input.hh>
#include <vector>
#include "Map.hpp"
#include "Thread.hpp"

class	Splitter
{
private:
  std::vector<Thread>	_tabUpdate;
  Map			*_data;
  static void		*update_one(void *);
  static void		*update_two(void *);
  static void		*update_three(void *);
  static void		*update_four(void *);
  gdl::Input		&_input;
  gdl::Clock		const &_clock;
public:
  Splitter(gdl::Clock const &, gdl::Input &, Map *);
  ~Splitter();
  Splitter(const Splitter &);
  int	update();
  Map                   *getData() const;
  gdl::Input		&getInput();
  const gdl::Clock	&getClock() const;
};

#endif /* SPLITTER_HPP_ */
