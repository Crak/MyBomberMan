//
// Particles_box.hpp for Particles in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Particles
// 
// Made by Lucas SARKADI
// Login   <sarkad_l@epitech.net>
// 
// Started on  Wed Jun 11 04:19:24 2014 Lucas SARKADI
// Last update Sun Jun 15 02:02:33 2014 Lucas SARKADI
//

#ifndef PARTICLES_BOX_H_
# define PARTICLES_BOX_H_

#include "../AObject.hpp"

#include "Particles.hpp"
#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <cstdlib>

class Particles_box : public Particles
{
public :
  Particles_box(int pos_x, int pos_y, Map *data) : Particles(pos_x, pos_y, data) { } 
  virtual ~Particles_box(void) { }
  
  virtual bool	initialize()
  {
    int		nbParticles = 5;
    
    _lifetime = 1;
    _speed = 9.0f;
    
    _texture = _data->getTextContainer()->getTexture("./assets/1.tga");

    _geometry.setColor(glm::vec4(1.0f, 1.0f, 1.0f, 0.80));

    _geometry.pushVertex(glm::vec3(0.5, 0, 0.5));
    _geometry.pushVertex(glm::vec3(0.5, 0, -0.5));
    _geometry.pushVertex(glm::vec3(-0.5, 0, -0.5));
    _geometry.pushVertex(glm::vec3(-0.5, 0, 0.5));

    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));

    _geometry.build();

    _tabPos.resize(nbParticles);
    for (size_t i = 0; i < _tabPos.size(); ++i)
      _tabPos[i] = glm::vec3((float)_pos_x, (float)_pos_y, (float)(0));
    _tabMove.resize(nbParticles);
    for (size_t i = 0; i < _tabMove.size(); ++i)
      _tabMove[i] = glm::vec3((float)(rand() % 100 - 50), (float)(rand() % 100 - 50), (float)(rand() % 50));
    _tabOri.resize(nbParticles);
    for (size_t i = 0; i < _tabOri.size(); ++i)
      _tabOri[i] = glm::vec3((float)(rand() % 360), (float)(rand() % 360), (float)(rand() % 360));
    _tabRot.resize(nbParticles);
    for (size_t i = 0; i < _tabRot.size(); ++i)
      _tabRot[i] = glm::vec3((float)((rand() % 5 - 2) * 25), ((rand() % 5 - 2) * 25), ((rand() % 5 - 2) * 25));
    _initialize = true;
    return (true);
  }
};

#endif /* !PARTICLES_BOX_H_ */
