//
// Fire.cpp for Fire in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 22:03:34 2014 corentin bajdek
// Last update Sun Jun 15 02:01:34 2014 Lucas SARKADI
//

#include "Particles.hpp"

/*
** Constructeurs / Destructeurs
*/

Particles::Particles(int pos_x, int pos_y, Map *data)
  : AObject(pos_x, pos_y, data)
{
  _type = PARTICLES;
  _subtype = DEFAULT;
  _lifetime = 1.5;
  _speed = 400.0;
  return ;
}

Particles::~Particles(void) { return ; }

bool		Particles::initialize(void)
{
  int		nbParticles = 150;

  _lifetime = 1.5;
  _speed = 6.0;
  
  _texture = _data->getTextContainer()->getTexture("./assets/Fire.tga");

  _geometry.setColor(glm::vec4(1.0f, 1.0f, 1.0f, 0.95f));

  _geometry.pushVertex(glm::vec3(0.05, 0, 0.05));
  _geometry.pushVertex(glm::vec3(0.05, 0, -0.05));
  _geometry.pushVertex(glm::vec3(-0.05, 0, -0.05));
  _geometry.pushVertex(glm::vec3(-0.05, 0, 0.05));

  _geometry.pushUv(glm::vec2(1.0f, 1.0f));
  _geometry.pushUv(glm::vec2(1.0f, 0.0f));
  _geometry.pushUv(glm::vec2(0.0f, 0.0f));
  _geometry.pushUv(glm::vec2(0.0f, 1.0f));

  _geometry.build();

  _tabPos.resize(nbParticles);
  for (size_t i = 0; i < _tabPos.size(); ++i)
    _tabPos[i] = glm::vec3((float)_pos_x, (float)_pos_y, (float)(-0.1));
  _tabMove.resize(nbParticles);
  for (size_t i = 0; i < _tabMove.size(); ++i)
    _tabMove[i] = glm::vec3((float)(rand() % 100 - 50), (float)(rand() % 100 - 50), (float)(rand() % 50));
  _tabOri.resize(nbParticles);
  for (size_t i = 0; i < _tabOri.size(); ++i)
    _tabOri[i] = glm::vec3((float)(rand() % 360), (float)(rand() % 360), (float)(rand() % 360));
  _tabRot.resize(nbParticles);
  for (size_t i = 0; i < _tabRot.size(); ++i)
    _tabRot[i] = glm::vec3((float)((rand() % 5 - 2) * 25), ((rand() % 5 - 2) * 25), ((rand() % 5 - 2) * 25));
  _initialize = true;
  return (true);
}

int		Particles::update(gdl::Clock const &clock, gdl::Input &input)
{
  (void)input;
  _lifetime -= clock.getElapsed();
  if (_lifetime < 0)
    return (false);
  for (size_t i = 0; i < _tabMove.size(); ++i)
    _tabPos[i] += (_tabMove[i] / _speed * (float)clock.getElapsed());
  for (size_t i = 0; i < _tabRot.size(); ++i)
    _tabOri[i] += (_tabRot[i] / _speed * (float)clock.getElapsed());
  _rotation.x = (_rotation.x < 0) ? 359.0f : ((_rotation.x > 359) ? 0.0f : _rotation.x);
  _rotation.y = (_rotation.y < 0) ? 359.0f : ((_rotation.y > 359) ? 0.0f : _rotation.y);
  _rotation.z = (_rotation.z < 0) ? 359.0f : ((_rotation.z > 359) ? 0.0f : _rotation.z);
  return (true);
}

void		Particles::draw(gdl::AShader &shader, gdl::Clock const &clock)
{
  (void)clock;

  glEnable(GL_BLEND);

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  _texture->bind();
  for (size_t i = 0; i < _tabPos.size(); ++i)
    {
      _position = _tabPos[i];
      _rotation = _tabOri[i];
      _geometry.draw(shader, getTransformation(), GL_QUADS);
    }
  glDisable(GL_BLEND);
}

void	Particles::is_touched_by_fire(Bonhomme *father)
{
  (void)father;
  return ;
}
