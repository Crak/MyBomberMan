//
// Fire.hh for Fire in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Sat May  3 22:03:23 2014 corentin bajdek
// Last update Wed Jun 11 04:20:10 2014 Lucas SARKADI
//

#ifndef PARTICLES_H_
# define PARTICLES_H_

#include "../AObject.hpp"

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <cstdlib>

class Particles : public AObject
{
protected :
  gdl::Texture		*_texture;
  gdl::Geometry		_geometry;
  std::vector<glm::vec3>	_tabPos;
  std::vector<glm::vec3>	_tabMove;
  std::vector<glm::vec3>	_tabOri;
  std::vector<glm::vec3>	_tabRot;
  float			_speed;
public :
  Particles(int, int, Map *);
  virtual ~Particles(void);

  virtual bool	initialize();
  virtual int	update(gdl::Clock const &clock, gdl::Input &input);
  virtual void	draw(gdl::AShader &shader, gdl::Clock const &clock);
  virtual void	is_touched_by_fire(Bonhomme *);
};

#endif /* !PARTICLES_H_ */
