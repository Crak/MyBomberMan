//
// Map.hpp for Map in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Fri May  9 20:16:29 2014 corentin bajdek
// Last update Sun Jun 15 13:35:23 2014 corentin bajdek
//

#include <math.h> // Pour pow()
#include "Map.hpp"
#include "GameEngine/GameEngine.hpp"
#include "GameEngine/GameEngine_histoire.hpp"
#include "GameEngine/GameEngine_temps.hpp"
#include "GameEngine/GameEngine_vies.hpp"
#include "AObject.hpp"
#include "Bonhomme/Bonhomme.hpp"
#include "Bonhomme/Bonhomme_J2.hpp"
#include "Bonhomme/Bonhomme_IA.hpp"
#include "Wall/Wall.hpp"
#include "Wall/Wall_Destructible.hpp"
#include "Bonus/Bonus.hpp"
#include "Bombes/Bombes.hpp"
#include "Fire/Fire.hpp"
#include "Floor/Floor.hpp"
#include "Menu/Texte.hpp"
#include "Background.hpp"
#include "Overlay/Overlay_long.hpp"
#include "Overlay/Overlay_top.hpp"
#include "Overlay/Minimap.hpp"
#include "Overlay/Score.hpp"
#include "Overlay/IScore_top.hpp"
#include "Overlay/Score_top_vies.hpp"
#include "Overlay/Score_top_time.hpp"

Map::Map(Mod &mode) :
  _width(mode.getwidth()), _length(mode.getlength()), _mode(mode), _human1(-1), _human2(-1)
{
  _textContainer = new TextContainer;
  _minimap2 = NULL;
  _minimap1 = NULL;
}

Map::~Map() { delete _textContainer; }

GameEngine	*Map::create_gameengine(const Mod &mod, gdl::SdlContext &context,
					int width_screen, int length_screen)
{
  int		gamemode;

  gamemode = _mode.getIntMod("GameMode"); 
  if (gamemode == MODE_HISTOIRE)
    return (new GameEngine_histoire(mod, this, context, width_screen, length_screen));
  else if (gamemode == MODE_VIE)
    return (new GameEngine_vies(mod, this, context, width_screen, length_screen));
  else if (gamemode == MODE_TEMPS)
    return (new GameEngine_temps(mod, this, context, width_screen, length_screen));
  return (new GameEngine(mod, this, context, width_screen, length_screen));
}

IScore_top	*Map::create_score_top(Bonhomme *father)
{
  int		gamemode;

  gamemode = _mode.getIntMod("GameMode"); 
  if (gamemode == MODE_CLASSIC || gamemode == MODE_HISTOIRE)
    return (new Score_top(this, father));
  else if (gamemode == MODE_VIE)
    return (new Score_top_vies(this, father));
  else
    return (new Score_top_time(this, father));
  return (new Score_top(this, father));
}

Score		*Map::create_score(Bonhomme *father)
{
  return (new Score(this, father));
}

Info		*Map::create_info(const std::string path, double target_x, double target_y)
{
  return (new Info(path, this, target_x, target_y));
}

Wall		*Map::create_wall(int _pos_x, int _pos_y, bool _destructible)
{
  if (_destructible == false)
    return (new Wall(_pos_x, _pos_y, this));
  else
    return (new Wall_Destructible(_pos_x, _pos_y, this));
}

void		Map::create_minimap(int wich, Bonhomme *father)
{
  if (wich == J2)
    _minimap2 = new Minimap(0, 0, this, father);
  else
    _minimap1 = new Minimap(0, 0, this, father);
}

void		Map::delete_minimap(int wich)
{
  if (wich == J2)
    {
      delete _minimap2;
      _minimap2 = NULL;
    }
  else
    {
      delete _minimap1;
      _minimap1 = NULL;
    }
}

Bonus		*Map::create_bonus(int _pos_x, int _pos_y)
{
  return (new Bonus(_pos_x, _pos_y, this));
}

Floor		*Map::create_floor(int _pos_x, int _pos_y)
{
  return (new Floor(_pos_x, _pos_y, this));
}

Fire		*Map::create_fire(int _pos_x, int _pos_y, double delay, int multiples, int direction, Bonhomme *father)
{
  return (new Fire(_pos_x, _pos_y, delay, multiples, direction, this, father));
}

Bombes		*Map::create_bombes(int _pos_x, int _pos_y, double delay, Bonhomme *father, int max_len)
{
  return (new Bombes(_pos_x, _pos_y, delay, father, max_len, this));
}

Background	*Map::create_background(const std::string path)
{
  return (new Background(path, this));
}

Overlay		*Map::create_overlay()
{
  if (_mode.getIntMod("Overlay") == 0)
    return (new Overlay("./assets/Overlay.tga", this));
  if (_mode.getIntMod("Overlay") == 1)
    return (new Overlay("./assets/Overlay1.tga", this));
  return (new Overlay("./assets/Overlay.tga", this));
}

Overlay_long	*Map::create_overlay_long()
{
  if (_mode.getIntMod("Overlay") == 0)
    return (new Overlay_long("./assets/Overlay_long.tga", this));
  if (_mode.getIntMod("Overlay") == 1)
    return (new Overlay_long("./assets/Overlay_long1.tga", this));
  return (new Overlay_long("./assets/Overlay_long.tga", this));
}

Overlay_top	*Map::create_overlay_top()
{
  if (_mode.getIntMod("Overlay") == 0)
    return (new Overlay_top("./assets/Overlay_top.tga", this));
  if (_mode.getIntMod("Overlay") == 1)
    return (new Overlay_top("./assets/Overlay_top1.tga", this));
  return (new Overlay_top("./assets/Overlay_top.tga", this));
}

Texte		*Map::create_texte(const std::string ph, const float size, const glm::vec4 color)
{
  return (new Texte(ph, this, size, color));
}

Texte		*Map::create_texte(int value, const float size, int nbchiffre, const glm::vec4 color)
{
  std::ostringstream	ss;
  std::string		tmp;
  

  ss << value;
  tmp = ss.str();
  if (value == 0)
    {
      for (int i = 0; i < nbchiffre - 1; i++)
	if (value < (int)pow(10, i))
	  tmp = std::string("0") + tmp;
    }
  else
    {
      for (int i = 0; i < nbchiffre; i++)
	if (value < (int)pow(10, i))
	  tmp = std::string("0") + tmp;
    }
  return (new Texte(tmp, this, size, color));
}

Loading		*Map::create_loading(int nb_items_max)
{
  return (new Loading(this, nb_items_max));
}

Bonhomme	*Map::create_bonhomme(int _pos_x, int _pos_y, int id)
{
  if (id == _human1)
    return (new Bonhomme(_pos_x, _pos_y, 1, this, id));
  else if (id == _human2)
    return (new Bonhomme_J2(_pos_x, _pos_y, 1, this, id));
  else
    return (new Bonhomme_IA(_pos_x, _pos_y, 1, this, id));
}

Minimap		*Map::get_minimap(int wich) const
{
  if (wich == J2 && _minimap2 != NULL)
    return (_minimap2);
  else if (wich == DEFAULT && _minimap1 != NULL)
    return (_minimap1);
  return (NULL);
}

int		Map::get_width(void) const { return (this->_width) ; }
int		Map::get_length(void) const { return (this->_length) ; }
void		Map::set_width(int width) { this->_width = width; }
void		Map::set_length(int length) { this->_length = length ; }

TextContainer	*Map::getTextContainer(void) const 
{
  return _textContainer;
}

void		Map::define_places(int idj1, int idj2)
{
  _human1 = idj1;
  _human2 = idj2;
}

void		Map::define_places(int Nbspawn)
{
  int		ret;

  if (_mode.getIntMod("HumanPlayers") == 1)
    _human1 = rand() % Nbspawn;
  else if (_mode.getIntMod("HumanPlayers") == 2)
    {
      _human1 = rand() % Nbspawn;
      do
	ret = rand() % Nbspawn;
      while (ret == _human1);
      _human2 = ret;
    }
}

void		Map::clear(void)
{
  _human1 = -1;
  _human2 = -1;
}
