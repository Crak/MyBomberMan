//
// Minimap.hpp for header in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud/src/Menu
// 
// Made by corentin bajdek
// Login   <bajdek_c@epitech.net>
// 
// Started on  Thu May  8 17:41:17 2014 corentin bajdek
// Last update Sun Jun 15 22:46:27 2014 corentin bajdek
//

#ifndef BACKGROUND_HPP_
# define BACKGROUND_HPP_

#include <vector>
#include <Geometry.hh>
#include <Texture.hh>
#include <string>
#include <iostream>
#include <SdlContext.hh>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <BasicShader.hh>
#include "Map.hpp"

class Background
{
private :
  gdl::Texture	 		*_texture;
  gdl::Geometry			_geometry;
  gdl::BasicShader		_shader;
  glm::vec3			_position;
  glm::vec3			_rotation;
  glm::vec3			_scale;  
  Map				*_data;
  const std::string		_path;

public :
  Background(const std::string path, Map *data) : _position(0, 0, 0), _rotation(0, 0, 0), _scale(1, 1, 1), _data(data), _path(path) {}
  ~Background() {}

  glm::mat4	getTransformation()
  {
    glm::mat4 transform(1);

    transform = glm::translate(transform, _position);

    transform = glm::scale(transform, _scale);

    transform = glm::rotate(transform, _rotation.x, glm::vec3(1, 0, 0));
    transform = glm::rotate(transform, _rotation.y, glm::vec3(0, 1, 0));
    transform = glm::rotate(transform, _rotation.z, glm::vec3(0, 0, 1));
    return (transform);
  }

  bool	initialize()
  {
    _texture = _data->getTextContainer()->getTexture(_path);

    _geometry.pushVertex(glm::vec3(0, 1000, 0));
    _geometry.pushVertex(glm::vec3(1000, 1000, 0));
    _geometry.pushVertex(glm::vec3(1000, 0, 0));
    _geometry.pushVertex(glm::vec3(0, 0, 0));

    _geometry.pushUv(glm::vec2(0.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 0.0f));
    _geometry.pushUv(glm::vec2(1.0f, 1.0f));
    _geometry.pushUv(glm::vec2(0.0f, 1.0f));
    _geometry.build();
    return true;
  }

  void			draw(gdl::AShader &shader)
  {
    int			_width = 1000;
    int			_length = 1000;
    glm::mat4		projection = glm::ortho(0.0f, (float)_width, (float)_length, 0.0f, -1.0f, 1.0f);
    
    shader.bind();
    shader.setUniform("view", glm::mat4(1));
    shader.setUniform("projection", projection);
    _texture->bind();
    _position.x = 0;
    _position.y = 0;

    /* activation de la transparence et desactivation du test de profondeur (inutile en 2D) */
    glDisable(GL_DEPTH_TEST);
    _geometry.draw(shader, getTransformation(), GL_QUADS);
    /* remise en état */
    glEnable(GL_DEPTH_TEST);
  }
};

#endif /* !LETTER_HPP_ */
