//
// Thread.cpp for Thread in /home/sarkad_l/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
// 
// Made by Lucas SARKADI
// Login   <sarkad_l@epitech.net>
// 
// Started on  Fri Jun 13 23:59:12 2014 Lucas SARKADI
// Last update Sun Jun 15 22:57:28 2014 corentin bajdek
//

#include <cstdlib>
#include "Thread.hpp"
#include <iostream>

Thread::Thread(Fonction function, void *data) : _function(function) , _data(data)
{ 
  std::cout << "un thread crée" << std::endl;  
}

int	Thread::create()
{
  return (pthread_create(&_id, NULL, _function, _data));
}

int	Thread::join()
{
  pthread_join(_id, NULL);
  return (-1);
}

Thread::~Thread()
{
  std::cout << "thread detruit?" << std::endl;
}
