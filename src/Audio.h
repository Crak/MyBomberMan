/*
** Audio.h for coucou in /home/bajdek_c/Dropbox/Projets/BOMBERMAN-lulu-coco-theo-thibaud
** 
** Made by corentin bajdek
** Login   <bajdek_c@epitech.net>
** 
** Started on  Fri Jun 13 13:54:23 2014 corentin bajdek
** Last update Sun Jun 15 21:48:21 2014 corentin bajdek
*/

#ifndef AUDIO_H_
#define AUDIO_H_

#include "fmod.h"
#include <iostream>

class Audio
{
 public:
  Audio();
  ~Audio();

  void Play_music();
  void Play_blast();
  void set_music(float f);
  void set_blast(float f);
  void set_sound(int);

 private:
  FMOD_SYSTEM  *sys;
  FMOD_SOUND   *_music;
  FMOD_SOUND   *_bomb;
  FMOD_CHANNEL *_cmusic;
  FMOD_CHANNEL *_cbomb;
  int         _sound;
};

#endif
